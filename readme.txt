=== WPHester Plus ===

Contributors: spicethemes
Requires at least: 4.5
Tested up to: 6.7.1
Stable tag: 1.2.3
Requires PHP: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhance WPHester WordPress Themes functionality.

== Description ==

WPHester WordPress Theme is lightweight, elegant, fully responsive and translation-ready theme that allows you to create stunning blogs and websites. The theme is well suited for companies, law firms,ecommerce, finance, agency, travel, photography, recipes, design, arts, personal and any other creative websites and blogs. Theme is developed using Bootstrap 4 framework. It comes with a predesigned home page, good looking header designs and number of content sections that you can easily customize. It also has lots of customization options (banner, services, testimonial, etc) that will help you create a beautiful, unique website in no time. WPHester is  compatible with popular plugins like WPML, Polylang, Woo-commerce  and Conact Form 7. WPHester theme comes with various popular locales.(DEMO: https://wphester-pro.spicethemes.com) 


== Changelog ==

@version 1.2.3
* Updated freemius directory.

@version 1.2.2
* Updated font-awesome library and freemius directory.

@version 1.2.1
* Added Rank Math, Seo Yoast and NavXT Breadcrumbs Feature.

@version 1.2
* Updated freemius directory.

@version 1.1
* Added local google font feature.
* Fix slider section video mute issue.

@Version 1.0
* Added freemius directory.

@Version 0.1
* Initial Release.

== External resources ==

Owl Carousel: 
Copyright: (c) David Deutsch
License: MIT license
Source: https://cdnjs.com/libraries/OwlCarousel2/2.2.1

Alpha color picker Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Repeater Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT license
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-repeater

Custom control - Image Radio Button Custom Control:
Copyright: Anthony Hortin
License: GNU General Public License v2 or later
Source: https://github.com/maddisondesigns/customizer-custom-controls

* Images on /images folder
Copyright (C) 2023, spicethemes and available as [GPLv2](https://www.gnu.org/licenses/gpl-2.0.html)