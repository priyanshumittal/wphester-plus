jQuery( document ).ready(function($) {
	// Change the width of logo
	wp.customize('wphester_logo_length', function(control) {
		control.bind(function( controlValue ) {
			$('.custom-logo').css('max-width', '500px');
			$('.custom-logo').css('width', controlValue + 'px');
			$('.custom-logo').css('height', 'auto');
		});
	});

	// Change the border radius
	wp.customize('after_menu_btn_border', function(control) {
		control.bind(function( borderRadius ) {
		$('.main-header-btn').css('border-radius', borderRadius + 'px');
			
		});
	});

	// Change the container width
	wp.customize('container_width_pattern', function(control) {
		control.bind(function( containerWidth ) {
		$('body .container.container_default').css('max-width', containerWidth + 'px');
		});
	});

	// Change Slider container width
		wp.customize('container_slider_width', function(control) {
		control.bind(function( slideWidth ) {
		$('body .container.slider-caption').css('max-width', slideWidth + 'px');
		});
	});

	//Change CTA 1 Container width
	wp.customize('container_cta1_width', function(control) {
		control.bind(function( cta1Width ) {
		$('body .wphester-cta1-container').css('max-width', cta1Width + 'px');
		});
	});

	// Change Service container width
	wp.customize('container_service_width', function(control) {
		control.bind(function( servicesWidth ) {
		$('body .wphester-service-container.container').css('max-width', servicesWidth + 'px');
		});
	});

	// Change Funfact container width
	wp.customize('container_fun_fact_width', function(control) {
		control.bind(function( funWidth ) {
		$('body .wphester-fun-container.container').css('max-width', funWidth + 'px');
		});
	});

	// Change Portfolio container width
	wp.customize('container_portfolio_width', function(control) {
		control.bind(function( portWidth ) {
		$('body .wphester-portfolio-container.container').css('max-width', portWidth + 'px');
		});
	});

	//Change Homepage Newz Container width
	wp.customize('container_about_width', function(control) {
		control.bind(function( aboutWidth ) {
		$('body .wphester-about-container').css('max-width', aboutWidth + 'px');
		});
	});

	// Change Testi container width
	wp.customize('container_testimonial_width', function(control) {
		control.bind(function( testiWidth ) {
		$('body .wphester-tesi-container.container').css('max-width', testiWidth + 'px');
		});
	});

	//Change Homepage Newz Container width 
	wp.customize('container_home_blog_width', function(control) {
		control.bind(function( newzWidth ) {
		$('body .wphester-newz.container').css('max-width', newzWidth + 'px');
		});
	});

	//Change Homepage Newz Container width
	wp.customize('container_cta2_width', function(control) {
		control.bind(function( cta2Width ) {
		$('body .wphester-cta2-container').css('max-width', cta2Width + 'px');
		});
	});

	//Change Team Container width
	wp.customize('container_team_width', function(control) {
		control.bind(function( teamWidth ) {
		$('body .wphester-team-container.container').css('max-width', teamWidth + 'px');
		});
	});

	//Change shop Container width
	wp.customize('container_shop_width', function(control) {
		control.bind(function( shopWidth ) {
		$('body .wphester-shop-container.container').css('max-width', shopWidth + 'px');
		});
	});

	//Change client & partners Container width
	wp.customize('container_clients_width', function(control) {
		control.bind(function( clientWidth ) {
		$('body .wphester-client-container.container').css('max-width', clientWidth + 'px');
		});
	});	

});