// OWL SLIDER CUSTOM JS

jQuery(document).ready(function () {
    //Slider Lightbox js
        function ctaVideo(div, video_id) {
          var video = jQuery(video_id).src;
          jQuery(video_id).src = video + '?&autoplay=1'; 
          jQuery(div).style.display = 'block';
        }

        jQuery('.header-module .search-box-outer a.search-icon').on('click', function() {
                jQuery('.header-module .search-box-outer ul.dropdown-menu').toggle('addSerchBox');
            });
        // Closing the lightbox 
        function closeVideo(div, video_id) {
          var video = jQuery(video_id).src;
          var cleaned = video.replace('?&autoplay=1', '');
          jQuery(video_id).src = cleaned;
          jQuery(div).style.display = 'none';
        }

  /* Preloader */
  jQuery(window).on('load', function() {
     setTimeout(function(){
        jQuery('body').addClass('loaded');
      }, 1500);
  }); 

    /* ---------------------------------------------- /*
     * Home section height
     /* ---------------------------------------------- */

      jQuery(".search-icon").click(function(e){
            e.preventDefault();
            //console.log();
            //jQuery('.five .search-box-outer ul.dropdown-menu,.seven .search-box-outer ul.dropdown-menu').toggle('addSerchBox');
         });
    function buildHomeSection(homeSection) {
        if (homeSection.length > 0) {
            if (homeSection.hasClass('home-full-height')) {
                homeSection.height(jQuery(window).height());
            } else {
                homeSection.height(jQuery(window).height() * 0.85);
            }
        }
    }


    /* ---------------------------------------------- /*
     * Scroll top
     /* ---------------------------------------------- */

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-up').fadeIn();
        } else {
            jQuery('.scroll-up').fadeOut();
        }
    });

    jQuery('a[href="#totop"]').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 'slow');
        return false;
    });

    // Tooltip Js
    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip()
    });

    // Accodian Js
    function toggleIcon(e) {
        jQuery(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('fa-plus-square-o fa-minus-square-o');
    }
    jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
    jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);

    jQuery('.grid').masonry({
        itemSelector: '.grid-item',
        transitionDuration: '0.2s',
        horizontalOrder: true,
    });


    //Homepage Blog Slider
   
    
});

 (function ($){

  // remove box on click 
    $("a").keypress(function() {
     this.blur();
     this.hideFocus = false;
     this.style.outline = null;
      });
      $("a").mousedown(function() {
           this.blur();
           this.hideFocus = true;
           this.style.outline = 'none';
      });
      
      
         }(jQuery));
