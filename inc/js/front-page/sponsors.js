//RLT check function
if (!jQuery.bol_return) {
    jQuery.extend({
        bol_return: function (tmp_vl) {
            if (tmp_vl == 1) {
                return true;
            }
            return false;
        }
    });
}

jQuery(document).ready(function () {
    switch (sponsorssettings.client_nav_style) {
        case 'bullets':
            sponsorssettings.client_nav_style_nav = false;
            sponsorssettings.client_nav_style_dot = true;
            break;
        case 'navigation':
            sponsorssettings.client_nav_style_nav = true;
            sponsorssettings.client_nav_style_dot = false;
            break;
        case 'both':
            sponsorssettings.client_nav_style_nav = true;
            sponsorssettings.client_nav_style_dot = true;
            break;
        default:
            sponsorssettings.client_nav_style_nav = false;
            sponsorssettings.client_nav_style_dot = true;
    }
    jQuery("#clients-carousel").owlCarousel({
        navigation : true, // Show next and prev buttons        
        autoplay: 3000,
        smartSpeed: sponsorssettings.client_smooth_speed,
        autoplayTimeout: sponsorssettings.client_animation_speed,
        autoplayHoverPause: true,
        loop: true, // loop is true up to 1199px screen.
        nav: sponsorssettings.client_nav_style_nav, // is true across all sizes
        margin: 30, // margin 10px till 960 breakpoint
        autoHeight: true,
        responsiveClass: true, // Optional helper class. Add 'owl-reponsive-' + 'breakpoint' class to main element.
        items: sponsorssettings.slide_items,
        dots: sponsorssettings.client_nav_style_dot,
        navText: sponsorssettings.navdir,
        responsive: {
            100: {items: 1},
            500: {items: 2},
            768: {items: 3},
            1000: {items: sponsorssettings.slide_items}
        },
        rtl: jQuery.bol_return(sponsorssettings.rtl)
    });

});