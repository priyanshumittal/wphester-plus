<?php 
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
wp_register_script('wphester-related-posts', WPHESTERP_PLUGIN_URL . '/inc/js/front-page/related-posts.js', array('jquery'));
wp_localize_script('wphester-related-posts', 'related_posts_settings', array('rtl' => $isRTL));
wp_enqueue_script('wphester-related-posts');

$wphester_plus_related_post = wphester_plus_related_posts(); 
$wphester_plus_related_title=get_theme_mod('wphester_related_post_title',__('Related Posts','wphester-plus'));
if($wphester_plus_related_post->have_posts() ): ?>
<article class="related-posts">
   <?php
   if(!empty($wphester_plus_related_title)):?>
   <div class="comment-title">
      <h3><?php echo esc_html(get_theme_mod('wphester_related_post_title',__('Related Posts','wphester-plus')));?></h3>
   </div>
<?php endif;?>
   <div class="row">
      <div id="related-posts-carousel" class="owl-carousel owl-theme col-lg-12">
         <?php while ($wphester_plus_related_post->have_posts()) : $wphester_plus_related_post->the_post();?>
         <div class="item">
            <article class="single-post preview media">
               <figure class="post-thumbnail">
                  <?php
                        if(has_post_thumbnail()):?>
                        <a href="<?php the_permalink();?>"><?php the_post_thumbnail('full',array('class'=>'img-fluid'));?></a>
                     <?php else:?>
                        <a href="<?php the_permalink();?>">
                           <!--<img class='img-fluid'src="<?php //echo esc_url(WPHESTERP_PLUGIN_URL);?>/inc/images/featured/related.png"/>-->
                        </a>  
                     <?php endif;?>					
               </figure>
               <div class="post-content">
                  <?php
                  if(has_category()):?>
                  <div class="entry-meta">
                     <span class="cat-links"><?php the_category( ', ' );?></span>
                  </div>
               <?php endif;?>
                  <header class="entry-header blog-title">
                      <h4 class="entry-title blog-title"><a class="blog-title" href="<?php the_permalink();?>"><?php the_title();?></a></h4>
                  </header>
               </div>
            </article>
         </div>
         <?php endwhile;
         wp_reset_postdata();?>
      </div>
   </div>
</article>
<?php endif;?>