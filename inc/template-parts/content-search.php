<article  id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>	
	<?php
	if(has_post_thumbnail()):?>
	<figure class="post-thumbnail">
		<?php the_post_thumbnail('full',array('class'=>'img-fluid','alt'=>esc_attr( get_the_title() )));?>	
		<div class="click-view">
			<a href="<?php the_post_thumbnail_url();?>" data-lightbox="image" title="<?php the_title();?>"><i class="fa fa-link"></i></a>
		</div>		
	</figure>	
	<?php endif;?>	
	<div class="post-content <?php if(!has_post_thumbnail()){ echo 'remove-images';}?>">
		<?php if(get_theme_mod('wphester_enable_blog_date',true)==true):?>
			<div class="entry-date <?php if(!has_post_thumbnail()){ echo 'remove-image';}?>">
	            <a href="<?php echo esc_url(home_url()).'/'.esc_html(date('Y/m', strtotime(get_the_date()))); ?>">
	                <span class="date"><?php echo esc_html(get_the_date()); ?></span>
	            </a>
	        </div>
		<?php 
		endif;
		if(get_theme_mod('wphester_enable_blog_author',true) || get_theme_mod('wphester_enable_blog_category',true) || get_theme_mod('wphester_enable_blog_comnt',true)){ ?>
			<div class="entry-meta"> 
				<?php 
				if(get_theme_mod('wphester_enable_blog_author',true)==true):?>
					<i class="fa fa-user-o"></i>
					<span class="author postauthor">
						<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
		                <?php echo get_the_author(); ?>
			            </a>
			        </span>
					<?php 
				endif;

				if(get_theme_mod('wphester_enable_blog_category',true)==true):
					$cat_list = get_the_category_list();
                    if (!empty($cat_list)) {?>
                        <i class="fa fa-folder-open-o"></i><span class="cat-links postcat"><?php the_category(', '); ?></span>
                    <?php }
				endif;

				if (get_theme_mod('wphester_enable_blog_comnt', true) == true):
					$commt = get_comments_number();
                    if (!empty($commt)) {
                        ?>
                        <i class="fa fa-comment-o"></i><span class="cat-links"><a href="<?php the_permalink();?>"><?php echo get_comments_number();?></span></a>
                    <?php }
			    endif;?> 
			</div>
		<?php 
		} ?>
		<header class="entry-header">
			<h4 class="entry-title">
				<a class="rm-h4" href="<?php the_permalink();?>"><?php the_title();?></a>
			</h4> 
		</header>
		<div class="entry-content">
		<?php wphester_posted_content();?>
		 <?php
		 $button_show_hide=get_theme_mod('wphester_blog_content','excerpt');
		 if($button_show_hide=="excerpt")
		 {
		 if(get_theme_mod('wphester_enable_blog_read_button',true)==true):
		 wphester_button_title();
		 endif;
		} ?>
	</div>
	</div>
</article>