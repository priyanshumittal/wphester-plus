<?php
/**
 * Template Name: Blog list view
 */
get_header();
$blog_temp_type = get_theme_mod('wphester_blog_content', 'excerpt');
?>
<section class="page-section-space blog bg-default <?php
if ($blog_temp_type == 'content') {
    echo 'blog-list-view-full';
}
?>">
    <div class="container<?php echo esc_html(wphester_blog_post_container());?>">
        <div class="row">				
            <div class="col-lg-12 col-md-12 col-sm-12 list-view">
                <?php
                if (get_theme_mod('post_nav_style_setting', 'pagination') == 'pagination') {
                    if (get_query_var('paged')) {
                        $paged = get_query_var('paged');
                    } elseif (get_query_var('page')) {
                        $paged = get_query_var('page');
                    } else {
                        $paged = 1;
                    }
                    $args = array('post_type' => 'post', 'paged' => $paged);
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()):
                        while ($loop->have_posts()): $loop->the_post();
                            include(WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-list-view-content.php');
                        endwhile;
                    else:
                        get_template_part('template-parts/content','none');
                    endif;
                    // pagination function
                    echo '<div class="center">';
                    $obj = new wphester_plus_pagination();
                    $obj->wphester_plus_page($loop);
                    echo '</div>';
                } else {
                    echo do_shortcode('[ajax_posts]');
                }
                ?>
            </div>	
        </div>
    </div>
</section>
<?php get_footer(); ?>