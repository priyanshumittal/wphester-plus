<?php
/**
 * Template Name: Shortcode Page
 *
 * @package wphester
 */
get_header();?>
    <header class="entry-header">
        <?php the_post();?>
    </header>
        <div class="entry-content">
          <?php the_content(); ?>
        </div>  
<?php       
get_footer();?>