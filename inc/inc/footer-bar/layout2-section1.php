<div class="col-lg-6 col-md-6 col-sm-6 left">
<?php
 $foot_section_1 = get_theme_mod('footer_bar_sec1','custom_text');
switch ( $foot_section_1 )
      {
        case 'none':
        break;

        case 'footer_menu':
        echo wphester_plus_footer_bar_menu();
        break;

        case 'custom_text':
        echo get_theme_mod('footer_copyright','<p class="copyright-section"><span>'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: <a href="https://spicethemes.com/wphester-wordpress-theme" rel="nofollow">WPHester</a> by <a href="https://spicethemes.com" rel="nofollow">Spicethemes</a>', 'wphester-plus').'</span></p>');
        break;

        case 'widget':
        wphester_plus_footer_widget_area('footer-bar-1');
        break;
      }
?>
</div>