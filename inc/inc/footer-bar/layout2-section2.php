<div class="col-lg-6 col-md-6 col-sm-6 right-info">
<?php
$foot_section_2 = get_theme_mod('footer_bar_sec2','none');
switch ( $foot_section_2 )
      {
        case 'none':
        break;

        case 'footer_menu':
        echo wphester_plus_footer_bar_menu();
        break;

        case 'custom_text':
        echo get_theme_mod('footer_copyright_2','<p class="copyright-section"><span>'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: <a href="https://spicethemes.com/wphester-wordpress-theme" rel="nofollow">WPHester</a> by <a href="https://spicethemes.com" rel="nofollow">Spicethemes</a>', 'wphester-plus').'</span></p>');
        break;

        case 'widget':
        wphester_plus_footer_widget_area('footer-bar-2');
        break;
      }
?>
</div>