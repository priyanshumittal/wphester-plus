<?php
add_action('widgets_init', 'wphester_plus_widgets_init');
function wphester_plus_widgets_init() {

    /* sidebar */

    register_sidebar(array(
        'name' => esc_html__('Footer Bar 1', 'wphester-plus'),
        'id' => 'footer-bar-1',
        'description' => esc_html__('Add widgets in footer bar widget area 1', 'wphester-plus'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
    register_sidebar( array(
        'name' => esc_html__( 'Footer Bar 2', 'wphester-plus' ),
        'id' => 'footer-bar-2',
        'description' => esc_html__( 'Add widgets in footer bar widget area 2', 'wphester-plus' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ) );

     // Header Social Icon Sidebar
    register_sidebar(array(
        'name' => esc_html__('Top header sidebar left area', 'wphester-plus' ),
        'id' => 'home-header-sidebar_left',
        'description' => esc_html__('Add widgets in top header left widget area', 'wphester-plus' ),
        'before_widget' => '<aside id="%1$s" class="widget right-widgets %2$s">',
        'after_widget' => '</aside>',
    ));

    // Subscribe Sidebar
    register_sidebar(array(
        'name' => esc_html__('Top header sidebar right area', 'wphester-plus' ),
        'id' => 'home-header-sidebar_right',
        'description' => esc_html__('Add widgets in top header right widget area', 'wphester-plus' ),
        'before_widget' => '<aside id="%1$s" class="widget left-widgets %2$s">',
        'after_widget' => '</aside>',
    ));

     //register Menu sidebar
    register_sidebar( array(
    'name' => esc_html__('After Menu widget area', 'wphester-plus' ),
    'id' => 'menu-widget-area',
    'description' => esc_html__( 'Widgets in this area are used in the after menu region.', 'wphester-plus' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>',
    ) );

    //register Slider Widgets
    register_sidebar( array(
    'name' => esc_html__('Slider Widgets', 'wphester-plus' ),
    'id' => 'slider-widget-area',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h2 class="widget-title">',
    'after_title' => '</h2>',
    ) );
}