<?php
//error_reporting(0);

// Register and load the widget
function wphester_header_topbar_info_widget() {
    register_widget('wphester_header_topbar_info_widget');
}

add_action('widgets_init', 'wphester_header_topbar_info_widget');

// Creating the widget
class wphester_header_topbar_info_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'wphester_header_topbar_info_widget', // Base ID
                esc_html__('WPHester: Header info widget', 'wphester-plus' ), // Widget Name
                array(
                    'classname' => 'wphester_header_topbar_info_widget',
                    'phone_number' => esc_html__('Topbar header info widget.', 'wphester-plus' ),
                ),
                array(
                    'width' => 600,
                )
        );
    }

    public function widget($args, $instance) {

        //echo $args['before_widget']; 

        echo $args['before_widget'];
        ?>
        <ul class="head-contact-info">
            <li class="phone">
                <?php if (!empty($instance['wphester_phone_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['wphester_phone_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa-solid fa-phone"></i>
                <?php } ?>	
                <?php
                if (!empty($instance['phone_number'])) {
                    echo esc_html($instance['phone_number']);
                } else {
                    echo esc_html($instance['phone_number']);
                }
                ?>
            </li>
            <li class="envelope">
                <?php if (!empty($instance['wphester_email_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['wphester_email_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa-solid fa-envelope"></i>
                <?php }?>
                <?php if (!empty($instance['wphester_email_id'])) { ?>
                <a href="mailto:<?php echo esc_attr($instance['wphester_email_id']);?>">
                    <?php echo esc_html($instance['wphester_email_id']);?>
                </a>
                <?php } ?>
            </li>
        </ul>
        <?php
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance) {

        if (isset($instance['wphester_phone_icon'])) {
            $wphester_phone_icon = $instance['wphester_phone_icon'];
        } else {
            $wphester_phone_icon = esc_html__('fa fa-phone', 'wphester-plus' );
        }

        if (isset($instance['phone_number'])) {
            $phone_number = $instance['phone_number'];
        } else {
            $phone_number = esc_html__('+99 999-999-9999', 'wphester-plus' );
        }

        if (isset($instance['wphester_email_icon'])) {
            $wphester_email_icon = $instance['wphester_email_icon'];
        } else {
            $wphester_email_icon = esc_html__('fa-solid fa-envelope', 'wphester-plus' );
        }

        if (isset($instance['wphester_email_id'])) {
            $wphester_email_id = $instance['wphester_email_id'];
        } else {
            $wphester_email_id = esc_html__('abc@example.com', 'wphester-plus' );
        }

        // Widget admin form
        ?>

        <label for="<?php echo esc_attr($this->get_field_id('wphester_phone_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'wphester-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('wphester_phone_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('wphester_phone_icon')); ?>" type="text" value="<?php
        if ($wphester_phone_icon)
            echo esc_attr($wphester_phone_icon);
        else
            echo esc_attr('fa fa-phone', 'wphester-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'wphester-plus' ); ?><a href="<?php echo esc_url('http://fortawesome.github.io/Font-Awesome/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'wphester-plus' ); ?></a></span>
        <br><br>

        <label for="<?php echo esc_attr($this->get_field_id('phone_number')); ?>"><?php esc_html_e('Phone', 'wphester-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone_number')); ?>" name="<?php echo esc_attr($this->get_field_name('phone_number')); ?>" type="text" value="<?php
        if ($phone_number)
            echo esc_attr($phone_number);
        else
            esc_html_e('+99 999-999-9999', 'wphester-plus' );
        ?>" /><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('wphester_email_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'wphester-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('wphester_email_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('wphester_email_icon')); ?>" type="text" value="<?php
        if ($wphester_email_icon)
            echo esc_attr($wphester_email_icon);
        else
            echo esc_attr('fa fa-phone','wphester-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'wphester-plus' ); ?><a href="<?php echo esc_url('http://fortawesome.github.io/Font-Awesome/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'wphester-plus' ); ?></a></span><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('wphester_email_id')); ?>"><?php esc_html_e('Email', 'wphester-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('wphester_email_id')); ?>" name="<?php echo esc_attr($this->get_field_name('wphester_email_id')); ?>" type="text" value="<?php
        if ($wphester_email_id)
            echo esc_attr($wphester_email_id);
        else
            esc_html_e('abc@example.com', 'wphester-plus' );
        ?>" /><br><br>
        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {

        $instance = array();
        $instance['wphester_phone_icon'] = (!empty($new_instance['wphester_phone_icon']) ) ? wphester_sanitize_text($new_instance['wphester_phone_icon']) : '';
        $instance['phone_number'] = (!empty($new_instance['phone_number']) ) ? wphester_sanitize_text($new_instance['phone_number']) : '';
        $instance['wphester_email_icon'] = (!empty($new_instance['wphester_email_icon']) ) ? wphester_sanitize_text($new_instance['wphester_email_icon']) : '';
        $instance['wphester_email_id'] = (!empty($new_instance['wphester_email_id']) ) ? $new_instance['wphester_email_id'] : '';
       
        return $instance;
    }

}