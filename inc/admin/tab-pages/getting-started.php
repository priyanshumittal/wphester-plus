<?php
/**
 * Getting started template
 */
$wphester_plus_name = wp_get_theme();?>
<div id="getting_started" class="wphester-plus-tab-pane active">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="wphester-plus-info-title text-center">
				<?php 
				/* translators: %s: theme name */
				printf( esc_html__('%s Theme Configuration','wpheter-plus'), $wphester_plus_name );?>
				</h1>
			</div>
		</div>
		<div class="row">
			
			<div class="col-md-12">			
			    <div class="wphester-plus-page">
			    	<div class="mockup">
			    		<img src="<?php echo esc_url(WPHESTERP_PLUGIN_URL).'/inc/admin/assets/img/mockup-pro.png';?>"  width="100%">
			    	</div>				
				</div>	
			</div>	

		</div>

		<div class="row" style="margin-top: 20px;">			

			<div class="col-md-6">
				<div class="wphester-plus-page">
					<div class="wphester-plus-page-top"><?php esc_html_e( 'Links to Customizer Settings', 'wphester-plus' ); ?></div>
					<div class="wphester-plus-page-content">
						<ul class="wphester-plus-page-list-flex">
							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=title_tagline' ) ); ?>" target="_blank"><?php esc_html_e('Site Logo','wphester-plus'); ?></a>
							</li>
							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wphester_theme_panel' ) ); ?>" target="_blank"><?php esc_html_e('Blog Options','wphester-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=nav_menus' ) ); ?>" target="_blank"><?php esc_html_e('Menus','wphester-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=theme_style' ) ); ?>" target="_blank"><?php esc_html_e('Layout & Color Scheme','wphester-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=widgets' ) ); ?>" target="_blank"><?php esc_html_e('Widgets','wphester-plus'); ?></a>
							</li>
							 <li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wphester_general_settings' ) ); ?>" target="_blank"><?php esc_html_e('General Settings','wphester-plus'); ?></a><?php esc_html_e(' ( Preloader, After Menu, Header Presets, Sticky Header, Container settings, Post Navigation Styles, Scroll to Top settings ) ','wphester-plus' ); ?>
							</li>
                                                        <li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=section_settings' ) ); ?>" target="_blank"><?php esc_html_e('Homepage Sections','wphester-plus'); ?></a>
							</li>
                                                        <li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wphester_template_settings' ) ); ?>" target="_blank"><?php esc_html_e('Page template Settings','wphester-plus'); ?></a>
							</li>
			
							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=wphester_typography_setting' ) ); ?>" target="_blank"><?php esc_html_e('Typography','wphester-plus'); ?></a>
							</li>
							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'themes.php?page=wphester_plus_Hooks_Settings' ) ); ?>" target="_blank"><?php esc_html_e('Hooks','wphester-plus'); ?></a>
							</li>
							
							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=frontpage_layout' ) ); ?>" target="_blank"><?php esc_html_e('Sections Order Manager','wphester-plus'); ?></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="col-md-6"> 
				<div class="wphester-plus-page">
					<div class="wphester-plus-page-top"><?php esc_html_e( 'Useful Links', 'wphester-plus' ); ?></div>
					<div class="wphester-plus-page-content">
						<ul class="wphester-plus-page-list-flex">
							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url('https://wphester-pro.spicethemes.com/'); ?>" target="_blank">
								<?php 
								/* translators: %s: theme name */
								printf( esc_html__('%s Plus Demo','wphester-plus'), $wphester_plus_name ); ?></a>
							</li>

							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wphester-plus'); ?>" target="_blank">
									<?php 
								/* translators: %s: theme name */
								printf( esc_html__('%s Plus Details','wphester-plus'), $wphester_plus_name ); ?></a>
							</li>
							
							<li class="">
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url('https://support.spicethemes.com/index.php?p=/categories/wphester-pro'); ?>" target="_blank"><?php 
								/* translators: %s: theme name */
								printf( esc_html__('%s Plus Support','wphester-plus'), $wphester_plus_name );?>
							</li>
							
						    <li class=""> 
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url('https://wordpress.org/support/theme/wphester/reviews/#new-post'); ?>" target="_blank"><?php echo esc_html__('Your feedback is valuable to us','wphester-plus'); ?></a>
							</li>
							
							<li class=""> 
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url('https://helpdoc.spicethemes.com/category/wphester-plus/'); ?>" target="_blank">
									<?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Documentation','wphester-plus'), $wphester_plus_name );?>
								</a>
							</li>
							
						    <li> 
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wphester-free-vs-plus/'); ?>" target="_blank"><?php echo esc_html__('Free vs Plus','wphester-plus'); ?></a>
							</li> 

							<li> 
								<a class="wphester-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/wphester-plus-changelog/'); ?>" target="_blank"><?php echo esc_html__('Changelog','wphester-plus'); ?></a>
							</li> 
						</ul>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>	