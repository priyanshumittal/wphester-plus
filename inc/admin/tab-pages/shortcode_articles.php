<?php
/**
 * shortcode
 */
$customizer_url = admin_url() . 'customize.php' ;
?>

<div id="shortcode_articles" class="wphester-plus-tab-pane active" style="display: none;">
	<div class="container-fluid">			
			<div class="row" style="margin-top: 20px; margin-bottom: 20px;">
				<h3 class="wphester-plus-info-title text-center"><?php esc_html_e('Shortcodes','wphester-plus'); ?></h3>
			<div class="col-md-4" style="padding-left: 50px;">

			<h4>Service Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_service]','wphester-plus'); ?></b></h4><br>

			<h4>Funfact Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_funfact]','wphester-plus'); ?></b></h4><br>

			<h4>Portfolio Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_portfolio]','wphester-plus'); ?></b></h4><br>

			<h4>CTA Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_cta]','wphester-plus'); ?></b></h4><br>
			
			<h4>About Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_about]','wphester-plus'); ?></b></h4><br>

			<h4>Testimonial Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_testimonial]','wphester-plus'); ?></b></h4><br>

			<h4>Team Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_team]','wphester-plus'); ?></b></h4><br>

			<h4>Callout Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_callout]','wphester-plus'); ?></b></h4><br>

			<h4>Client Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_client]','wphester-plus'); ?></b></h4><br>

			<h4>Contact Section Shortcode</h4>
			<h4><b><?php echo esc_html('[wphester_contact]','wphester-plus'); ?></b></h4><br>		
			
		</div>
		<div class="col-lg-8">
			<img src="<?php echo esc_url(WPHESTERP_PLUGIN_URL).'/inc/admin/assets/img/shortcode-image.png';?>"  width="100%">
		</div>
	</div>
	<div class="row" style="margin: 20px;">
		<div style="background-color: #d4edda; padding:10px;">
				<h4><a href="<?php echo esc_url('https://helpdoc.spicethemes.com/wphester-plus/how-to-add-shortcodes-in-wphester-plus/'); ?>" target="_blank"><?php echo esc_html__('Click Here','wphester-plus');?></a> <?php echo esc_html('to know about the attributes with the shortcodes.','wphester-plus');?></h4>
		</div>
	</div>
</div>	