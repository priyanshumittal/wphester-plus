<?php
/**
 * Helper functions.
 *
 * @package wphester
 */

/**
 * Get Footer widgets
 */
if (!function_exists('wphester_plus_footer_widget_area')) {

    /**
     * Get Footer Default Sidebar
     *
     * @param  string $sidebar_id   Sidebar Id..
     * @return void
     */
    function wphester_plus_footer_widget_area($sidebar_id) {

        if (is_active_sidebar($sidebar_id)) {
            dynamic_sidebar($sidebar_id);
        } elseif (current_user_can('edit_theme_options')) {

            global $wp_registered_sidebars;
            $sidebar_name = '';
            if (isset($wp_registered_sidebars[$sidebar_id])) {
                $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
            }
            ?>
            <div class="widget ast-no-widget-row">
                <h2 class='widget-titlhee'><?php echo esc_html($sidebar_name); ?></h2>

                <p class='no-widget-text'>
                    <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                        <?php esc_html_e('Click here to assign a widget for this area.', 'wphester-plus'); ?>
                    </a>
                </p>
            </div>
            <?php
        }
    }

}

/**
 * Function to get Footer Menu
 */
if (!function_exists('wphester_plus_footer_bar_menu')) {

    /**
     * Function to get Footer Menu
     *
     */
    function wphester_plus_footer_bar_menu() {

        ob_start();

        if (has_nav_menu('footer_menu')) {
            wp_nav_menu(
                    array(
                        'theme_location' => 'footer_menu',
                        'menu_class' => 'nav-menu',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 1,
                    )
            );
        } else {
            if (is_user_logged_in() && current_user_can('edit_theme_options')) {
                ?>
                <a href="<?php echo esc_url(admin_url('/nav-menus.php?action=locations')); ?>"><?php esc_html_e('Assign Footer Menu', 'wphester-plus'); ?></a>
                <?php
            }
        }

        return ob_get_clean();
    }

}

if (!function_exists('wphester_plus_widget_layout')):

    function wphester_plus_widget_layout() {

        $wphester_plus_footer_widget = get_theme_mod('footer_widgets_section', 4);
        switch ($wphester_plus_footer_widget) {
            case 1:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-1.php');
                break;

            case 2:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-2.php');
                break;

            case 3:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-3.php');
                break;

            case 4:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-4.php');
                break;

            case 5:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-5.php');
                break;

            case 6:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-6.php');
                break;

            case 7:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-7.php');
                break;

            case 8:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-8.php');
                break;

            case 9:
                include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-9.php');
                break;
        }
    }

endif;

/* Footer Widget Layout section */
if (!function_exists('wphester_plus_footer_widget_layout_section')) {

    function wphester_plus_footer_widget_layout_section() {
            /**
             * Get Footer widgets
             */
            if (!function_exists('wphester_plus_footer_widget_area')) {

                /**
                 * Get Footer Default Sidebar
                 *
                 * @param  string $sidebar_id   Sidebar Id..
                 * @return void
                 */
                function wphester_plus_footer_widget_area($sidebar_id) {

                    if (is_active_sidebar($sidebar_id)) {
                        dynamic_sidebar($sidebar_id);
                    } elseif (current_user_can('edit_theme_options')) {

                        global $wp_registered_sidebars;
                        $sidebar_name = '';
                        if (isset($wp_registered_sidebars[$sidebar_id])) {
                            $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
                        }
                        ?>
                        <div class="widget ast-no-widget-row">
                            <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                            <p class='no-widget-text'>
                                <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                                    <?php esc_html_e('Click here to assign a widget for this area.', 'wphester-plus'); ?>
                                </a>
                            </p>
                        </div>
                        <?php
                    }
                }

            }
            /* Function for widget sectons */
            wphester_plus_widget_layout();
        /* Function for widget sectons */

        
    }

}
/* Footer Widget Layout section */

/* Footer Bar layout section */
if (!function_exists('wphester_plus_footer_bar_section')) {

    function wphester_plus_footer_bar_section() {
        if (get_theme_mod('ftr_bar_enable', true) == true):
            $advance_footer_bar_section = get_theme_mod('advance_footer_bar_section', 1);
            switch ($advance_footer_bar_section) {
                case 1:
                    include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-1.php');
                    break;

                case 2:
                    include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-2.php');
                    break;
            }
        endif;
        }

}
/* Footer Bar layout section */

if (!function_exists('wphester_plus_custom_navigation')) :

    function wphester_plus_custom_navigation() {
        if(get_theme_mod('post_nav_style_setting','pagination')=='pagination'){
            echo '<div class="row justify-content-center center">';
                    $obj = new wphester_plus_pagination();
                    $obj->wphester_plus_page();
                    echo '</div>';
        }else{
            echo do_shortcode('[ajax_posts]');
        }
    }
endif;
add_action('wphester_plus_post_navigation', 'wphester_plus_custom_navigation');

function wphester_plus_comment($comment, $args, $depth) {
    $tag = 'div';
    $add_below = 'comment';
    ?>
    <div class="media comment-box 1">
        <span class="pull-left-comment">
    <?php echo get_avatar($comment, 100, null, 'comments user', array('class' => array('img-fluid comment-img'))); ?>
        </span>
        <div class="media-body">
            <div class="comment-detail">
                <h5 class="comment-detail-title"><?php esc_html(comment_author()); ?><time class="comment-date"><?php 
                /* translators: %1$s: comment date and %2$s: comment time */
                printf(esc_html__('%1$s  %2$s', 'wphester-plus'), esc_html(get_comment_date()), esc_html(get_comment_time())); ?></time></h5>
    <?php comment_text(); ?>

                <div class="reply">
    <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
            </div>


        </div>      

    </div>
    <?php
}

if (!function_exists('wphester_plus_posted_content')) :

    /**
     * Content
     *
     */
    function wphester_plus_posted_content() {
        $blog_content = get_theme_mod('wphester_blog_content', 'excerpt');
        $excerpt_length = get_theme_mod('wphester_blog_content_length', 30);

        if ('excerpt' == $blog_content) {
            $excerpt = wphester_the_excerpt(absint($excerpt_length));
            if (!empty($excerpt)) :
                ?>


                <?php
                echo wp_kses_post(wpautop($excerpt));
                ?>


            <?php endif;
        } else {
            ?>

            <?php the_content(); ?>

        <?php }
        ?>
    <?php
    }

endif;



if (!function_exists('wphester_plus_the_excerpt')) :

    /**
     * Generate excerpt.
     *
     */
    function wphester_plus_the_excerpt($length = 0, $post_obj = null) {

        global $post;

        if (is_null($post_obj)) {
            $post_obj = $post;
        }

        $length = absint($length);

        if (0 === $length) {
            return;
        }

        $source_content = $post_obj->post_content;

        if (!empty($post_obj->post_excerpt)) {
            $source_content = $post_obj->post_excerpt;
        }

        $source_content = preg_replace('`\[[^\]]*\]`', '', $source_content);
        $trimmed_content = wp_trim_words($source_content, $length, '&hellip;');
        return $trimmed_content;
    }

endif;

if (!function_exists('wphester_plus_button_title')) :

    /**
     * Display Button on Archive/Blog Page 
     */
    function wphester_plus_button_title() {
        if (get_theme_mod('wphester_enable_blog_read_button', true) == true):
            $blog_button = get_theme_mod('wphester_blog_button_title', 'READ MORE');

            if (empty($blog_button)) {
                return;
            }
            echo '<p><a href = "' . esc_url(get_the_permalink()) . '" class="more-link">' . esc_html($blog_button) . ' <i class="fa fa-long-arrow-right"></i></a></p>';

        endif;
    }

endif;


/*  Related posts
  /* ------------------------------------ */
if (!function_exists('wphester_plus_related_posts')) {

    function wphester_plus_related_posts() {
        wp_reset_postdata();
        global $post;

        // Define shared post arguments
        $args = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'ignore_sticky_posts' => 1,
            'orderby' => 'rand',
            'post__not_in' => array($post->ID),
            'posts_per_page' => 10
        );
        // Related by categories
        if (get_theme_mod('wphester_related_post_option') == 'categories') {

            $cats = get_post_meta($post->ID, 'related-cat', true);

            if (!$cats) {
                $cats = wp_get_post_categories($post->ID, array('fields' => 'ids'));
                $args['category__in'] = $cats;
            } else {
                $args['cat'] = $cats;
            }
        }
        // Related by tags
        if (get_theme_mod('wphester_related_post_option') == 'tags') {

            $tags = get_post_meta($post->ID, 'related-tag', true);

            if (!$tags) {
                $tags = wp_get_post_tags($post->ID, array('fields' => 'ids'));
                $args['tag__in'] = $tags;
            } else {
                $args['tag_slug__in'] = explode(',', $tags);
            }
            if (!$tags) {
                $break = true;
            }
        }

        $query = !isset($break) ? new WP_Query($args) : new WP_Query;
        return $query;
    }

}

/**
 * Displays the author name
 */
function wphester_plus_get_author_name($post) {

    $user_id = $post->post_author;
    if (empty($user_id)) {
        return;
    }

    $user_info = get_userdata($user_id);
    echo esc_html($user_info->display_name);
}

function wphester_plus_footer_ribbun_fn(){
$ribon_title= get_theme_mod('ribon_title','Follow Us:');
$ribon_enable = get_theme_mod('ribon_setting_enable',true);
    if($ribon_enable == true) : ?>
        <div class="footer-social-links">
            <div class="container">
                <ul class="footer-custom-social-icons">
                <?php
                  if(!empty($ribon_title)):?>
                  <li class="text-white"><span><?php echo $ribon_title; ?></span></li>
                  <?php
                endif;
                  $ribon_data =get_theme_mod('wphester_ribon_content');
                  if(empty($ribon_data))
                    {
                      $ribon_data = json_encode( array(
                      array(
                            'icon_value' => 'fa-brands fa-facebook-f',
                            'title'      => esc_html__( 'Facebook', 'wphester-plus' ),
                            'link'       => '#',
                            'open_new_tab' => 'yes',
                            'id'         => 'customizer_repeater_56d7ea7f40b76',
                            ),
                            array(
                            'icon_value' => 'fa-brands fa-x-twitter',
                            'title'      => esc_html__( 'Twitter', 'wphester-plus' ),
                            'link'       => '#',
                            'open_new_tab' => 'yes',
                            'id'         => 'customizer_repeater_56d7ea7f40b77',
                            ),
                            array(
                            'icon_value' => 'fa-brands fa-linkedin',
                            'title'      => esc_html__( 'LinkedIn', 'wphester-plus' ),
                            'link'       => '#',
                            'open_new_tab' => 'yes',
                            'id'         => 'customizer_repeater_56d7ea7f40b78',
                            ),
                            array(
                            'icon_value' => 'fa-brands fa-instagram',
                            'title'      => esc_html__( 'Instagram', 'wphester-plus' ),
                            'link'       => '#',
                            'open_new_tab' => 'yes',
                            'id'         => 'customizer_repeater_56d7ea7f40b80',
                            ),
                            array(
                            'icon_value' => 'fa-brands fa-whatsapp',
                            'title'      => esc_html__( 'Whatsapp', 'wphester-plus' ),
                            'link'       => '#',
                            'open_new_tab' => 'yes',
                            'id'         => 'customizer_repeater_56d7ea7f40b79',
                            ),                
                            array(
                            'icon_value' => 'fa-brands fa-pinterest-p',
                            'title'      => esc_html__( 'Pinterest', 'wphester-plus' ),
                            'link'       => '#',
                            'open_new_tab' => 'yes',
                            'id'         => 'customizer_repeater_56d7ea7f40b80',
                            ),
                            array(
                            'icon_value' => 'fa-brands fa-youtube',
                            'title'      => esc_html__( 'Youtube', 'wphester-plus' ),
                            'link'       => '#',
                            'open_new_tab' => 'yes',
                            'id'         => 'customizer_repeater_56d7ea7f40b80',
                            ),
                      ) );
                    }
                  $ribon_data = json_decode($ribon_data);
                    if (!empty($ribon_data))
                    { 
                      foreach($ribon_data as $ribon_team)
                      { 
                        $ribon_icon = ! empty( $ribon_team->icon_value ) ? apply_filters( 'wphester_translate_single_string', $ribon_team->icon_value, 'Ribon section' ) : '';
                        
                        $ribon_title = ! empty( $ribon_team->title ) ? apply_filters( 'wphester_translate_single_string', $ribon_team->title, 'Ribon section' ) : '';
                        
                        $ribon_link = ! empty( $ribon_team->link ) ? apply_filters( 'wphester_translate_single_string', $ribon_team->link, 'Ribon section' ) : '#';
                        if(!empty($ribon_icon)):
                            $exp = explode("fa-",$ribon_icon);
                            $a = $exp[1]; ?>
                            <li>
                              <a class="<?php echo $a; ?>" <?php if($ribon_team->open_new_tab== 'yes'){echo "target='_blank'";} ?> href="<?php echo esc_url($ribon_link); ?>" ><i class="fa <?php echo esc_attr($ribon_icon); ?>"></i><?php echo esc_html($ribon_team->title); ?></a>
                            </li>
                        <?php endif;?>
                    
                        <?php 
                        }                     
                    } ?>
                </ul>
            </div>
        </div>
    <?php
    endif;
}
add_action('wphester_plus_footer_ribbun_hook','wphester_plus_footer_ribbun_fn');

function wphester_plus_footer_section_fn(){
wphester_plus_before_footer_section_trigger();?>
<footer class="site-footer" <?php if (!empty(get_theme_mod('ftr_wgt_background_img'))): ?> style="background-image: url(<?php echo get_theme_mod('ftr_wgt_background_img'); ?>);" <?php endif; ?>>  
    <?php 
    do_action('wphester_plus_footer_ribbun_hook');
    $fwidgets_overlay_section_color = get_theme_mod('wphester_plus_fwidgets_overlay_section_color', 'rgba(0, 0, 0, 0.7)'); ?>
    <div class="overlay" <?php
    if (!empty(get_theme_mod('ftr_wgt_background_img'))) {
        if (get_theme_mod('wphester_plus_fwidgets_image_overlay', true) == true) {
            ?> style="background-color:<?php echo $fwidgets_overlay_section_color; ?>;width: 100%;" <?php
             }
         }
         ?>>

        <?php if (get_theme_mod('ftr_widgets_enable', true) === true) { ?>
            <div class="container">
                <?php
//        Widget Layout
                wphester_plus_footer_widget_layout_section();
                ?>
            </div>
        <?php } ?>

        <!-- Animation lines-->
        <div _ngcontent-kga-c2="" class="lines">
            <div _ngcontent-kga-c2="" class="line"></div>
            <div _ngcontent-kga-c2="" class="line"></div>
            <div _ngcontent-kga-c2="" class="line"></div>
        </div>
        <!--/ Animation lines-->

        <?php
//    Footer bar
        wphester_plus_footer_bar_section();
        ?>
    </div>
</footer>
<?php wphester_plus_after_footer_section_trigger();
$ribon_enable = get_theme_mod('scrolltotop_setting_enable', true);
if ($ribon_enable == true) {
    ?>
    <div class="scroll-up custom <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>"><a href="#totop"><i class="<?php echo get_theme_mod('wphester_plus_scroll_icon_class', 'fa fa-angle-up'); ?>"></i></a></div>
<?php } ?>

<style type="text/css">
    .scroll-up {
        <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>: 30px !important;
    }
    .scroll-up.left{right: auto;}
    .scroll-up.custom a {
        border-radius: <?php echo get_theme_mod('wphester_plus_scroll_border_radius', 3); ?>px;
    }  
    <?php if(get_theme_mod('apply_scrll_top_clr_enable',false)==true):?>
    .scroll-up.custom a {
    background: <?php echo get_theme_mod('wphester__scroll_bg_color','#56ABAB');?>;
    color: <?php echo get_theme_mod('wphester__scroll_icon_color','#fff');?>;
    }
    .scroll-up.custom a:hover,
    .scroll-up.custom a:active {
        background: <?php echo get_theme_mod('wphester__scroll_bghover_color','#fff');?>;
        color: <?php echo get_theme_mod('wphester__scroll_iconhover_color','#56ABAB');?>;
    }
<?php endif;?> 
 <?php if(get_theme_mod('funfact_image_overlay',true)){?>    
    .funfact .overlay {
            background-color: rgba(0, 0, 0, 0.8);
        }
<?php }?> 
</style>
<style type="text/css">
    <?php
    if (get_theme_mod('testimonial_image_overlay', true) != false) {
        $testimonial_overlay_section_color = get_theme_mod('testimonial_overlay_section_color', 'rgba(0,0,0,0.3)');
        ?>
        .section-space.testimonial:before {
            background-color:<?php echo $testimonial_overlay_section_color; ?>;
        }
               
        .testi-4:before {
            background-color: <?php echo $testimonial_overlay_section_color; ?>;
        <?php } ?>
        </style>

    <style type="text/css">
            .site-footer {
            background-repeat:  <?php echo get_theme_mod('footer_widget_reapeat', 'no-repeat'); ?>;
            background-position: <?php echo get_theme_mod('footer_widget_position', 'left top'); ?>;
            background-size: <?php echo get_theme_mod('footer_widget_bg_size', 'cover'); ?>;
            background-attachment: <?php echo get_theme_mod('footer_widget_bg_attachment', 'scroll'); ?>;
        }
    </style>
<?php
$slidelayout=get_theme_mod('home_testimonial_design_layout',1);
$slideitem=get_theme_mod('home_testimonial_slide_item',1);
if($slidelayout==2 && $slideitem==3){?>
    <style type="text/css">
        #testimonial-carousel2 .testmonial-block, 
        .page-template-template-testimonial-6 .testmonial-block {
            padding: 15px;
            margin: 0 15px 15px;
        }
        #testimonial-carousel2 .avatar, 
        .page-template-template-testimonial-6 .avatar {
            position: relative;
        }
    </style>
    <?php
}
//Stick Header
    if (get_theme_mod('sticky_header_enable', false) == true):
        include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/sticky-header/sticky-with' . get_theme_mod('sticky_header_animation', '') . '-animation.php');
    endif;
}

add_action('wphester_plus_footer_section_hook', 'wphester_plus_footer_section_fn');

function the_company_blog_meta() {
    if (get_theme_mod('wphester_enable_blog_author') == false) {
        $string = ' In ';
    } else {
        $string = ' in ';
    }
    return $string;
}

function the_company_single_meta() {
    if (get_theme_mod('wphester_plus_enable_single_post_admin') == false) {
        $string = ' In ';
    } else {
        $string = ' in ';
    }
    return $string;
}

add_action('wphester_plus_sticky_header_logo', 'wphester_plus_sticky_header_logo_fn');

function wphester_plus_sticky_header_logo_fn() {
    $custom_logo_id = get_theme_mod('custom_logo');
    $image = wp_get_attachment_image_src($custom_logo_id, 'full');
    if (get_theme_mod('sticky_header_device_enable', 'desktop') == 'desktop') {
        $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop', '');
        if (!empty($sticky_header_logo_desktop)):
            ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($sticky_header_logo_desktop); ?>" class="custom-logo"></a>
            <?php
        else:
            ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
            <?php
            endif;
        } elseif (get_theme_mod('sticky_header_device_enable', 'desktop') == 'mobile') {
            $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl', '');
            if (!empty($sticky_header_logo_mbl)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
        <?php
            endif;
        } else {
            $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop', '');
            if (!empty($sticky_header_logo_desktop)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($sticky_header_logo_desktop); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
            <?php
            endif;

            $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl', '');
            if (!empty($sticky_header_logo_mbl)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
        <?php
        endif;
    }
}

/* WPHester Contact template function * */

function content_contact_data($data) {
    $contact_data = get_theme_mod($data);
    if (empty($contact_data)) {
        $contact_data = json_encode(array(
            array(
                'icon_value' => 'fa-solid fa-location-dot',
                'title' => 'Address',
                'text' => __('17504 Carlton Cuevas Rd, Gulfport, MS, 39503', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b60',
            ),
            array(
                'icon_value' => 'fa-solid fa-mobile-screen',
                'title' => 'Phone',
                'text' => __('(007) 123 456 7890<br>(007) 444 333 6678', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b61',
            ),
            array(
                'icon_value' => 'fa-solid fa-envelope',
                'title' => 'Email',
                'text' => __('info@honeypress.com <br>support@honeypress.com', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b62',
            ),           
        ));
    }
    return $contact_data;
}

// Add heder feature
add_action('wphester_plus_header_feaure_section_hook','wphester_plus_header_feaure_section_hook');
function wphester_plus_header_feaure_section_hook(){
    if (get_theme_mod('header_logo_placing', 'seven')!='eight'):
        include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/topbar-header.php');
    endif;
    wphester_plus_before_header_section_trigger();
    //Header Preset
    if (get_theme_mod('header_logo_placing', 'left')):
        $header_logo_path= WPHESTERP_PLUGIN_DIR.'/inc/inc/header-preset/menu-with-'.get_theme_mod('header_logo_placing','seven').'-logo.php';
        include_once($header_logo_path);
    endif;
    
    if (get_theme_mod('search_effect_style_setting', 'toggle') != 'toggle'):?>
        <div id="searchbar_fullscreen" <?php if (get_theme_mod('search_effect_style_setting', 'popup_light') == 'popup_light'): ?> class="bg-light" <?php endif; ?>>
            <button type="button" class="close">×</button>
            <form method="get" id="searchform" autocomplete="off" class="search-form" action="<?php echo esc_url(home_url('/')); ?>"><label><input autofocus type="search" class="search-field" placeholder="Search Keyword" value="" name="s" id="s" autofocus></label><input type="submit" class="search-submit btn" value="<?php echo esc_html__('Search', 'wphester-plus'); ?>"></form>
        </div>
    <?php
    endif;
    wphester_plus_after_header_section_trigger();
}

//Container Setting For Page
function wphester_container()
{
  if(get_theme_mod('page_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('page_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Container Setting For Blog Post
function wphester_blog_post_container()
{
  if(get_theme_mod('post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Conainer Setting For Single Post

function wphester_single_post_container()
{
  if(get_theme_mod('single_post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('single_post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Preloader feature section function
function wphester_plus_preloader_feaure_section_fn(){
global $template;
$col=explode("-",basename($template));
if (array_key_exists(1,$col)){
 $column=$col[1];
}
else{
$column='';
}
//Preloader
if(get_theme_mod('preloader_enable',false)==true && ($column!='portfolio')):
 include_once(WPHESTERP_PLUGIN_DIR.'/inc/inc/preloader/preloader-'.get_theme_mod('preloader_style',1).'.php');
endif;
}
add_action('wphester_plus_preloader_feaure_section_hook','wphester_plus_preloader_feaure_section_fn');





/* SHORTCODE FOR THEME */



//SHORTCODE FOR SERVICE SECTION
function wphester_service_callback ( $atts ){
ob_start();    
extract(shortcode_atts( array('style' => '1','col' => '3', ), $atts ));
$service_style=$style;
if($service_style==1 || $service_style==2) { $services_id=''; } else { $services_id=$service_style; }
switch($col)
{   
    case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
    case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
    case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    default:$col = 'col-md-4 col-sm-6 col-xs-12'; break; 
} 
$service_data = get_theme_mod('wphester_service_content');
if (empty($service_data)) { $service_data = starter_service_json(); }
$wphester_service_section_title = get_theme_mod('home_service_section_title', __('Services We Provide', 'wphester-plus'));
$wphester_service_section_discription = get_theme_mod('home_service_section_discription', __('Our Best Services', 'wphester-plus'));        
?>
<section class="section-space services<?php echo $services_id;?> bg-default<?php if($service_style==4){echo '-lite';} ?>">
    <div class="wphester-service-container container">
        <?php if ($wphester_service_section_discription != '' || $wphester_service_section_title != '') {?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-header">
                    <?php if ($wphester_service_section_title != '') { ?>
                    <h2 class="section-title"><?php echo $wphester_service_section_title; ?></h2>
                    <?php if ($wphester_service_section_discription != '') { ?>
                    <h5 class="section-subtitle"><?php echo $wphester_service_section_discription; ?></h5>
                    <?php } 
                    } ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <?php
            $service_data = json_decode($service_data);
            if (!empty($service_data)) {
            foreach ($service_data as $service_team) {
                    $service_icon = !empty($service_team->icon_value) ? apply_filters('wphester_translate_single_string', $service_team->icon_value, 'Service section') : '';
                    $service_image = !empty($service_team->image_url) ? apply_filters('wphester_translate_single_string', $service_team->image_url, 'Service section') : '';
                    $service_title = !empty($service_team->title) ? apply_filters('wphester_translate_single_string', $service_team->title, 'Service section') : '';
                    $service_desc = !empty($service_team->text) ? apply_filters('wphester_translate_single_string', $service_team->text, 'Service section') : '';
                    $service_link = !empty($service_team->link) ? apply_filters('wphester_translate_single_string', $service_team->link, 'Service section') : '';
                    ?>
            <div class="<?php echo $col;?>">
                <?php if($service_style==1): ?>
                    <div class="card">
                        <div class="card-body">
                            <?php if ($service_link != '' && $service_image == '') { ?>
                                <p class="service-icon">
                                    <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo $service_link; ?>">
                                    <i class="fa <?php echo $service_icon; ?>"></i>
                                    </a>
                                </p>
                            <?php }
                            elseif($service_image != '') {
                                if ($service_link != '') { ?>
                                    <p class="service-icon">
                                    <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo $service_link; ?>">
                                <?php } ?>
                                <img class='card-img-top' src="<?php echo $service_image; ?>">
                                <?php if ($service_link != '') { ?>
                                    </a>
                                </p>
                                <?php } 
                            }
                            else { ?>
                                <p class="service-icon"><a><i class="fa <?php echo $service_icon; ?>"></i></a></p>
                            <?php }
                            if ($service_title != "") { ?>
                                <h4 class="entry-title">
                                <?php if ($service_link != '') { ?>
                                    <a href="<?php echo $service_link; ?>" <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?>> 
                                <?php
                                } 
                                    echo $service_title;
                                if ($service_link != '') { ?>
                                    </a>
                                <?php } ?>
                                </h4>
                            <?php
                            }?>
                            <div class="section-separator"></div>
                            <?php if ($service_desc != "") { ?>
                                <p class="description"><?php echo $service_desc; ?></p>
                            <?php  } ?>
                            <?php if ($service_link != '' && $service_image == '') { ?>
                                <p class="service-big-icon">
                                    <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo $service_link; ?>">
                                    <i class="fa <?php echo $service_icon; ?>"></i>
                                    </a>
                                </p>
                            <?php } 
                            else { ?>
                                <p class="service-big-icon"><a><img class='card-img-top' src="<?php echo $service_image; ?>"></a></p>
                            <?php }?>
                        </div>
                    </div>
                <?php else:?>
                <article class="post <?php if($service_style!=4) { ?>text-center <?php } ?>">
                <?php
                if ($service_team->choice == 'customizer_repeater_icon') {
                    if ($service_icon != '') { ?>
                        <figure class="post-thumbnail"> 
                        <?php if ($service_link != '') { ?>
                            <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo $service_link; ?>">
                                <i class="fa <?php echo $service_icon; ?>"></i>
                             </a>
                        <?php } 
                        else { ?>
                            <a><i class="fa <?php echo $service_icon; ?>"></i></a>
                        <?php } ?>
                        </figure>
                    <?php
                    }
                } 
                else if ($service_team->choice == 'customizer_repeater_image') {
                    if ($service_image != '') { ?>
                        <figure class="post-thumbnail"> 
                        <?php if ($service_link != '') { ?>
                            <a <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?> href="<?php echo $service_link; ?>">
                        <?php } ?>
                                <img class='card-img-top' src="<?php echo $service_image; ?>">
                         <?php if ($service_link != '') { ?>
                            </a>
                        <?php } ?>
                        </figure>
                    <?php
                    }
                }
                if ($service_title != "") { ?>
                    <div class="entry-header">
                        <h4 class="entry-title">
                        <?php if ($service_link != '') { ?>
                            <a href="<?php echo $service_link; ?>" <?php if ($service_team->open_new_tab == 'yes') { echo "target='_blank'"; } ?>> 
                        <?php
                        } 
                            echo $service_title;
                        if ($service_link != '') { ?>
                            </a>
                        <?php } ?>
                        </h4>
                    </div>
                <?php
                }
                if ($service_desc != "") { ?>
                    <div class="entry-content"><p><?php echo $service_desc; ?></p></div>
                <?php  } ?>      
                </article>
                <?php endif;?>
                </div>
                <?php 
                    }
                } ?>
            </div>
        </div>
    </section>
<?php 
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;   
}
add_shortcode( 'wphester_service', 'wphester_service_callback' );

//SHORTCODE FOR Callout
function wphester_callout_callback(){
ob_start(); 
$home_cta1_title = get_theme_mod('home_cta1_title', __('GET YOUR FREE CONSULTANT NOW', 'wphester-plus'));
$home_cta1_btn_text = get_theme_mod('home_cta1_btn_text', __('GET FREE QUOTE', 'wphester-plus'));
$cta1_button_link = get_theme_mod('home_cta1_btn_link', '#');
$home_cta1_btn_link_target = get_theme_mod('home_cta1_btn_link_target', false);
?>  
<!--Call to Action-->
<?php
$callout_cta1_background = get_theme_mod('callout_cta1_background', '');
if ($callout_cta1_background != '') {
    ?>
    <section class="callout bg-default-lite"  style="background-color: <?php echo esc_url($callout_cta1_background); ?>">
    <?php
    } else {
        ?>
        <section class="callout bg-default-lite" >
            <?php
        }
        ?>
        <?php if (!empty($home_cta1_title) || (!empty($home_cta1_btn_text))): ?>
            <div class="wphester-cta1-container container">
                <div class="row">
                    <div class="col-md-8 col-sm-7 col-xs-12 bg-text">
                        <div class="section-header">
                          <?php if (!empty($home_cta1_title)): ?><h2 class="section-title"><?php echo $home_cta1_title; ?></h2><?php endif; ?>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5 col-xs-12 bg-btn">
                        <?php if ($home_cta1_btn_text != '') {
                            ?>
                            <a class="btn-small btn-light cta_btn" <?php if ($cta1_button_link != '') { ?> href="<?php echo $cta1_button_link; ?>" <?php
                                                                           if ($home_cta1_btn_link_target == true) {
                                                                               echo "target='_blank'";
                                                                           }
                                                                       }else{echo "href='#'";}
                                                                       ?>><?php echo $home_cta1_btn_text; ?></a>

                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </section>
    <!--Cta Section-->
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}
add_shortcode( 'wphester_callout', 'wphester_callout_callback' );


//SHORTCODE FOR CTA
function wphester_cta_callback(){
ob_start();    
$home_cta2_title = get_theme_mod('home_cta2_title', __('Best Solutions', 'wphester-plus'));
$home_cta2_desc = get_theme_mod('home_cta2_desc', __('Make you feel <br> good is our main priority!', 'wphester-plus'));
$home_cta2_embed_code = get_theme_mod('home_cta2_btn1_link', '<iframe width="560" height="315" src="https://www.youtube.com/embed/X7lTMMtoA74" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
 preg_match('/src="([^"]+)"/', $home_cta2_embed_code, $matches);
$home_cta2_btn_link=$matches[1];?>

 <script type="text/javascript">

        //Slider Lightbox js
        function ctaVideo(div, video_id) {
            var video = document.getElementById(video_id).src;
            document.getElementById(video_id).src = video + '?&autoplay=1';
            document.getElementById(div).style.display = 'block';
        }

        // Closing the lightbox 
        function closeVideo(div, video_id) {
            var video = document.getElementById(video_id).src;
            var cleaned = video.replace('?&autoplay=1', '');
            document.getElementById(video_id).src = cleaned;
            document.getElementById(div).style.display = 'none';
        }

</script>
<?php 

$callout_cta2_background = get_theme_mod('callout_cta2_background', WPHESTERP_PLUGIN_URL.'/inc/images/bg/cta-3.jpg');
if ($callout_cta2_background != '') {?>

<!--Call to Action-->

    <section class="section-space cta"  style="background-image:url('<?php echo esc_url($callout_cta2_background); ?>'); background-repeat: no-repeat; background-position: top left; width: 100%;
             background-size: cover;">
             <?php
     } else {
         ?>
    <section class="section-space cta" >
        <?php
    }
    $cta2_overlay_section_color = get_theme_mod('cta2_overlay_section_color', 'rgba(0, 0, 0, 0.75)');
    $cta2_image_overlay = get_theme_mod('cta2_image_overlay', true);?>
        <div class="overlay" <?php if ($cta2_image_overlay != false) { ?>style="background-color:<?php echo $cta2_overlay_section_color; ?>"<?php }?> >
        <?php if (!empty($home_cta2_title) || (!empty($home_cta2_desc))):?>
            <div class="wphester-cta2-container container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="section-header">
                        <?php 
                        if(!empty($home_cta2_title)):?>
                            <p class="section-subtitle"><?php echo $home_cta2_title; ?></p>
                        <?php endif;
                        if (!empty($home_cta2_desc)): ?>
                            <h2 class="section-title"><?php echo $home_cta2_desc; ?></h2>
                        <?php endif;?>                        
                        </div>
                    </div>
                </div>
                <?php
                if ($home_cta2_btn_link != '' ):?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="cta-block text-center">
                                <div class="video-btn"> 
                                   <a id="cta-video" onclick="ctaVideo('video','youtube')" ></a>
                               </div>
                            </div>
                        </div>
                    </div>                                   
                <?php endif; ?>                  
            </div>
        </div>
        <?php endif; ?>
    </section>
  <!--Cta Video Lightbox-->
  <div id="video" class="lightbox-cta" onclick="closeVideo('video','youtube')" style="display: none;">
        <div class="lightbox-container">
              <div class="lightbox-content">
                <button onclick="closeVideo('video','youtube')" class="lightbox-close"> ✕ </button>
                <div class="video-container">
                <iframe id="youtube" width="960" height="540" src="<?php echo esc_url($home_cta2_btn_link);?>" frameborder="0" allowfullscreen=""></iframe>
             </div>
            </div>
       </div>
  </div>
<!--Call to Action-->
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}
add_shortcode( 'wphester_cta', 'wphester_cta_callback' );



//SHORTCODE FOR Testimonial
function wphester_testimonial_callback( $atts )
{
ob_start();    
extract(shortcode_atts( array(
      'format' => 'slide',
      'style' => '1',
      'items' =>'1',
      'col'=>'1',
   ), $atts ));
if(strtolower($style)=='1'){if(strtolower($format)=="slide"){$shortitem='shortitem-'.$items;}else{$shortitem='shortitem-'.$col;}}else{$shortitem='';}
switch($col)
{
    case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
    case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
    case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    default: $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    
}
switch($items)
{
    case '1': $items = 1; break;
    case '2': $items = 2; break; 
    case '3': $items = 3; break; 
    case '4': $items = 4; break; 
    default: $items = 1 ; break; 
    
}
$testimonial_style=$style;
switch($testimonial_style)
{
    case '1': $testimonial_section = 'testimonial testimonial-1 bg-default-color-2';$text_align=''; break;
    case '2': $testimonial_section = 'testimonial testimonial-2';$text_align=''; break; 
    case '3': $testimonial_section = 'testimonial testimonial-3';$text_align='text-center'; break; 
    case '4': $testimonial_section = 'testimonial testimonial-4';$text_align=''; break; 
}
$testimonial_options = get_theme_mod('wphester_testimonial_content');
if (empty($testimonial_options))
    {
        if (get_theme_mod('home_testimonial_title') != '' || get_theme_mod('home_testimonial_desc') != '' || get_theme_mod('home_testimonial_name') != '' || get_theme_mod('home_testimonial_thumb') != '')
        {
            $home_testimonial_title = get_theme_mod('home_testimonial_title');
            $home_testimonial_discription = get_theme_mod('home_testimonial_desc');
            $home_testimonial_client_name = get_theme_mod('home_testimonial_name');
            $home_testimonial_designation = get_theme_mod('home_testimonial_designation');
            $home_testimonial_star=get_theme_mod('home_testimonial_star');
            $home_testimonial_link = get_theme_mod('home_testimonial_link');
            $home_testimonial_image = get_theme_mod('home_testimonial_thumb');
            $testimonial_options = json_encode( array(
                                        array(
                                        'title' => !empty($home_testimonial_title) ? $home_testimonial_title : 'Exellent Theme & Very Fast Support',
                                        'text' => !empty($home_testimonial_discription) ? $home_testimonial_discription :'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                                        'clientname' => !empty($home_testimonial_client_name) ? $home_testimonial_client_name : __('Amanda Smith', 'thecompany-plus'),
                                        'designation' => !empty($home_testimonial_designation) ? $home_testimonial_designation : __('Developer', 'thecompany-plus'),
                                        'home_testimonial_star' => !empty($home_testimonial_star) ? $home_testimonial_star : '4.5',
                                        'link' => !empty($home_testimonial_link) ? $home_testimonial_link : '#',
                                        'image_url' =>  !empty($home_testimonial_image) ? $home_testimonial_image : WPHESTERP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                                        'open_new_tab' => 'no',
                                        'id' => 'customizer_repeater_56d7ea7f40b96',
                                        'home_slider_caption' => 'customizer_repeater_star_',
                                        ),
                                    ));
        }
        else
        {
            $home_testimonial_section_title = get_theme_mod('home_testimonial_section_title');
            $home_testimonial_section_discription = get_theme_mod('home_testimonial_section_discription');
            $designation = get_theme_mod('designation');
            $home_testimonial_thumb = get_theme_mod('home_testimonial_thumb');
            $testimonial_options = json_encode(array(
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Amanda Smith', 'wphester-plus'),
                        'designation' => __('Developer', 'wphester-plus'),
                        'home_testimonial_star' => '4.5',
                        'link' => '#',
                        'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_77d7ea7f40b96',
                        'home_slider_caption' => 'customizer_repeater_star_4.5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Travis Cullan', 'wphester-plus'),
                        'designation' => __('Team Leader', 'wphester-plus'),
                        'home_testimonial_star' => '5',
                        'link' => '#',
                        'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_88d7ea7f40b97',
                        'home_slider_caption' => 'customizer_repeater_star_5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Victoria Wills', 'wphester-plus'),
                        'designation' => __('Volunteer', 'wphester-plus'),
                        'home_testimonial_star' => '3.5',
                        'link' => '#',
                        'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                        'id' => 'customizer_repeater_11d7ea7f40b98',
                        'open_new_tab' => 'no',
                        'home_slider_caption' => 'customizer_repeater_star_3.5',
                    ),
                ));
        }
    }
if(strtolower($format)=="slide")
{    
$testimonial_animation_speed = get_theme_mod('testimonial_animation_speed', 3000);
$testimonial_smooth_speed = get_theme_mod('testimonial_smooth_speed', 1000);
$isRTL = (is_rtl()) ? (bool) true : (bool) false;

$slide_items = $items;
$testimonial_nav_style = get_theme_mod('testimonial_nav_style', 'bullets');
if($testimonial_style!=1){
$designId='testimonial-carousel'.$testimonial_style;
}else{
$designId='testimonial-carousel';
}
$testimonial_settings = array('design_id' => '#' . $designId, 'slide_items' => $slide_items, 'animationSpeed' => $testimonial_animation_speed, 'smoothSpeed' => $testimonial_smooth_speed, 'testimonial_nav_style' => $testimonial_nav_style, 'rtl' => $isRTL);

wp_register_script('wphester-testimonial', WPHESTERP_PLUGIN_URL . '/inc/js/front-page/testi.js', array('jquery'));
wp_localize_script('wphester-testimonial', 'testimonial_settings', $testimonial_settings);
wp_enqueue_script('wphester-testimonial');
}
$home_testimonial_section_title = get_theme_mod('home_testimonial_section_title', __('Our Happy Customers', 'wphester-plus'));
$home_testimonial_section_discription = get_theme_mod('home_testimonial_section_discription', __('What Our Client Says', 'wphester-plus'));

$testimonial_callout_background = get_theme_mod('testimonial_callout_background',WPHESTERP_PLUGIN_URL.'/inc/images/bg/img.png');
$testi_layout=$testimonial_style;
$callout_cta2_background = get_theme_mod('callout_cta2_background', WPHESTERP_PLUGIN_URL.'/inc/images/bg/cta-3.jpg');
if ($testimonial_callout_background != '') { ?>
<section class="section-space <?php echo $testimonial_section;?> slideitem-<?php echo get_theme_mod('home_testimonial_slide_item',1); ?> <?php echo $shortitem; if(strtolower($format)=="grid"){echo ' testi1-grid';}?> bg-default-lite" style="background-image:url('<?php echo esc_url($testimonial_callout_background); ?>'); background-repeat: no-repeat; background-position: top left; width: 100%; background-size: cover;" >
<?php } else { ?>
<section class="section-space <?php echo $testimonial_section; if($testimonial_style==2) {?> testi testi-2 <?php } ?> slideitem-<?php echo get_theme_mod('home_testimonial_slide_item',1); ?> <?php if($testimonial_style==4) {?> testi testi-4 <?php } ?>bg-default-lite" >
<?php } ?>
    <div class="wphester-tesi-container container">
    <?php if ($home_testimonial_section_title != '' || $home_testimonial_section_discription != '') { ?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="section-header">                
                <?php if($home_testimonial_section_title):?>
                    <h2 class="section-title"><?php echo esc_attr($home_testimonial_section_title); ?></h2>                    
                <?php endif;?>
                <?php if ($home_testimonial_section_discription != ''):?>
                    <p class="section-subtitle"><?php echo esc_attr($home_testimonial_section_discription); ?></p>
                <?php endif;?> 
                </div>
            </div>
        </div>
    <?php } ?>
     
        <div class="row" <?php if(strtolower($format)=="grid"){?> id="testimonial-carousel<?php if($testimonial_style!=1){ echo $testimonial_style;}?>"<?php } ?>>            
            <?php
            $testimonial_options = json_decode($testimonial_options);
            if ($testimonial_options != '') {
                $allowed_html = array(
                                        'br' => array(),
                                        'em' => array(),
                                        'strong' => array(),
                                        'b' => array(),
                                        'i' => array(),
                                    );
            if(strtolower($format)=="slide"){?>
            <div class="owl-carousel owl-theme col-md-12'"  id="testimonial-carousel<?php if($testimonial_style!=1){ echo $testimonial_style;}?>">
            <?php
            }
            foreach ($testimonial_options as $testimonial_iteam){
                $title = !empty($testimonial_iteam->title) ? apply_filters('wphester_translate_single_string', $testimonial_iteam->title, 'Testimonial section') : '';
                $test_desc = !empty($testimonial_iteam->text) ? apply_filters('wphester_translate_single_string', $testimonial_iteam->text, 'Testimonial section') : '';
                $test_link = $testimonial_iteam->link;
                $open_new_tab = $testimonial_iteam->open_new_tab;
                $clientname = !empty($testimonial_iteam->clientname) ? apply_filters('wphester_translate_single_string', $testimonial_iteam->clientname, 'Testimonial section') : '';
                $designation = !empty($testimonial_iteam->designation) ? apply_filters('wphester_translate_single_string', $testimonial_iteam->designation, 'Testimonial section') : '';
                $stars = !empty($testimonial_iteam->home_testimonial_star) ? apply_filters('wphester_translate_single_string', $testimonial_iteam->home_testimonial_star, 'Testimonial section') : '';
                     if(strtolower($format)=="grid") {
                    echo '<div class="'.$col.' team-grid">';
                }
                    //Below Code will Run For Testimonial Design 1
                    if($testimonial_style==1) { ?>
                        <div class="testimonial-block">
                            <figure class="avatar">
                                <img src="<?php echo $testimonial_iteam->image_url; ?>" alt="img" class="img-fluid"/>
                            </figure>
                            <?php if ($test_desc != '') { ?>
                                <div class="entry-content">                                
                                    <?php if (!empty($test_desc)): ?>
                                    <h4 class="name"><?php echo wp_kses(html_entity_decode($test_desc), $allowed_html); ?></h4>
                                    <?php endif; 
                                     if (($clientname != '' || $designation != '')) { ?>
                                        <p><a href="<?php if (empty($test_link)) { echo '#'; } else { echo $test_link; } ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>
                                                <?php echo '- '.$clientname; ?>
                                            </a><br><?php echo $designation; ?></p>
                                    <?php } ?>
                                </div>
                            <?php }?>
                        </div>                    
                    <?php }
                    else{
                         ?>
                <blockquote class="testimonial-block">
                    <?php if ($testimonial_iteam->image_url != ''): ?>
                    <figure class="avatar">
                        <img src="<?php echo $testimonial_iteam->image_url; ?>" class="img-fluid rounded-circle" >
                    </figure>
                    <?php endif;

                    if (!empty($test_desc)):
                    if($testimonial_style!=2):
                    if (($clientname != '' || $designation != '')) { ?>
                        <figcaption>
                          <?php if (!empty($clientname)): ?>
                            <a href="<?php if (empty($test_link)) { echo '#'; } else { echo $test_link; } ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>   
                        <h5 class="name  <?php echo $text_align;?>"><?php echo $clientname; ?></h5></a><?php endif; ?>
                        <?php if (!empty($designation)): ?><h6 class="designation  <?php echo $text_align;?>"><?php echo $designation; ?></h6><?php endif; ?>
                    </figcaption>
                    <?php } 
                    endif;?>  
                    <div class="entry-content">
                        <p><?php echo wp_kses(html_entity_decode($test_desc), $allowed_html); ?></p>
                    </div> 
                    <?php
                    if($testimonial_style==2):
                    if (($clientname != '' || $designation != '')) { ?>
                        <figcaption class="testimonial-dasignation">
                          <?php if (!empty($clientname)): ?>
                            <a href="<?php if (empty($test_link)) { echo '#'; } else { echo $test_link; } ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>   
                        <cite class="name"><?php echo $clientname; ?></cite></a><?php endif; ?>
                        <?php if (!empty($designation)): ?><span class="designation"><?php echo $designation; ?></span><?php endif; ?>
                    </figcaption>
                    <?php } 
                    endif;       
                    endif;?>
                                     
                </blockquote>
                <?php
                }
                if(strtolower($format)=="grid") {
                    echo '</div>';
                } 
                }if(strtolower($format)=="slide") {?>
            </div>
            <?php 
            }
            
            }
            ?>                  
        </div>
    </div>
</section>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}
add_shortcode( 'wphester_testimonial', 'wphester_testimonial_callback' );




//SHORTCODE FOR TEAM SECTION
function wphester_team_callback($atts)
{
ob_start();    
extract(shortcode_atts( array(
      'format' => 'slide',
      'style' => '1',
      'items' =>'3',
      'col' =>'3',
   ), $atts ));
switch($col)
{
    case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
    case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
    case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    default: $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
    
}
switch($items)
{
    case '1': $items = 1; break;
    case '2': $items = 2; break; 
    case '3': $items = 3; break; 
    case '4': $items = 4; break; 
    default: $items = 3 ; break; 
    
}
$team_style=$style;
switch($team_style)
{
    case '1': $team_section = 'team bg-default';$text_align=''; $text_align='text-center'; break; 
    case '2': $team_section = 'team2 bg-default';$text_align='text-center'; break; 
    case '3': $team_section = 'team3 bg-default-lite';$text_align='text-center'; break; 
    case '4': $team_section = 'team4 bg-default';$text_align='text-center'; break;  
}
$team_options = get_theme_mod('wphester_team_content');
if (empty($team_options)) {
    $team_options = starter_team_json();
}
if(strtolower($format)=="slide"):
$team_animation_speed = get_theme_mod('team_animation_speed', 3000);
$team_smooth_speed = get_theme_mod('team_smooth_speed', 1000);
$team_nav_style = get_theme_mod('team_nav_style', 'bullets');
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
$team_items=$items;
$teamsettings = array( 'team_items' => $team_items,'team_animation_speed' => $team_animation_speed, 'team_smooth_speed' => $team_smooth_speed, 'team_nav_style' => $team_nav_style, 'rtl' => $isRTL);
wp_register_script('wphester-team', WPHESTERP_PLUGIN_URL . '/inc/js/front-page/team.js', array('jquery'));
wp_localize_script('wphester-team', 'team_settings', $teamsettings);
wp_enqueue_script('wphester-team');
endif;?>
<section class="section-space <?php echo $team_section;?>">
    <div class="wphester-team-container container">
    <?php
    $home_team_section_title = get_theme_mod('home_team_section_title', __('Our Team Is Very Expert', 'wphester-plus'));
    $home_team_section_discription = get_theme_mod('home_team_section_discription', __('Our Expert Team Members', 'wphester-plus'));
    if (($home_team_section_title!= '') || ($home_team_section_discription != '')) {?>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="section-header">
                <?php                
                if(!empty($home_team_section_title)):?><h2 class="section-title"><?php echo $home_team_section_title; ?></h2><?php endif;
                if(!empty($home_team_section_discription)):?><p class="section-subtitle"><?php echo $home_team_section_discription; ?></p>
                <?php endif;?>
                </div>
            </div>                      
        </div>
        <?php } ?>
        <div class="row">
            <?php if(strtolower($format)=="slide"):?><div id="team-carousel" class="owl-carousel owl-theme col-lg-12"><?php endif;?>
                <?php
                $team_options = json_decode($team_options);
                if ($team_options != '') {
                    foreach ($team_options as $team_item) {
                        $image = !empty($team_item->image_url) ? apply_filters('wphester_translate_single_string', $team_item->image_url, 'Team section') : '';
                        $title = !empty($team_item->membername) ? apply_filters('wphester_translate_single_string', $team_item->membername, 'Team section') : '';
                        $subtitle = !empty($team_item->designation) ? apply_filters('wphester_translate_single_string', $team_item->designation, 'Team section') : '';
                        $aboutme = !empty($team_item->text) ? apply_filters('wphester_translate_single_string', $team_item->text, 'Team section') : '';
                       // $link = !empty($team_item->link) ? apply_filters('wphester_translate_single_string', $team_item->link, 'Team section') : '';
                        $open_new_tab = $team_item->open_new_tab;
                        ?>
                    <div class="<?php if(strtolower($format)=="slide") { echo 'item';} else { echo $col .' '.'team-grid-col'; } ?>">
                        <div class="team-grid <?php echo $text_align;?>" tabindex="0" id="A1">
                            <?php if($team_style=="2") { ?>   
                                <div class="overlay">      
                            <?php } 
                            if(!empty($image)){ ?>
                            <div class="img-holder"> <img src="<?php echo esc_url($image); ?>" class="img-fluid">
                            <?php }
                            if($team_style=="1"){ 
                                //Below Script will run Only For Design 4   
                                $icons = html_entity_decode($team_item->social_repeater);
                                    $icons_decoded = json_decode($icons, true);
                                    $socails_counts = $icons_decoded;
                                    if (!empty($socails_counts)) :
                                        if (!empty($icons_decoded)) : ?>
                                             <div class="social-share">
                                    <div class="slideouticons">
                                        <label class="mainlabel"><i class="fa fa-share-alt"></i></label>
                                        <div class="icon-wrapper">
                                            <ul class="list-inline list-unstyled ml-0 mt-3 mb-1">
                                                <?php
                                                foreach ($icons_decoded as $value) 
                                                {
                                                $social_icon = !empty($value['icon']) ? apply_filters('wphester_translate_single_string', $value['icon'], 'Team section') : '';
                                                $social_link = !empty($value['link']) ? apply_filters('wphester_translate_single_string', $value['link'], 'Team section') : '';
                                                    if (!empty($social_icon)) 
                                                    { ?>                          
                                                        <li class="list-inline-item"><a class="fa-lg fb-ic" <?php if ($open_new_tab == 'yes') { echo 'target="_blank"'; } ?> href="<?php echo esc_url($social_link); ?>" class="btn btn-just-icon btn-simple"><i class="fa <?php echo esc_attr($social_icon); ?> " aria-hidden="true"></i></a></li>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                            </ul>
                                            </div>
                                    </div>
                                </div>
                                             <?php
                                        endif;
                                    endif;
                                }?>
                                </div>
                                <?php 
                                if($team_style=="2"){                         
                                $icons = html_entity_decode($team_item->social_repeater);
                                $icons_decoded = json_decode($icons, true);
                                $socails_counts = $icons_decoded;
                                if (!empty($socails_counts)) :
                                    if (!empty($icons_decoded)) : ?>
                                        <ul class="list-inline list-unstyled ml-0 mt-3 mb-1" id="A2">
                                            <?php
                                            foreach ($icons_decoded as $value) 
                                            {
                                            $social_icon = !empty($value['icon']) ? apply_filters('wphester_translate_single_string', $value['icon'], 'Team section') : '';
                                            $social_link = !empty($value['link']) ? apply_filters('wphester_translate_single_string', $value['link'], 'Team section') : '';
                                                if (!empty($social_icon)) 
                                                { ?>                          
                                                    <li class="list-inline-item"><a class="p-2 fa-lg fb-ic" <?php if ($open_new_tab == 'yes') { echo 'target="_blank"'; } ?> href="<?php echo esc_url($social_link); ?>" class="btn btn-just-icon btn-simple"><i class="fa <?php echo esc_attr($social_icon); ?> " aria-hidden="true"></i></a></li>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                <?php
                                    endif;
                                endif;
                            }
                            if($team_style=="2") { ?>  </div> <?php }
                            ?>
                                <div class="card-body">
                                <?php if (!empty($title)) : ?>
                                <h4 class="<?php if(($team_style!="3") && ($team_style!="4")):?>name<?php endif;?> mt-1 mb-2"><?php echo esc_html($title); ?></h4>
                                <?php endif; 
                                if (!empty($subtitle)) : ?>
                                <p class="mt-1 mb-2"><?php echo esc_html($subtitle); ?></p>
                                <?php endif;  
                                //Below Script will run apart Design 4
                                if($team_style!="1"){                         
                                $icons = html_entity_decode($team_item->social_repeater);
                                $icons_decoded = json_decode($icons, true);
                                $socails_counts = $icons_decoded;
                                if (!empty($socails_counts)) :
                                    if (!empty($icons_decoded)) : ?>
                                        <ul class="list-inline list-unstyled ml-0 mt-3 mb-1" id="A2">
                                            <?php
                                            foreach ($icons_decoded as $value) 
                                            {
                                            $social_icon = !empty($value['icon']) ? apply_filters('wphester_translate_single_string', $value['icon'], 'Team section') : '';
                                            $social_link = !empty($value['link']) ? apply_filters('wphester_translate_single_string', $value['link'], 'Team section') : '';
                                                if (!empty($social_icon)) 
                                                { ?>                          
                                                    <li class="list-inline-item"><a class="p-2 fa-lg fb-ic" <?php if ($open_new_tab == 'yes') { echo 'target="_blank"'; } ?> href="<?php echo esc_url($social_link); ?>" class="btn btn-just-icon btn-simple"><i class="fa <?php echo esc_attr($social_icon); ?> " aria-hidden="true"></i></a></li>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                <?php
                                    endif;
                                endif;
                            }
                                ?>        
                            </div>                            
                        </div>
                    </div>
                    <?php
                    }
                }
            if(strtolower($format)=="slide"):?></div><?php endif;?>                       
        </div>
    </div>
</section>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}

add_shortcode( 'wphester_team', 'wphester_team_callback' );


//SHORTCODE FOR FUN FACT SECTION
function wphester_funfact_callback($atts){
    ob_start();
    extract(shortcode_atts( array(
      'col' => '4',
   ), $atts ));
    switch($col)
    {   
        case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
        case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
        case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
        case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
        default:$col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    } 
$home_fun_title = get_theme_mod('funfact_section_title', __('We Have Random And <br> Interesting Facts.', 'wphester-plus'));
$home_fun_subtitle = get_theme_mod('funfact_section_subtitle', __('We are ready to provide you with any financial, legal and auditing help as <br> well as prepare a business plan.', 'wphester-plus'));

$funfact_data = get_theme_mod('wphester_funfact_content');
if (empty($funfact_data)) {
    $funfact_data = starter_funfact_json();
}    

    $fun_callout_background = get_theme_mod('funfact_callout_background', WPHESTERP_PLUGIN_URL .'inc/images/bg/funfact-bg.jpg');
    if ($fun_callout_background != '') {?>
        <section class="section-space funfact bg-default"  style="background-image:url('<?php echo esc_url($fun_callout_background); ?>'); background-repeat: no-repeat; width: 100%; background-size: cover;">
        <?php
    } 
    else {
        ?>
        <section class="section-space funfact">
            <?php
    }?>
    <div class="overlay">
    <div class="wphester-fun-container container">
        <?php if(($home_fun_title) || ($home_fun_subtitle) != '') {?>
        <div class="row">
            <div class="col-lg-12">
                <div class="funfact-block">
                    <h2 class="title text-white"><?php echo _e($home_fun_title);?></h2>
                    <p class="text-white"><?php echo _e($home_fun_subtitle);?></p>
                </div>
            </div>
        </div>
            <?php
        }?>
        <div class="row fun-content">
        <?php
        $funfact_data = json_decode($funfact_data);
        if (!empty($funfact_data)) {
            $fun_id=1;
            foreach ($funfact_data as $funfact_iteam) {
                $funfact_icon = !empty($funfact_iteam->icon) ? apply_filters('wphester_translate_single_string', $funfact_iteam->icon, 'Funfact section') : '';
                $funfact_title = !empty($funfact_iteam->title) ? apply_filters('wphester_translate_single_string', $funfact_iteam->title, 'Funfact section') : '';
                $funfact_text = !empty($funfact_iteam->text) ? apply_filters('wphester_translate_single_string', $funfact_iteam->text, 'Funfact section') : '';
                $funfact_icon = !empty($funfact_iteam->icon_value) ? apply_filters('wphester_translate_single_string', $funfact_iteam->icon_value, 'Funfact section') : '';
                if($fun_id==1){$fun_count='-up';}else{$fun_count=$fun_id-1;}?>
                    <div class="<?php echo $col;?>">
                        <div class="funfact-inner">
                            <?php if ($funfact_title != '') { ?>
                            <h4 class="funfact-title mb-0 count<?php echo $fun_count;?>" data-from="0" data-to="<?php echo $funfact_title; ?>" data-time="2000"><?php echo $funfact_title; ?></h4>
                            <?php } ?>
                            <?php if ($funfact_text != '') { ?>
                            <p class="description mb-1"><?php echo $funfact_text; ?></p>
                            <?php } ?>
                        </div>
                    </div>
            <?php
            $fun_id++;
                    }
                }
            ?>
        </div>
    </div>
</div>
    </div>
</section>
<script type="text/javascript">
    (function($) {
            $.fn.counter = function() {
                const $this = $(this),
                    numberFrom = parseInt($this.attr('data-from')),
                    numberTo = parseInt($this.attr('data-to')),
                    delta = numberTo - numberFrom,
                    deltaPositive = delta > 0 ? 1 : 0,
                    time = parseInt($this.attr('data-time')),
                    changeTime = 10;

                let currentNumber = numberFrom,
                    value = delta * changeTime / time;
                var interval1;
                const changeNumber = () => {
                    currentNumber += value;
                    //checks if currentNumber reached numberTo
                    (deltaPositive && currentNumber >= numberTo) || (!deltaPositive && currentNumber <= numberTo) ? currentNumber = numberTo: currentNumber;
                    this.text(parseInt(currentNumber));
                    currentNumber == numberTo ? clearInterval(interval1) : currentNumber;
                }
                interval1 = setInterval(changeNumber, changeTime);
            }
        
        var a = 0;
        $(window).scroll(function() {
            var oTop = $('.funfact-inner').offset().top - window.innerHeight;
            if (a == 0 && $(window).scrollTop() > oTop) {
                $('.count-up').counter();
                $('.count1').counter();
                $('.count2').counter();
                $('.count3').counter();

                setTimeout(function() {
                    $('.count5').counter();
                }, 3000);
                a = 1;
            }
        });}(jQuery));
</script>
<?php

$stringa = ob_get_contents();
ob_end_clean();
return $stringa;  
}
add_shortcode('wphester_funfact','wphester_funfact_callback');


//SHORTCODE FOR CLIENT SECTION
function wphester_client_callback($atts)
{
ob_start();
extract(shortcode_atts( array(
  'items' => '4',
), $atts ));
switch($items)
{   
    case '1': $items = '1'; break;
    case '2': $items = '2'; break; 
    case '3': $items = '3'; break; 
    case '4': $items = '4'; break;
    case '5': $items = '5'; break; 

    default:$items = '4'; break; 
}  
$client_nav_style = get_theme_mod('client_nav_style', 'navigation');
$client_slide_items=$items;
$client_animation_speed = get_theme_mod('client_animation_speed', 3000);
$client_smooth_speed = get_theme_mod('client_smooth_speed', 1000); 
if(!is_rtl()):
    $navdir= ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"];
else:
    $navdir= ["<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"];
endif;
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
$sponsorssettings = array( 'slide_items' => $client_slide_items, 'client_animation_speed' => $client_animation_speed, 'client_smooth_speed' => $client_smooth_speed, 'client_nav_style' => $client_nav_style,'rtl' => $isRTL, 'navdir' => $navdir);

wp_register_script('wphester-sponsors', WPHESTERP_PLUGIN_URL . '/inc/js/front-page/sponsors.js', array('jquery'));
wp_localize_script('wphester-sponsors', 'sponsorssettings', $sponsorssettings);
wp_enqueue_script('wphester-sponsors');

$home_client_section_title=get_theme_mod('home_client_section_title', 'They Trust Us');
$home_client_section_subtitle=get_theme_mod('home_client_section_subtitle', 'You Grow We Grow');
$home_client_section_discription=get_theme_mod('home_client_section_discription', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore <br> magna aliqua quis ips gravida Risus commodo.');

$client_options = starter_client_json();
$clt_bg_color = get_theme_mod('clt_bg_color', '#111111');
$enable_clt_bg_color = get_theme_mod('enable_clt_bg_color', false);
if ($enable_clt_bg_color == true) { ?>
<section class="section-space sponsors bg-default-lite"  style="background-color: <?php echo $clt_bg_color; ?>">
<?php } 
else { ?>
<section class="section-space sponsors bg-default-lite" >
<?php } ?>
    <div class="wphester-client-container container"> 
        <?php if (($home_client_section_title!= '') || ($home_client_section_subtitle!= '')|| ($home_client_section_discription!= '')) {?> 
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="section-header">
                     <?php if ($home_client_section_title != ''){?><h2 class="section-title"><?php echo esc_html($home_client_section_title);?></h2><?php } ?>
                    <?php if ($home_client_section_subtitle!= ''){?><p class="section-subtitle"><?php echo esc_html($home_client_section_subtitle)?></p><?php } ?>
                    <?php if ($home_client_section_discription!= ''){?><p class="description"><?php echo wp_kses_post($home_client_section_discription)?></p><?php } ?>
                </div>
            </div>
        </div> 
        <?php } ?>     
        <div class="row">
            <div id="clients-carousel" class="owl-carousel owl-theme col-md-12">
                <?php
                $t = true;
                $client_options = json_decode($client_options);
                        if ($client_options != '') {
                            foreach ($client_options as $client_iteam) {
                                $client_image = !empty($client_iteam->image_url) ? apply_filters('wphester_translate_single_string', $client_iteam->image_url, 'Client section') : '';
                                $client_link = !empty($client_iteam->link) ? apply_filters('wphester_translate_single_string', $client_iteam->link, 'Client section') : '';
                                $open_new_tab = $client_iteam->open_new_tab;
                                ?>      
                                <div class="item">      
                                    <figure <?php if ($client_image != '') { ?>class="logo-scroll"<?php } ?>>
                                        <?php
                                        if (empty($client_link)) {
                                            ?>
                                            <img src="<?php echo $client_image; ?>" class="img-fluid" >
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo $client_link; ?>" <?php
                                            if ($open_new_tab == 'yes') {
                                                echo 'target="_blank"';
                                            }
                                            ?>>
                                                <img src="<?php echo $client_image; ?>" class="img-fluid" >
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </figure>
                                </div>      
                                <?php
                            }
                        }
                        else {
                            for ($i = 1; $i <= 4; $i++) {
                                ?>  
                                <div class="item">
                                    <figure class="logo-scroll">
                                        <a href="#"><img src="<?php
                                            echo WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-' . $i . '.png';
                                            ?>" class="img-fluid" alt="Sponsors <?php echo $i; ?>"></a>
                                    </figure>
                                </div>
                                <?php
                            }
                        }?>
            </div>
        </div>
    </div>
</section>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;  
}
add_shortcode('wphester_client','wphester_client_callback');

//SHORTCODE FOR ABOUT
function wphester_about_callback(){
ob_start();    
$about_image = WPHESTERP_PLUGIN_URL.'/inc/images/about.jpg';
$default = '<div class="row">
                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="about-block">
                            <div class="section-header">
                                <h2 class="section-title">'. esc_html__('We Are A Successful & Sustainable Consulting','wphester-plus').'</h2>
                                <p class="section-subtitle">'. esc_html__('Our Introduction','wphester-plus').'</p>
                            </div>
                            <div class="entry-content">
                                <p>'. esc_html__('We are ready to provide you with any financial, legal and auditing help as well as prepare a business plan','wphester-plus').'</p>
                                <ul class="list-style-four">
                                    <li>'. esc_html__('Business, Strategic and Succession Planning','wphester-plus').'</li>
                                    <li>'. esc_html__('Human Resources Consulting','wphester-plus').'</li>
                                    <li>'. esc_html__('Business Expansion & Acquisitions','wphester-plus').'</li>
                                    <li>'. esc_html__('Marketing & Sales Including E-Commerce','wphester-plus').'</li>
                                </ul>
                                <a href="#" class="btn-small btn-color-2 mt-4" alt="Check-it-out">'. esc_html__('ABOUT US','wphester-plus').'</a>
                            </div>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Featured image-->
                    <div class="col-md-6">
                        <div class="image-box">
                            <figure class="thumbnail">
                                <div id="counter" class="counter-content">
                                    <p class="iq-counter-info">
                                        <span class="timer" data-to="23" data-speed="5000">'. esc_html__('23','wphester-plus').'</span>
                                    </p>
                                    <p class="counter-content-text">'. esc_html__('Years Experience in Consulting','wphester-plus').'</p>
                                </div>
                                <img src="'.esc_url($about_image).'" class="img-fluid" alt="img1">
                            </figure>
                            <div class="about-shadow"></div>
                        </div>
                    </div>';

        $about_section_content = get_theme_mod('about_section_content',$default);
        ?> 
        <section class="about-section bg-default-color">
            <div class="wphester-about-container container">
                <?php echo $about_section_content;?> 
            </div>
        </section> 
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa;
}
add_shortcode( 'wphester_about', 'wphester_about_callback' );

//SHORTCODE FOR Portfolio SECTION
function wphester_portfolio_callback($atts){
ob_start();
extract(shortcode_atts( array(
    'filter' => 'on',
    'sidebar' => 'off',
    'margin' => 'on',
    'col' => '3',
    'pagination' => 'on',
    'projects_per_page' => '4',
), $atts ));
if(($col=='4')&&(strtolower($sidebar)!='off')){
    $port_class='port-4';
}else{
     $port_class='';
}
if($col=='2'):$portfolio_class='portfolio2';
elseif($col=='3'):$portfolio_class='portfolio3';
elseif($col=='4' && $col!='sidebar.php'):$portfolio_class='portfolio4';
elseif($col=='4' && $col=='sidebar.php'):$portfolio_class='portfolio4 p4l';
else:$portfolio_class='portfolio';
endif;
switch($col)
    {   
        case '1': $col = 'col-md-12 col-sm-12 col-xs-12'; break;
        case '2': $col = 'col-md-6 col-sm-6 col-xs-12'; break; 
        case '3': $col = 'col-md-4 col-sm-6 col-xs-12'; break; 
        case '4': $col = 'col-md-3 col-sm-6 col-xs-12'; break; 
        default:$col = 'col-md-3 col-sm-6 col-xs-12'; break; 
    }  
switch(strtolower($sidebar))
    {   
        case 'left': $sidebar = 'left'; break;
        case 'right': $sidebar = 'right'; break;
        case 'off': $sidebar = 'off'; break;          
        default:$sidebar = 'off'; break; 
    }   
$post_type = 'wphester_portfolio';
$tax = 'portfolio_categories';
$term_args = array('hide_empty' => true, 'orderby' => 'id');
$posts_per_page = $projects_per_page;
$tax_terms = get_terms($tax, $term_args);
$defualt_tex_id = get_option('wphester_default_term_id');
$j = 1;$tab='';
if(isset($_GET['tab'])):
   $tab = $_GET['tab'];
endif;
if (isset($_GET['div'])) {
    $tab = $_GET['div'];
}
$porfolio_page_title = get_theme_mod('porfolio_page_title', __('Our Portfolio', 'wphester-plus'));
$porfolio_page_subtitle = get_theme_mod('porfolio_page_subtitle', __('Our Recent Works', 'wphester-plus'));
global $template;?>
<section class="section-space portfolio bg-default <?php echo $portfolio_class;?> <?php echo $port_class; if(strtolower($margin)=='off'){ echo 'portfolio-gallery'; }?>  ">
    <div class="container<?php echo esc_html(wphester_container());?>">
        <?php  if($sidebar=="left" && (basename($template)=='template-shortcode.php')) { ?>  
        <div class="row">
            <!--Sidebar-->
            <div class="col-lg-4 col-md-5 col-sm-12">
                <div class="sidebar s-r-space">
                    <?php dynamic_sidebar('sidebar-1'); ?>
                </div>
            </div>
            <!--/Sidebar-->
            <div class="col-lg-8 col-md-7 col-sm-12">
        <?php }?>
       <?php  if($sidebar=="right") { ?> 
        <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-12">
        <?php } ?>
        <?php if (!empty($porfolio_page_title) || !empty($porfolio_page_subtitle)): ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header">
                        <?php if (!empty($porfolio_page_title)): ?><h2 class="section-title"><?php echo $porfolio_page_title; ?></h2>          
                        <?php endif; ?>
                        <?php if (!empty($porfolio_page_subtitle)): ?><p class="section-subtitle"><?php echo $porfolio_page_subtitle; ?></p><?php endif; ?>                       
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- Portfolio Filter -->
        <?php 
        if(strtolower($filter)=='on'){
           if ($tax_terms) : ?>
            <div class="row">
                <div class="col-12">
                    <!-- Nav tabs -->
                    <div class="tab-filter-wrapper">
                        <div class="tab-filter">
                            <ul class="nav md-pills flex-center flex-wrap mx-0" role="tablist">
                                <?php foreach ($tax_terms as $tax_term) { ?>
                                    <li class="nav-item"><span class="tab">
                                        <a id="tab-<?php echo rawurldecode($tax_term->slug); ?>" href="#<?php echo rawurldecode($tax_term->slug); ?>"  class="nav-link <?php
                                        if ($tab == '') {
                                            if ($j == 1) {
                                                echo 'active';
                                                $j = 2;
                                            }
                                        } else if ($tab == rawurldecode($tax_term->slug)) {
                                            echo 'active';
                                        }
                                        ?> text-uppercase" data-toggle="tab" role="tab"><?php echo $tax_term->name; ?></a>
                                    </span></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            endif; 
        }?>

        <div align="center" id="myDiv" style="display:none;">
            <img id="loading-image" width="120" src="<?php echo esc_url(WPHESTERP_PLUGIN_URL.'/inc/images/loading.gif');?>"  />
            <p>Loading...</p>
        </div>

        <!--Tab panels-->
        
        <div id="content" class="tab-content <?php if(strtolower($filter)=='off'): echo 'pt-0'; endif ?>" role="tablist">
            <?php
            global $paged;
            $curpage = $paged ? $paged : 1;
            $is_active = true;
            if ($tax_terms) {
                foreach ($tax_terms as $tax_term) {
                    if($filter=='off'){
                        $args = array(
                            'post_type' => $post_type,
                            'post_status' => 'publish',
                            'posts_per_page' => $posts_per_page,
                            'paged' => $curpage,
                            'orderby' => 'DESC',
                        );
                    }else{
                        $args = array(
                            'post_type' => $post_type,
                            'post_status' => 'publish',
                            'portfolio_categories' => $tax_term->slug,
                            'posts_per_page' => $posts_per_page,
                            'paged' => $curpage,
                            'orderby' => 'DESC',
                        );
                    }
                    $portfolio_query = null;
                    $portfolio_query = new WP_Query($args);
                    if ($portfolio_query->have_posts()):
                        ?>
                        <div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade show in <?php
                        if ($tab == '') {
                            if ($is_active == true) {
                                echo 'active';
                            }$is_active = false;
                        } else if ($tab == rawurldecode($tax_term->slug)) {
                            echo 'active';
                        }
                        ?>" role="tabpanel" aria-labelledby="tab-<?php echo rawurldecode($tax_term->slug); ?>">
                            <div class="row">
                                <?php
                                while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
                                    $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                                        $portfolio_description = get_the_content();
                                    if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                                        $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                                    } else {
                                        $portfolio_link = '';
                                    }?>
                                    <!-- Grid column -->
                        <div class="<?php echo $col;?>">
                         <!-- Card -->
                            <div class="card">
                                <figure class="portfolio-thumbnail">
                                    <?php
                                    the_post_thumbnail('full', array('class' => 'img-fluid'));
                                    if (has_post_thumbnail()) {
                                        $post_thumbnail_id = get_post_thumbnail_id();
                                        $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                    }

                                    if (!empty($portfolio_link)) {
                                        $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';

                                        $portlink = '<a href=' . "$portfolio_link" . ' title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                    } else {
                                        $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';
                                        $portlink ='<a href="#" title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                    }
                                    
                                    $modelId = get_the_ID() . '_model'.rawurldecode($tax_term->slug);
                                    ?>
                                    <a data-toggle="modal" data-target="#<?php echo $modelId; ?>"><i>+</i></a>
                                </figure>

                                <figcaption>
                                    <?php if(!empty($portlink)):?>
                                        <div class="entry-header">
                                            <h4 class="entry-title">
                                                <?php echo $portlink; ?>
                                            </h4>
                                        </div>
                                    <?php endif;
                                     $tax_string=implode(" ",get_the_taxonomies());
                                    $portfolio_search = array('Project categories:', ', and', 'and', ',', '.');
                                    $portfolio_replace = array('', ' / ', ' / ', ' / ','');
                                    $tax_cat=str_replace($portfolio_search, $portfolio_replace , $tax_string);?>
                                    <p class="taxonomy-list"><?php echo $tax_cat;?></p>
                                </figcaption>
                            </div>
                            <!-- Card -->                            
                        </div>
                        <!-- /Grid column -->
                        <!-- Modal -->
                            <div class="modal fade" id="<?php echo $modelId;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body p-0">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            
                                            <!-- Grid row -->
                                            <div class="row">
                                            
                                                <!-- Grid column -->
                                                <div class="col-md-6 py-5 pl-5">                                                 
                                                    <article class="post text-center">
                                                        <div class="entry-header">
                                                            <h2 class="entry-title"><?php echo $portlink; ?></h2>
                                                        </div>
                                                        <?php
                                                        if(the_content()){?>                                                        
                                                        <div class="entry-content">
                                                                <p><?php echo get_the_content(); ?></p>
                                                        </div>
                                                        <?php }?>                                                        
                                                    </article>                                                    
                                                </div>
                                                <!-- Grid column -->
                                            
                                                <!-- Grid column -->
                                                <div class="col-md-6">
                                                    <div class="view rounded-right">
                                                        <img class="img-fluid" src="<?php echo $post_thumbnail_url; ?>" alt="<?php the_title();?>">
                                                    </div>
                                                </div>
                                                <!-- Grid column --> 
                                            </div>
                                            <!-- Grid row -->                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Modal -->
                                    <?php endwhile; ?>
                             </div>
                             <?php
                             if(strtolower($pagination)=='on'){
                                echo '<div class="center">';
                                $total = $portfolio_query->found_posts;
                                $WPHester_Portfolio_Page = new WPHester_Portfolio_Page();
                                $WPHester_Portfolio_Page->WPHester_Port_Page($curpage, $portfolio_query, $total, $posts_per_page);
                                echo '</div>';
                                }
                                wp_reset_query();?>
                            </div>
                        <?php
                        
                    else:
                        ?>
                        <div id="<?php echo rawurldecode($tax_term->slug); ?>" class="tab-pane fade in <?php
                             if ($tab == '') {
                                 if ($is_active == true) {
                                     echo 'active';
                                 }$is_active = false;
                             } else if ($tab == rawurldecode($tax_term->slug)) {
                                 echo 'active';
                             }
                             ?>"></div>
                         <?php
                         endif;
                     }
                 }
                 ?> 
    </div>      
     <?php  if($sidebar=="left" && (basename($template)=='template-shortcode.php')) { ?>
                </div>
            </div>
                <?php  } 
                 if($sidebar=="right" && (basename($template)=='template-shortcode.php')) { ?>
                </div>
                    <!--Sidebar Widgets-->
                    <div class="col-lg-4 col-md-5 col-sm-12">
                        <div class="sidebar s-r-space">
                            <?php dynamic_sidebar('sidebar-1');?>
                        </div>
                    </div>
                    <!--/Sidebar Widgets-->
            </div>
                <?php } ?>
        </div>      
</section>
<script type="text/javascript">
    jQuery('.lightbox').hide();
    jQuery('.wphester-loader').hide();
    jQuery('#lightbox').hide();
    jQuery(".tab .nav-link ").click(function (e) {
        jQuery("#lightbox").remove();
        var h = decodeURI(jQuery(this).attr('href').replace(/#/, ""));
        var tjk = "<?php the_title(); ?>";
        var str1 = tjk.replace(/\s+/g, '-').toLowerCase();
        var pageurl = "<?php $structure = get_option('permalink_structure');if ($structure == ''){echo get_permalink()."&tab=";}else{echo get_permalink()."?tab=";}?>"+h;
        jQuery.ajax({url: pageurl, beforeSend: function () {
                jQuery(".tab-content").hide();
                jQuery("#myDiv").show();
            }, success: function (data) {
                jQuery(".tab-content").show();
                jQuery('.lightbox').remove();
                jQuery('#lightbox').remove();
                jQuery('#wrapper').html(data);
            }, complete: function (data) {
                jQuery("#myDiv").hide();
            }
        });
        if (pageurl != window.location) {
            window.history.pushState({path: pageurl}, '', pageurl);
        }
        return false;
    });
</script>
<?php
$stringa = ob_get_contents();
ob_end_clean();
return $stringa; 
}
add_shortcode( 'wphester_portfolio', 'wphester_portfolio_callback' );


//Function For Contact Page
function wphester_contact_info_fn(){
    $contact_dt_title = get_theme_mod('contact_dt_title', __("Contact Details", 'wphester-plus'));?>
    <div class="contact-info contact-page">
        <h2 class="title"><?php echo $contact_dt_title;?></h2>
        <?php $contact_data=content_contact_data('wphester_plus_contact_content');
        $contact_data = json_decode($contact_data);
        if (!empty($contact_data))
        { 
        foreach($contact_data as $contact_item)
            { 
                $contact_title = ! empty( $contact_item->title ) ? apply_filters( 'wphester_plus_translate_single_string', $contact_item->title, 'Contact section' ) : '';

                $contact_text = ! empty( $contact_item->text ) ? apply_filters( 'wphester_plus_translate_single_string', $contact_item->text, 'Contact section' ) : '';

                $contact_icon = ! empty( $contact_item->icon_value ) ? apply_filters( 'wphester_plus_translate_single_string', $contact_item->icon_value, 'Contact section' ) : '';?>

                <aside class="contact-widget">
                    <div class="media">         
                        <?php if($contact_icon):?>
                            <div class="contact-icon">
                                    <i class="fa <?php echo $contact_icon; ?>" aria-hidden="true"></i>
                            </div>
                        <?php endif;?>
                        <?php if($contact_title || $contact_text):?>
                            <div class="contact-detail media-body">
                                    <?php if($contact_title):?><h5 class="title"><?php echo $contact_title; ?></h5><?php endif;?>
                                    <?php if($contact_text):?><address><?php echo $contact_text; ?></address><?php endif;?>
                            </div>
                        <?php endif;?>
                    </div> 
                </aside> 
            <?php 
            } 
        }
        $wphester_social_links = get_theme_mod('wphester_social_links');
        $wphester_social_links = json_decode($wphester_social_links);
        echo '<aside class="widget"><ul class="custom-social-icons">';
        if (!empty($wphester_social_links)) {
            
            foreach ($wphester_social_links as $wphester_social_link) {

                $social_icon = !empty($wphester_social_link->icon_value) ? apply_filters('wphester_translate_single_string', $wphester_social_link->icon_value, 'Contact section') : '';
                $contact_link = !empty($wphester_social_link->link) ? apply_filters('wphester_translate_single_string', $wphester_social_link->link, 'Contact section') : '';
                ?>
                <li><a class="social-link" <?php if($wphester_social_link->open_new_tab== 'yes') { echo "target='_blank'"; } ?>  href="<?php echo $contact_link; ?>"><i class="fa <?php echo $social_icon; ?>"></i></a></li>
                <?php
            }
        }
        echo '</ul></aside>';?>                               
    </div>
<?php }
add_action('wphester_contact_info','wphester_contact_info_fn');

function  wphester_contact_shortcode_fn(){
    $contact_cf7_title = get_theme_mod('contact_cf7_title', __("Send Us A Message", 'wphester-plus'));?>
        <div class="contact-form">                            
            <?php if($contact_cf7_title!=''):?>
                <h2 class="title"><?php echo $contact_cf7_title; ?></h2>                                         
            <?php endif;
            if(get_theme_mod('contact_form_shortcode')):            
                 echo do_shortcode(get_theme_mod('contact_form_shortcode')); 
            endif;?>
        </div>
<?php 
}
add_action('wphester_contact_shortcode','wphester_contact_shortcode_fn');

function wphester_contact_map_fn(){?>
    <div id="google-map">
        <?php echo do_shortcode(get_theme_mod('contact_google_map_shortcode')); ?>
    </div>
<?php
}
add_action('wphester_contact_map','wphester_contact_map_fn');

//SHORTCODE FOR CONTACT SECTION
function wphester_contact_callback($atts){
ob_start();
extract(shortcode_atts( array('style' => '1' ), $atts ));
$contact_cf7_title = get_theme_mod('contact_cf7_title', __("Send Us A Message", 'wphester-plus'));
$wphester_social_links = get_theme_mod('wphester_social_links');
 
if (empty($wphester_social_links)) {
    $wphester_social_links = wphester_plus_starter_contact_social_json();
}
switch(strtolower($style))
    {   
        case '1': $style = 1; break;
        case '2': $style = 2; break; 
        case '3': $style = 3; break; 
        case '4': $style = 4; break; 
        default:$style = 1; break; 
    } 
if($style==1):?>    
    <section class="section-space contact-info-par contact1">
        <div class="container<?php echo esc_html(wphester_container());?>">     
            <div class="row">

                <div class="col-lg-8 col-md-7 col-sm-12">
                    <?php 
                    if(get_theme_mod('contact_form_shortcode')):            
                        do_action('wphester_contact_shortcode');
                    endif;?>
                </div>

                <div class="col-lg-4 col-md-5 col-sm-12">                    
                    <?php do_action('wphester_contact_info');?>
                </div>

            </div>            
        </div>
    </section>
    <?php
    if(get_theme_mod('contact_google_map_shortcode')):?>
        <section class="contact-form-map">
            <div class="row">   
                <div class="col-lg-12 col-md-12 col-sm-12"> 
                    <?php do_action('wphester_contact_map');?>
                </div>
            </div>
        </section>
    <?php
    endif;

elseif($style==2):    
    if(get_theme_mod('contact_form_shortcode')):?>
        <section class="section-space contact-info-par contact2 bg-default-lite">
            <div class="container">
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12"> 
                        <?php 
                        do_action('wphester_contact_shortcode');
                        ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif;?>

    <section class="contact-form-map contact2">
        <div class="container<?php echo esc_html(wphester_container());?>">     
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12">
                    <?php if(get_theme_mod('contact_google_map_shortcode')):
                        do_action('wphester_contact_map');
                    endif;?>
                </div>

                <div class="col-lg-4 col-md-5 col-sm-12">
                    <?php do_action('wphester_contact_info');?>
                </div>
                
            </div>
        </div>
    </section>
<?php
elseif($style==3):?>
    <section class="contact-form-map contact3">
        <div class="container<?php echo esc_html(wphester_container());?>">     
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12">
                    <?php if(get_theme_mod('contact_google_map_shortcode')):
                        do_action('wphester_contact_map');
                    endif;?>
                </div>

                <div class="col-lg-4 col-md-5 col-sm-12">
                    <?php do_action('wphester_contact_info');?>
                </div>
                
            </div>
        </div>
    </section>
    <?php
    if(get_theme_mod('contact_form_shortcode')):?>
        <section class="section-space contact-info-par contact3">
            <div class="container">
                <div class="row">   
                    <div class="col-lg-12 col-md-12 col-sm-12"> 
                        <?php 
                        do_action('wphester_contact_shortcode');
                        ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif;

elseif($style==4):
    if(get_theme_mod('contact_form_shortcode')):?>
        <section class="section-space contact-info-par contact1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">                    
                        <?php 
                        do_action('wphester_contact_shortcode');
                        ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif;

    if(get_theme_mod('contact_google_map_shortcode')):?>
        <section class="contact-form-map">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?php do_action('wphester_contact_map');?>
                </div>
            </div>
        </section>
    <?php endif;

    $contact_dt_title = get_theme_mod('contact_dt_title', __("Contact Details", 'wphester-plus'));?>    
    <section class="section-space contact-info-section contact-info contact4">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h2 class="title"><?php echo $contact_dt_title;?></h2>
                </div>
            </div>
            <div class="row">
            <?php $contact_data=content_contact_data('wphester_plus_contact_content');
            $contact_data = json_decode($contact_data);
            if (!empty($contact_data))
            { 
                foreach($contact_data as $contact_item)
                { 
                    $contact_title = ! empty( $contact_item->title ) ? apply_filters( 'wphester_plus_translate_single_string', $contact_item->title, 'Contact section' ) : '';

                    $contact_text = ! empty( $contact_item->text ) ? apply_filters( 'wphester_plus_translate_single_string', $contact_item->text, 'Contact section' ) : '';

                    $contact_icon = ! empty( $contact_item->icon_value ) ? apply_filters( 'wphester_plus_translate_single_string', $contact_item->icon_value, 'Contact section' ) : '';?>

                    <div class="col-lg-4 col-md-6">
                        <aside class="contact-widget text-center">
                            <?php if($contact_icon):?>
                                <figure class="contact-icon">
                                        <i class="fa <?php echo $contact_icon; ?>" aria-hidden="true"></i>
                                </figure>
                            <?php endif;
                            if($contact_title):?><h4 class="title"><?php echo $contact_title; ?></h4><?php endif;
                            if($contact_text):?><address><?php echo $contact_text; ?></address><?php endif;?>
                        </aside>
                    </div> 
                <?php 
                } 
            }?>
            
            </div>
        </div>
    </section>
<?php
endif;
$stringa = ob_get_contents();
ob_end_clean();
return $stringa; 
}
add_shortcode( 'wphester_contact', 'wphester_contact_callback' );
// menu header info
add_action('wphester_menu_header_info','wphester_menu_header_info_fn');
function wphester_menu_header_info_fn(){
    $menu_header_icon1=get_theme_mod('menu_header_icon1',__('fa-map','wphester-plus'));
    $menu_header_title1=get_theme_mod('menu_header_title1',__('Address','wphester-plus'));
    $menu_header_text1=get_theme_mod('menu_header_text1',__('Art Street 235 Newyork','wphester-plus'));

    $menu_header_icon2=get_theme_mod('menu_header_icon2',__('fa-regular fa-clock','wphester-plus'));
    $menu_header_title2=get_theme_mod('menu_header_title2',__('Opening Hours','wphester-plus'));
    $menu_header_text2=get_theme_mod('menu_header_text2',__('Mon - Sat 8.00 - 18.00','wphester-plus'));

    $menu_header_icon3=get_theme_mod('menu_header_icon3',__('fa-phone','wphester-plus'));
    $menu_header_title3=get_theme_mod('menu_header_title3',__('Contact','wphester-plus'));
    $menu_header_text3=get_theme_mod('menu_header_text3',__('Mobile: 9876543210','wphester-plus'));
    
    if(!empty($menu_header_icon1) ||!empty($menu_header_title1) || !empty($menu_header_text1)||!empty($menu_header_icon2) ||!empty($menu_header_title2) || !empty($menu_header_text2) || !empty($menu_header_icon3) ||!empty($menu_header_title3) || !empty($menu_header_text3)):?>
        <div class="col-lg-8 col-md-8 col-sm-12">                      
            <div class="head-contact-info">
                <div class="row">
                    <?php  if(!empty($menu_header_icon1) ||!empty($menu_header_title1) || !empty($menu_header_text1)):?>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <aside class="contact-widget first">          
                            <div class="media">
                                <?php if(!empty($menu_header_icon1)):?>
                                     <div class="contact-icon">
                                        <i class="fa <?php echo esc_attr($menu_header_icon1);?>" aria-hidden="true"></i>
                                    </div>
                                <?php endif;
                                if(!empty($menu_header_title1)|| !empty($menu_header_text1)):?>
                                    <div class="media-body">
                                        <?php if(!empty($menu_header_title1)):?><h4 class="title"><?php echo esc_html($menu_header_title1);?></h4><?php endif;
                                        if(!empty($menu_header_text1)):?><address><?php echo wp_kses_post($menu_header_text1);?></address><?php endif;?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </aside>
                    </div>
                    <?php endif;
                     if(!empty($menu_header_icon2) ||!empty($menu_header_title2) || !empty($menu_header_text2)):?>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <aside class="contact-widget second">
                            <div class="media">
                                <?php if(!empty($menu_header_icon2)):?>
                                     <div class="contact-icon">
                                        <i class="fa <?php echo esc_attr($menu_header_icon2);?>" aria-hidden="true"></i>
                                    </div>
                                <?php endif;
                                if(!empty($menu_header_title2)|| !empty($menu_header_text2)):?>
                                    <div class="media-body">
                                        <?php if(!empty($menu_header_title2)):?><h4 class="title"><?php echo esc_html($menu_header_title2);?></h4><?php endif;
                                        if(!empty($menu_header_text2)):?><address><?php echo wp_kses_post($menu_header_text2);?></address><?php endif;?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </aside>
                    </div> 
                    <?php endif;
                    if(!empty($menu_header_icon3) ||!empty($menu_header_title3) || !empty($menu_header_text3)):?> 
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <aside class="contact-widget third">
                            <div class="media">
                                <?php if(!empty($menu_header_icon3)):?>
                                     <div class="contact-icon">
                                        <i class="fa <?php echo esc_attr($menu_header_icon3);?>" aria-hidden="true"></i>
                                    </div>
                                <?php endif;
                                if(!empty($menu_header_title3)|| !empty($menu_header_text3)):?>
                                    <div class="media-body">
                                        <?php if(!empty($menu_header_title3)):?><h4 class="title"><?php echo esc_html($menu_header_title3);?></h4><?php endif;
                                        if(!empty($menu_header_text3)):?><address><?php echo wp_kses_post($menu_header_text3);?></address><?php endif;?>
                                    </div>
                                <?php endif;?>
                            </div>
                        </aside>
                    </div>  
                    <?php endif;?>           
               </div>
            </div>
        </div>
        <?php 
    endif;
}