<?php
// Typography
$enable_top_widget_typography = get_theme_mod('enable_top_widget_typography', false);
$enable_header_typography = get_theme_mod('enable_header_typography', false);
$enable_banner_typography = get_theme_mod('enable_banner_typography', false);
$enable_section_title_typography = get_theme_mod('enable_section_title_typography', false);
$enable_slider_title_typography = get_theme_mod('enable_slider_title_typography', false);
$enable_content_typography = get_theme_mod('enable_content_typography', false);
$enable_post_typography = get_theme_mod('enable_post_typography', false);
$enable_meta_typography = get_theme_mod('enable_meta_typography', false);
$enable_shop_page_typography = get_theme_mod('enable_shop_page_typography', false);
$enable_sidebar_typography = get_theme_mod('enable_sidebar_typography', false);
$enable_footer_bar_typography = get_theme_mod('enable_footer_bar_typography', false);
$enable_footer_widget_typography = get_theme_mod('enable_footer_widget_typography', false);
$enable_after_btn_typography= get_theme_mod('enable_after_btn_typography',false);


/* Top Widget Area */
if ($enable_top_widget_typography == true) {
    ?>
    <style>
        .header-sidebar .widgettitle, .header-sidebar .widget h1, .header-sidebar .widget h2, .header-sidebar .widget h3 , .header-sidebar .widget h4,  .header-sidebar .widget h5, .header-sidebar .widget h6 { 
            font-size:<?php echo get_theme_mod('topbar_widget_title_fontsize', '30') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('topbar_widget_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('topbar_widget_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('topbar_widget_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('topbar_widget_title_text_transform', 'default'); ?> !important;
            margin-bottom :0px;
            line-height:<?php echo get_theme_mod('topbar_widget_title_line_height','45').'px'; ?> !important;
        }
        .head-contact-info li, .head-contact-info li a, .header-sidebar .top-header .custom-social-icons li > a, .header-sidebar .top-header p, .header-sidebar .top-header a, .header-sidebar .top-header em { 
            font-size:<?php echo get_theme_mod('top_widget_typography_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('top_widget_typography_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('top_widget_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('top_widget_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('top_widget_typography_text_transform', 'default'); ?> !important;
        line-height:<?php echo get_theme_mod('topbar_widget_content_line_height','30').'px'; ?> !important;
        }

        .header-sidebar .custom-social-icons li > a {
            width: <?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?> !important;
            height: <?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?> !important;
            line-height:<?php echo get_theme_mod('top_widget_typography_fontsize', '15') + 12 . 'px'; ?> !important;
        }
    </style>
    <?php
}


/* Site title and tagline */
if ($enable_header_typography == true) {
    ?>
    <style>
        .site-title {
            font-family:<?php echo get_theme_mod('site_title_fontfamily', 'Open Sans'); ?> !important;
            font-size:<?php echo get_theme_mod('site_title_fontsize', '36') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('site_title_fontweight', '700'); ?> !important;
            font-style:<?php echo get_theme_mod('site_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('site_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('site_title_line_height','39').'px'; ?> !important;
        }

        .site-description {
            font-family:<?php echo get_theme_mod('site_tagline_fontfamily', 'Open Sans'); ?> !important;
            font-size:<?php echo get_theme_mod('site_tagline_fontsize', '20') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('site_tagline_fontweight', '400'); ?> !important;
            font-style:<?php echo get_theme_mod('site_tagline_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('site_tagline_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('site_tagline_line_height','30').'px'; ?> !important;
        }

        .navbar .nav > li > a:not(.main-header-btn) {
            font-family:<?php echo get_theme_mod('menu_title_fontfamily', 'Open Sans'); ?> !important;
            font-size:<?php echo get_theme_mod('menu_title_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('menu_title_fontweight', '600'); ?> !important;
            font-style:<?php echo get_theme_mod('menu_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('menu_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('menu_line_height','30').'px'; ?> !important;
        }

        .dropdown-menu .dropdown-item{
            font-family:<?php echo get_theme_mod('submenu_title_fontfamily', 'Open Sans'); ?> !important;
            font-size:<?php echo get_theme_mod('submenu_title_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('submenu_title_fontweight', '400'); ?> !important;
            font-style:<?php echo get_theme_mod('submenu_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('submenu_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('submenu_line_height','30').'px'; ?> !important;
        }
    </style>
    <?php
}

/* After Title */
if($enable_after_btn_typography == true)
{
?>
<style>
body .btn-style-one{
    font-family:<?php echo get_theme_mod('after_btn_fontfamily','Open Sans'); ?> !important;
    font-size:<?php echo get_theme_mod('after_btn_fontsize','15').'px'; ?> !important;
    line-height:<?php echo get_theme_mod('after_btn_lheight','1').'px'; ?> !important;
    font-weight:<?php echo get_theme_mod('after_btn_fontweight','600'); ?> !important;
    font-style:<?php echo get_theme_mod('after_btn_fontstyle','normal'); ?> !important;
    text-transform:<?php echo get_theme_mod('after_btn_text_transform','default'); ?> !important;
}
</style>
<?php } 


/* Banner Title */
if ($enable_banner_typography == true) {
    ?>
    <style>
        .page-title h1 {
            font-family:<?php echo get_theme_mod('banner_title_fontfamily', 'Open Sans'); ?> !important;
            font-size:<?php echo get_theme_mod('banner_title_fontsize', '32') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('banner_title_fontweight', '700'); ?> !important;
            font-style:<?php echo get_theme_mod('banner_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('banner_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('banner_titl_line_height','41').'px'; ?> !important;
        }

        /* Breadcrumb Title */
        .page-breadcrumb a, .page-breadcrumb span,.page-breadcrumb, .rank-math-breadcrumb a , .rank-math-breadcrumb span, .navxt-breadcrumb a , .navxt-breadcrumb span {
            font-family:<?php echo get_theme_mod('breadcrumb_title_fontfamily', 'Open Sans'); ?> !important;
            font-size:<?php echo get_theme_mod('breadcrumb_title_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('breadcrumb_title_fontweight', '400'); ?> !important;
            font-style:<?php echo get_theme_mod('breadcrumb_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('breadcrumb_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo get_theme_mod('banner_titl_line_height','41').'px'; ?> !important;
        }
    </style>
    <?php
}


/* Section Title */
if ($enable_section_title_typography == true) {
    ?>
    <style>
        .section-header  h2:not(.cta .title), .contact .section-header h2, .funfact .funfact-block h2.title {
            font-size:<?php echo get_theme_mod('section_title_fontsize', '36') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('section_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('section_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('section_title_fontstyle', 'Normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('section_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('section_title_line_height','54')).'px'; ?> !important;
    }


        .section-header .section-subtitle, .testimonial .section-header p,.funfact .funfact-block p{
            font-size:<?php echo get_theme_mod('section_subtitle_fontsize', '36') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('section_subtitle_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('section_subtitle_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('section_subtitle_fontstyle', 'Normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('section_subtitle_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('section_description_line_height','30')).'px'; ?> !important;
    }
    </style>
    <?php
}


/* Slider Title */
if ($enable_slider_title_typography == true) {
    ?>
    <style>
        .slider-caption h2  {
            font-size:<?php echo get_theme_mod('slider_title_fontsize', '50') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('slider_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('slider_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('slider_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('slider_title_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('slider_line_height','85')).'px'; ?> !important;
        }
    </style>
    <?php
}


/* Content */
if ($enable_content_typography == true) {
    ?>
    <style>
        /* Heading H1 */
        .about-section h1, body:not(.woocommerce-page) .entry-content h1, .services h1, .contact h1, .error-page h1, .cta_main h1 {
            font-size:<?php echo get_theme_mod('h1_typography_fontsize', '36') . 'px'; ?> !important;
            line-height:<?php echo get_theme_mod('h1_typography_fontsize', '36') + 5 . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h1_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h1_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h1_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h1_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h1_line_height','54')).'px'; ?> !important;;
        }

        /* Heading H2 */
        body:not(.woocommerce-page) .entry-content h2, .about h2, .contact h2, .cta-2 h2, .section-space.abou-section h2, .section-header h2.counter-value,.about-header h2,.about-section h2, .cta h2{
            font-size:<?php echo get_theme_mod('h2_typography_fontsize', '30') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h2_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h2_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h2_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h2_typography_text_transform', 'default'); ?> !important;
           line-height:<?php echo esc_html(get_theme_mod('h2_line_height','45')).'px'; ?> !important;
    
        }


        .error-page h2{
            font-size:<?php echo get_theme_mod('h2_typography_fontsize', '30') . 'px'; ?> !important;
            line-height:<?php echo get_theme_mod('h2_typography_fontsize', '30') + 45 . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h2_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h2_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h2_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h2_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h2_line_height','45')).'px'; ?> !important;    
        }

        /* Heading H3 */
        body:not(.woocommerce-page) .entry-content h3, .related-posts h3, .about-section h3, .services h3, .contact h3, .contact-form-map .title h3, .section-space .about-section h3, .comment-form .comment-respond h3, .home-blog .entry-header h3.entry-title a {
            font-size:<?php echo get_theme_mod('h3_typography_fontsize', '24') . 'px'; ?> !important;
            line-height:<?php echo get_theme_mod('h3_typography_fontsize', '24') + 5 . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h3_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h3_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h3_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h3_typography_text_transform', 'default'); ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h3_line_height','36')).'px'; ?> !important;
    
        }
        .comment-title h3{
            font-size:<?php echo get_theme_mod('h3_typography_fontsize', '24') + 4 . 'px'; ?> !important;
            line-height:<?php echo get_theme_mod('h3_typography_fontsize', '24') + 4 + 5 . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h3_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h3_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h3_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h3_typography_text_transform', 'default'); ?> !important;
        line-height:<?php echo esc_html(get_theme_mod('h3_line_height','36')).'px'; ?> !important;
    
        }

        /* Heading H4 */
           body h4:not(.blog h4.blog-title,.blog h4.entry-title,.footer-sidebar h4,.sidebar h4), .entry-content h4, .about-header h4:not(.blog-title), .team-grid h4, .entry-header h4:not(.entry-title) a:not(.blog-title),.entry-header .entry-title a.home-blog-title, .contact-widget h4, .about-section h4, .testimonial .testmonial-block .name, .services h4, .contact h4, .portfolio h4, .section-space .about-sections h4, #related-posts-carousel .entry-header h4 a:not(.blog-title), .blog-author h4.name, .error-page h4{

            font-size:<?php echo get_theme_mod('h4_typography_fontsize', '20') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('h4_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h4_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h4_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h4_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h4_typography_text_transform', 'default'); ?> !important;
        }

        /* Heading H5 */
        .product_meta h5, .blog-author h5, .comment-detail h5, .entry-content h5, .about h5, .contact h5, .section-space .about-sections h5, .contact-info h5,.about-section h5 {
            font-size:<?php echo get_theme_mod('h5_typography_fontsize', '18') . 'px'; ?> !important;
             line-height:<?php echo esc_html(get_theme_mod('h5_line_height','24')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('h5_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h5_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h5_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h5_typography_text_transform', 'default'); ?> !important;
        }

        /* Heading H6 */
        body h6, .entry-content h6, .about-sections h6, .services h6, .contact h6, .section-space .about-section h6 {
            font-size:<?php echo get_theme_mod('h6_typography_fontsize', '14') . 'px'; ?> !important;
             line-height:<?php echo esc_html(get_theme_mod('h6_line_height','21')).'px'; ?> !important;
           font-weight:<?php echo get_theme_mod('h6_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('h6_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('h6_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('h6_typography_text_transform', 'default'); ?> !important;
        }

        /* Paragraph */
        .entry-content p:not(.testimonial .testmonial-block .designation), .about-content p, .funfact p:not(.funfact .funfact-block p), .woocommerce-product-details__short-description p, .wpcf7 .wpcf7-form p label, .about-section p, .entry-content li, .contact address, .contact p, .services p, .contact p, .sponsors p, .cta-2 p, .funfact p.description{
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('p_typography_text_transform', 'default'); ?> !important;
        }
        .slider-caption p, body p:not(.site-description,.footer-sidebar p,.sidebar p,.testimonial p,.funfact .funfact-block p){
            font-size:<?php echo get_theme_mod('p_typography_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('p_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('p_typography_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('p_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('p_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('_typography_text_transform', 'default'); ?> !important;
        }

        /* Button Text */
        .btn-combo a, .mx-auto a, .pt-3 a, .wpcf7-form .wpcf7-submit,  .woocommerce .button, .btn-default, .btn-light, .sidebar .woocommerce button[type="submit"], .site-footer .woocommerce button[type="submit"], .sidebar .widget .search-submit, #commentform input[type="submit"],.search-submit,.wp-block-button__link,.more-link, .woocommerce .added_to_cart {
            font-size:<?php echo get_theme_mod('button_text_typography_fontsize', '15') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('button_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('button_text_typography_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('button_text_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('button_text_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('button_text_typography_text_transform', 'default'); ?> !important;
        }
    </style>
    <?php
}

/* Blog / Archive / Single Post */
if ($enable_post_typography == true) {
    ?>
    <style>
        .entry-header h4.blog-title, .entry-header h4 a.blog-title, #related-posts-carousel .entry-header h4 a.blog-title,.entry-header h2 a, .entry-header h3.entry-title a:not(.home-blog-title), .blog h4.entry-title a{
            font-size:<?php echo get_theme_mod('post-title_fontsize', '36') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('post-title_line_height','54')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('post-title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('post-title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('post-title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('post-title_text_transform', 'default'); ?> !important;
        }
    </style>
    <?php
}

/* Post Meta */
if ($enable_meta_typography == true) {
    ?>
    <style>
        .blog .entry-meta a,.blog .entry-date {
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('meta_fontweight', '500'); ?>;
            font-family:<?php echo get_theme_mod('meta_fontfamily', 'Open Sans'); ?>;
            font-style:<?php echo get_theme_mod('meta_fontstyle', 'normal'); ?>;
            text-transform:<?php echo get_theme_mod('meta_text_transform', 'default'); ?>;
        }
        .blog .entry-meta a .author
        {
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
        }
        .blog .entry-meta i {
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?> !important;
            
        }

        .blog .date{
            font-size:<?php echo get_theme_mod('meta_fontsize', '15') . 'px'; ?>;
            line-height:<?php echo esc_html(get_theme_mod('meta_line_height','28')).'px'; ?> !important;
            font-family:<?php echo get_theme_mod('meta_fontfamily', 'Open Sans'); ?>;
            font-weight:<?php echo get_theme_mod('meta_fontweight', '500'); ?>;
            font-style:<?php echo get_theme_mod('meta_fontstyle', 'normal'); ?>;
            text-transform:<?php echo get_theme_mod('meta_text_transform', 'default'); ?>;
        }

       /* .blog .entry-date{
            margin-top: <?php// echo (-( get_theme_mod('meta_fontsize', '15') ) * 1.5 - 96) . 'px'; ?>;
        }*/

        .blog .list-view .entry-date{
            margin-top: -22px !important;
        }
        
    </style>
    <?php
}


/* Shop Page */
if ($enable_shop_page_typography == true) {
    ?>
    <style>
        /* Heading H1 */
        /*.woocommerce div.product h1.product_title,.woocommerce h1{
            font-size:<?php //echo get_theme_mod('shop_h1_typography_fontsize', '36') . 'px'; ?> !important;
            line-height:<?php //echo esc_html(get_theme_mod('shop_h1_line_height','54')).'px'; ?> !important;
            font-weight:<?php //echo get_theme_mod('shop_h1_typography_fontweight', '700'); ?> !important;
            font-family:<?php //echo get_theme_mod('shop_h1_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php //echo get_theme_mod('shop_h1_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php //echo get_theme_mod('shop_h1_typography_text_transform', 'default'); ?> !important;
        }*/

        /* Heading H2 */
        .woocommerce .products h2, .woocommerce .cart_totals h2, .woocommerce-Tabs-panel h2, .woocommerce .cross-sells h2, .woocommerce div.product h2.product_title,.woocommerce h2:NOT(.site-title, .sidebar h2){
            font-size:<?php echo get_theme_mod('shop_h2_typography_fontsize', '18') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('shop_h2_line_height','30')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('shop_h2_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('shop_h2_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('shop_h2_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('shop_h2_typography_text_transform', 'default'); ?> !important;
        }

        /* Heading H3 */
        .woocommerce .checkout h3:not(footer h3,.sidebar h3),.woocommerce h3:not(footer h3,.sidebar h3) {
            font-size:<?php echo get_theme_mod('shop_h3_typography_fontsize', '24') . 'px'; ?> !important;
            line-height:<?php echo esc_html(get_theme_mod('shop_h3_line_height','36')).'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('shop_h3_typography_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('shop_h3_typography_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('shop_h3_typography_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('shop_h3_typography_text_transform', 'default'); ?> !important;
        }
    </style>
    <?php
}


/* Sidebar widgets */
if ($enable_sidebar_typography == true) {
    ?>
    <style>
        .sidebar .widget-title,body .sidebar .widget.widget_block :is(h1,h2,h3,h4,h5,h6),
        .sidebar .wp-block-search .wp-block-search__label{
            font-size:<?php echo get_theme_mod('sidebar_fontsize', '24') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('sidebar_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('sidebar_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('sidebar_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('sidebar_text_transform', 'default'); ?> !important;
        line-height:<?php echo esc_html(get_theme_mod('sidebar_line_height','36')).'px'; ?> !important;
        }
        /* Sidebar Widget Content */
        .sidebar .widget_recent_entries a, .sidebar a, .sidebar p , .sidebar .wp-block-latest-posts__post-excerpt,body .sidebar .wp-block-latest-posts__post-full-content{
            font-size:<?php echo get_theme_mod('sidebar_widget_content_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('sidebar_widget_content_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('sidebar_widget_content_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('sidebar_widget_content_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('sidebar_widget_content_text_transform', 'default'); ?> !important;
        line-height:<?php echo esc_html(get_theme_mod('sidebar_widget_content_line_height','30')).'px'; ?> !important;
        }
    </style>
    <?php
}


/* Footer Bar */
if ($enable_footer_bar_typography == true) {
    ?>
    <style>
        .site-footer .site-info *{
            font-size:<?php echo get_theme_mod('footer_bar_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('footer_bar_fontweight', '400'); ?> !important;
            font-family:<?php echo get_theme_mod('footer_bar_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('footer_bar_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('footer_bar_text_transform', 'default'); ?> !important;
        line-height:<?php echo esc_html(get_theme_mod('fbar_line_height','28')).'px'; ?> !important;
        }
    </style>
    <?php
}


/* Footer Widget */
if ($enable_footer_widget_typography == true) {
    ?>
    <style>
        /* Footer Widget Title */
        .site-footer .footer-typo .widget-title,.footer-sidebar .widget.widget_block :is(h1,h2,h3,h4,h5,h6),
        .footer-sidebar .wp-block-search .wp-block-search__label{
            font-size:<?php echo get_theme_mod('footer_widget_title_fontsize', '24') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('footer_widget_title_fontweight', '700'); ?> !important;
            font-family:<?php echo get_theme_mod('footer_widget_title_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('footer_widget_title_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('footer_widget_title_text_transform', 'default'); ?> !important;
        line-height:<?php echo esc_html(get_theme_mod('footer_widget_title_line_height','36')).'px'; ?> !important;
        }
        /* Footer Widget Content */
        .footer-sidebar .widget_recent_entries a, .footer-sidebar.footer-typo a, .footer-sidebar.footer-typo p, .footer-sidebar.footer-typo .textwidget, .footer-sidebar  .head-contact-info li, .footer-sidebar .head-contact-info li a, .footer-sidebar em ,  .footer-sidebar .wp-block-latest-posts__post-excerpt {
            font-size:<?php echo get_theme_mod('footer_widget_content_fontsize', '15') . 'px'; ?> !important;
            font-weight:<?php echo get_theme_mod('footer_widget_content_fontweight', '600'); ?> !important;
            font-family:<?php echo get_theme_mod('footer_widget_content_fontfamily', 'Open Sans'); ?> !important;
            font-style:<?php echo get_theme_mod('footer_widget_content_fontstyle', 'normal'); ?> !important;
            text-transform:<?php echo get_theme_mod('footer_widget_content_text_transform', 'default'); ?> !important;
        line-height:<?php echo esc_html(get_theme_mod('footer_widget_content_line_height','30')).'px'; ?> !important;
        }
    </style>
<?php } ?>



<?php
// -----------------Colors & Background----------------------
?>



<style>
    <?php 
     if (get_theme_mod('header_clr_enable', false) == true) : ?>
        /* Header Background Color*/
        
        .five .layout5.header-logo, .six .navbar-header.index6,body .seven .layout3.header-sidebar,body .layout3.header-sidebar .bottom-header
        {
         background: <?php echo get_theme_mod('header_background_color', '#111111'); ?>;  
        }
        /* Site Title & Tagline */
        body .site-title a.site-title-name, body .navbar4 .custom-logo-link-url .site-title a{
            color: <?php echo get_theme_mod('site_title_link_color', '#ffffff'); ?>;
        }
        body .site-title a.site-title-name:hover,body .navbar4 .custom-logo-link-url .site-title a:hover{
            color: <?php echo get_theme_mod('site_title_link_hover_color', '#56ABAB'); ?>;
        }
        body p.site-description{
            color: <?php echo get_theme_mod('site_tagline_text_color', '#c5c5c5'); ?>!important;
        }
    <?php endif; ?>

    /* Sticky Header Color shceme */
    <?php if (get_theme_mod('sticky_header_clr_enable', false) == true) : ?>
        .header-sticky.stickymenu1, .header-sticky.stickymenu, .header-sticky.shrink1,.five.stickymenu .navbar2,.six.stickymenu .index5, .five.shrink1 .header-logo.layout5, .six.shrink1 .navbar-header.index6, .five.stickymenu .header-logo.layout5, .six.stickymenu .navbar-header.index6
        {
            background-color: <?php echo get_theme_mod('sticky_header_bg_color', '#111111'); ?> !important;
        }
        body .five.stickymenu1 .layout5.header-logo
        {
                background: <?php echo get_theme_mod('sticky_header_bg_color', '#111111'); ?>;
        }
        body .six.stickymenu1 .navbar-header.index6,body .seven.stickymenu .header-sidebar.layout3 .bottom-header,body .seven.shrink1 .header-sidebar.layout3 .bottom-header,body .seven.stickymenu .header-sidebar.layout3 .bottom-header
        {
            background: <?php echo get_theme_mod('sticky_header_bg_color', '#111111'); ?>;
        }
        .header-sticky.stickymenu1 .site-title a, .header-sticky.stickymenu .site-title a, .header-sticky.shrink1 .site-title a
        {
            color: <?php echo get_theme_mod('sticky_header_title_color', '#ffffff'); ?> !important;
        }
         body .header-sticky.stickymenu .navbar.navbar6 .nav .nav-item:hover .nav-link,body .header-sticky.stickymenu .navbar.navbar6 .nav .nav-item .nav-link:focus {
             color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#56ABAB'); ?> !important;
        }
        .header-sticky.stickymenu1 .site-description, .header-sticky.stickymenu .site-description, .header-sticky.shrink1 .site-description
        {
            color: <?php echo get_theme_mod('sticky_header_subtitle_color', '#c5c5c5'); ?> !important;
        }
        .header-sticky.stickymenu1 .nav .nav-item .nav-link, .header-sticky.stickymenu .nav .nav-item .nav-link, .header-sticky.shrink1 .nav .nav-item .nav-link {
            color: <?php echo get_theme_mod('sticky_header_menus_link_color', '#ffffff'); ?> !important;
        }
        .header-sticky.stickymenu1 .nav .nav-item:hover .nav-link, .header-sticky.stickymenu1 .navbar.custom .nav .nav-item.active .nav-link:hover, .header-sticky.stickymenu .nav .nav-item:hover .nav-link, .header-sticky.stickymenu .navbar.custom .nav .nav-item.active .nav-link:hover , .header-sticky.shrink1 .nav .nav-item:hover .nav-link, .header-sticky.shrink1 .navbar.custom .nav .nav-item.active .nav-link:hover,.navbar.header-sticky.stickymenu .nav .nav-item:hover .nav-link,.header-sticky.stickymenu .navbar .nav .nav-item:hover .nav-link, body .header-sticky.stickymenu .navbar .nav .nav-item:hover .nav-link,.header-sticky.stickymenu .navbar .nav .nav-item:hover .nav-link{
            color: <?php echo get_theme_mod('sticky_header_menus_link_hover_color', '#56ABAB'); ?> !important;
        }
        .header-sticky.stickymenu.navbar ul li.menu-item a .menu-text:hover:after, .header-sticky.stickymenu.navbar ul li.menu-item a .menu-text:hover:after,.header-sticky.stickymenu.navbar .nav li.active .nav-link .menu-text:after, .header-sticky.stickymenu ul li.menu-item a .menu-text:hover:after,.header-sticky.stickymenu .navbar ul li.menu-item a .menu-text:hover:after,.header-sticky.stickymenu  .navbar.custom .nav .nav-item.active .nav-link .menu-text:hover:after,.header-sticky.stickymenu  .navbar.custom .nav .nav-item.active .nav-link .menu-text:after{
            background-color:<?php echo get_theme_mod('sticky_header_menus_link_hover_color', '#56ABAB'); ?> !important;
        }

        .dark .header-sticky.stickymenu .nav .nav-item.active .nav-link,.dark .header-sticky.stickymenu .navbar-nav .show .dropdown-menu > .active > .menu-text:after,.navbar.custom.header-sticky.stickymenu1 .nav .nav-item.active .nav-link, .navbar.custom.header-sticky.stickymenu .nav .nav-item.active .nav-link, .navbar.custom.header-sticky.shrink1 .nav .nav-item.active .nav-link,body .header-sticky.stickymenu .nav .nav-item.active .nav-link,.header-sticky.stickymenu  .navbar.custom .nav .nav-item.active .nav-link:hover, body .header-sticky.stickymenu .navbar .nav .nav-item.active .nav-link,body .header-sticky.stickymenu .navbar .nav .nav-item.current_page_ancestor.current_page_parent.nav-item .nav-link,
        body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active a.dropdown-item, 
        body .header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu li.active a.dropdown-item,.header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .current_page_item.active a.dropdown-item, body .header-sticky.stickymenu.navbar .nav .nav-item .dropdown.active > a {
            color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#56ABAB'); ?> !important;
        }
        .dark .header-sticky.stickymenu.navbar .nav li.active .nav-link .menu-text:after,.dark .header-sticky.stickymenu .navbar .nav li.active .nav-link .menu-text:after {
            background-color: <?php echo get_theme_mod('sticky_header_menus_link_active_color', '#56ABAB'); ?> !important;
        }

        /* Sticky Header Submenus */
        .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item, .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-menu, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item, .header-sticky.stickymenu .nav.navbar-nav .dropdown-menu, .header-sticky.shrink1 .nav.navbar-nav .dropdown-item, .header-sticky.shrink1 .nav.navbar-nav .dropdown-menu,body .header-sticky.stickymenu .layout3.navbar .nav .nav-item .dropdown-menu {
            background-color: <?php echo get_theme_mod('sticky_header_submenus_background_color', '#000000'); ?>;
        }
        .header-sticky.stickymenu .nav.navbar-nav a.dropdown-item,.header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item {
            color: <?php echo get_theme_mod('sticky_header_submenus_link_color', '#ffffff'); ?>!important;
        }
        .header-sticky.stickymenu .nav.navbar-nav a.dropdown-item:hover,.header-sticky.stickymenu .nav.navbar-nav a.bg-light.dropdown-item,.header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active a.dropdown-item,.header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu li.active a.dropdown-item, body .header-sticky.stickymenu.navbar .nav .nav-item .dropdown.active > a, body .navbar .nav .nav-item .dropdown:hover > a,.header-sticky.stickymenu .nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item:hover,body .header-sticky.stickymenu .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark:hover,body .header-sticky.stickymenu .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark { 
            color: <?php echo get_theme_mod('sticky_header_submenus_link_hover_color', '#56ABAB'); ?>!important;
        }
         .header-sticky.stickymenu.navbar ul.dropdown-menu li.menu-item a .menu-text:hover:after,.header-sticky.stickymenu ul.dropdown-menu li.menu-item a .menu-text:hover:after{
        background-color: <?php echo get_theme_mod('sticky_header_submenus_link_hover_color', '#56ABAB'); ?>!important;
    }
       
        .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item:focus, .header-sticky.stickymenu1 .nav.navbar-nav .dropdown-item:hover, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item:focus, .header-sticky.stickymenu .nav.navbar-nav .dropdown-item:hover, .header-sticky.shrink1 .nav.navbar-nav .dropdown-item:focus, .header-sticky.shrink1 .nav.navbar-nav .dropdown-item:hover,.navbar .nav li.active .nav-link .menu-text:after
        {
            background-color: transparent!important;
        }
    <?php endif; ?>

    /* Primary Menu */
    <?php if (get_theme_mod('apply_menu_clr_enable', false) == true) : ?>
       .navbar.custom .nav .nav-item .nav-link,body .navbar .nav .nav-item .nav-link,body .navbar.navbar1 .nav .nav-item .nav-link, body .navbar.navbar4 .nav .nav-item .nav-link,body .navbar5.navbar .nav .nav-item .nav-link,body .navbar.navbar6 .nav .nav-item .nav-link,body .layout3.navbar .nav .nav-item .nav-link {
            color: <?php echo get_theme_mod('menus_link_color', '#ffffff'); ?>;
        }
        body .navbar.navbar6 .nav .nav-item:hover .nav-link,body .navbar.navbar6 .nav .nav-item .nav-link:focus,body .navbar.seven .nav .nav-item.active:hover .nav-link,body .navbar.seven .nav .nav-item.active .nav-link:focus,body .navbar.custom .nav .nav-item.active .nav-link,.navbar1 .dropdown-menu > li.active > a,body .navbar .dropdown-menu > li.active > a,body.dark .navbar.navbar5 .nav .nav-item.active .nav-link{
             color: <?php echo get_theme_mod('menus_link_active_color', '#56ABAB'); ?>!important;
        }

        body .navbar .nav li.active .nav-link .menu-text:after{
            background-color: <?php echo get_theme_mod('menus_link_active_color', '#56ABAB'); ?>!important;
        }
        .navbar.custom .nav .nav-item:hover .nav-link,body .navbar .nav .nav-item:hover .nav-link,body .navbar .nav .nav-item.active .nav-link:hover,.navbar .nav .nav-item:hover .nav-link,body .navbar.navbar6 .nav .nav-item .nav-link:hover{
            color: <?php echo get_theme_mod('menus_link_hover_color', '#56ABAB'); ?>!important;
        }
        .nav.navbar-nav a.dropdown-item:hover {
            color: <?php echo get_theme_mod('menus_link_hover_color', '#56ABAB'); ?>!important;
        }
        body .navbar ul li.menu-item a .menu-text:hover:after{
            background: <?php echo get_theme_mod('menus_link_hover_color', '#56ABAB'); ?>;
        }
        .navbar .nav li.active .nav-link .menu-text:after,body.dark .navbar.navbar5 ul li a .menu-text:hover:after{
            background:<?php echo get_theme_mod('menus_link_hover_color', '#56ABAB'); ?>; 
            width: 100%;
        }
        /*.navbar.custom .nav .nav-item.active.current_page_ancestor .nav-link,body .navbar .nav .nav-item.active .nav-link,body .navbar.navbar6 .nav .nav-item:hover .nav-link, body .navbar.navbar6 .nav .nav-item .nav-link:focus  {
            background-color: <?php //echo get_theme_mod('menus_link_hover_color', '#926AA6'); ?>!important;
        }*/

        .navbar.custom .nav .nav-item.active .nav-link,body .navbar .nav .nav-item.active .nav-link, body .navbar .nav .nav-item.current_page_ancestor.current_page_parent.nav-item .nav-link  {
            color: <?php echo get_theme_mod('menus_link_active_color', '#56ABAB'); ?>!important;
        }
        
        /* Submenus */
        .nav.navbar-nav .dropdown-item, .nav.navbar-nav .dropdown-menu,body .layout3.navbar .nav .nav-item .dropdown-menu,body .navbar2.navbar .dropdown-menu {
            background-color: <?php echo get_theme_mod('submenus_background_color', '#000000'); ?>;
        }
        .nav.navbar-nav a.dropdown-item,.nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item{
            color: <?php echo get_theme_mod('submenus_link_color', '#ffffff'); ?>!important;
        }
     
        body .navbar ul.dropdown-menu li.menu-item a .menu-text:hover:after{
            background: <?php echo get_theme_mod('submenus_link_hover_color', '#56ABAB'); ?>;
        }
      
        body .navbar .nav .nav-item.active li.active a.dropdown-item .nav-link:hover,.nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active a.dropdown-item,.nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu li.active a.dropdown-item, body .navbar .nav .nav-item .dropdown.active > a{
         color: <?php echo get_theme_mod('menus_link_active_color', '#56ABAB'); ?>!important;
        }
           .nav.navbar-nav a.dropdown-item:hover,.nav.navbar-nav a.bg-light.dropdown-item, body .navbar .nav .nav-item .dropdown:hover > a,.nav.navbar-nav ul.dropdown-menu .menu-item-has-children.active ul.dropdown-menu a.dropdown-item:hover,body .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark:hover,body .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark  {
    
            color: <?php echo get_theme_mod('submenus_link_hover_color', '#56ABAB'); ?> !important;

        }

        .nav.navbar-nav .dropdown-item:focus, .nav.navbar-nav .dropdown-item:hover
        {
            background-color: transparent;
        }
    <?php endif; ?>
    /* Banner */
    .page-title-section .page-title h3.theme-dtl{
        color: <?php echo get_theme_mod('home_banner_subtitle_color', '#fff'); ?> !Important;
    }
    .page-title-section .page-title h1{
        color: <?php echo get_theme_mod('banner_text_color', '#fff'); ?> !Important;
    }
    /* Breadcrumb */
    <?php
    $enable_brd_link_clr_setting = get_theme_mod('enable_brd_link_clr_setting', false);
    if ($enable_brd_link_clr_setting == true):
        ?>
        .page-breadcrumb span a, nav.rank-math-breadcrumb a, .navxt-breadcrumb span a, .navxt-breadcrumb
        {
            color: <?php echo get_theme_mod('breadcrumb_title_link_color', '#ffffff'); ?> !important;
        }
        .page-breadcrumb span a:hover, nav.rank-math-breadcrumb a:hover,.navxt-breadcrumb span a:hover {
            color: <?php echo get_theme_mod('breadcrumb_title_link_hover_color', '#56ABAB'); ?> !important;
        }
    <?php endif; ?>

    /* After Menu Button */
     <?php
    $enable_after_menu_btn_clr_setting=get_theme_mod('enable_after_menu_btn_clr_setting',false);
    if($enable_after_menu_btn_clr_setting==true): ?>
    #wrapper a.main-header-btn{
    background-color: <?php echo get_theme_mod('after_menu_btn_bg_clr','#56ABAB');?>
    }   
    #wrapper a.main-header-btn{
    color: <?php echo get_theme_mod('after_menu_btn_txt_clr','#ffffff');?>
    }
    #wrapper a.main-header-btn:hover {
   background-color: <?php echo get_theme_mod('after_menu_btn_hover_clr','#ffffff');?>
    }
    #wrapper a.main-header-btn:hover {
   color: <?php echo get_theme_mod('after_menu_btn_text_hover_clr','#56ABAB');?>
    }

    <?php endif;?>  

    /* Content */
    <?php
    $enable_content_link_clr_setting = get_theme_mod('content_clr_enable', false);
    if ($enable_content_link_clr_setting == true) {
        ?>
        body h1:not(.sidebar h1),body h1 a {
            color: <?php echo get_theme_mod('h1_color', '#ffffff'); ?> ;
        }
        body .section-header h2:not(.cta_main h2,.testimonial h2, .funfact h2, .sidebar h2), body h2:not(.site-footer h2,.cta_main h2,.testimonial h2, .funfact h2, .sidebar h2),body h2:not(.testimonial h2, .funfact h2, .sidebar h2) a,body .contact-form h2.title,body .contact-section h2.title, body .contact-info h2.title{
            color: <?php echo get_theme_mod('h2_color', '#ffffff'); ?>;
        }
        body h3:not(.sidebar h3), body h3 a {
            color: <?php echo get_theme_mod('h3_color', '#ffffff'); ?>;
        }
        body .entry-header h4 > a:not(.rm-h4), body h4:not(.site-footer h4,.sidebar h4,.funfact-title,.single .blog-title .entry-title,.blog.bg-default .entry-header .entry-title,.testimonial h4), body h4 a:not(.blog.bg-default .entry-header .entry-title a), body .team .team-grid .name, .section-space.contact-detail .contact-area h4,.services h4.entry-title a,body .portfolio .tab-content .portfolio-thumbnail .entry-title a, body.dark .blog .entry-header a.home-blog-title, body .team .name, body .contact .contact-widget .title, body .team3 h4, body .team4 h4{
            color: <?php echo get_theme_mod('h4_color', '#ffffff'); ?>;
        }
        body .blog-author h5, body .comment-detail h5, body h5:not(.sidebar h5), body h5 a, body .blog-author .post-by,.contact-info .contact-widget h5.title{
            color: <?php echo get_theme_mod('h5_color', '#ffffff'); ?>;
        }

        .section-header h5.section-subtitle,body .contact-section .contact-info h5.subtitle{
            color: <?php echo get_theme_mod('h5_color', '#ffffff'); ?>;
        }

        body .product-price h5 > a{
            color: <?php echo get_theme_mod('h5_color', '#ffffff'); ?>;
        }

        body h6:not(.sidebar h6), .section-space.contact-detail .contact-area h6, body h6 a {
            color: <?php echo get_theme_mod('h6_color', '#ffffff'); ?>;
        }
        p:not(.cta-2 .section-subtitle, .woocommerce-mini-cart__total, .slider-caption .description, .site-description, .testimonial p, .funfact p,.sidebar p,.footer-sidebar.footer-typo p,.site-info  p),.services .card p.description,body .contact-section .contact-info .details{
            color: <?php echo get_theme_mod('p_color', '#c5c5c5'); ?>;
        }
        body .team .team-grid .card-body p,body .team2 .team-grid .card-body p,body .team3 .team-grid .card-body p,body .team4 .team-grid .card-body p{color: <?php echo get_theme_mod('p_color', '#c5c5c5'); ?>;}
    <?php } ?>

    <?php if (get_theme_mod('apply_slider_clr_enable', false) == true): ?> 
        /* Slider Section */
        body .spice-slider  .heading span{
            color: <?php echo get_theme_mod('home_slider_subtitle_color', '#ffffff'); ?>;
        }
        .spice-slider  .slider-caption h2{
            color: <?php echo get_theme_mod('home_slider_title_color', '#ffffff'); ?>;
        }
        .spice-slider  .slider-caption .description
        {
            color: <?php echo get_theme_mod('home_slider_description_color', '#ffffff'); ?>;
        }

        .spice-slider  .btn-small.btn-default {
            border: 1px solid <?php echo get_theme_mod('slider_btn1_color', '#56ABAB'); ?>;
            background: <?php echo get_theme_mod('slider_btn1_color', '#56ABAB'); ?>;
        }
        .spice-slider  #slider-carousel .btn-small.btn-default:hover {
            border: 1px solid <?php echo get_theme_mod('slider_btn1_hover_color', '#ffffff'); ?>;
            background: <?php echo get_theme_mod('slider_btn1_hover_color', '#ffffff'); ?>;
        }
    <?php endif;?>
     <?php if (get_theme_mod('slider_btn2_color_enable', false) == true):?> 
    .spice-slider  .btn-light {
        background: <?php echo get_theme_mod('slider_btn2_color', '#56ABAB'); ?>;
        border: 1px solid <?php echo get_theme_mod('slider_btn1_color', '#56ABAB'); ?>;
    }

    .spice-slider  #slider-carousel .btn-light:hover {
        border: 1px solid <?php echo get_theme_mod('slider_btn2_hover_color', '#56ABAB'); ?>;
        background: <?php echo get_theme_mod('slider_btn2_hover_color', '#56ABAB'); ?>;
    }
    <?php endif; ?>

    /* Testimonial Section */
    <?php if (get_theme_mod('apply_testimonial_clr_enable', false) == true) { ?> 
        .testimonial .section-title,.testimonial .section-header .section-title
        {
            color: <?php echo get_theme_mod('home_testi_title_color', '#ffffff'); ?> !important;
        }
        .section-space.testimonial .section-header p,.testimonial .section-header p.section-subtitle {
            color: <?php echo get_theme_mod('home_testi_subtitle_color', '#56ABAB'); ?> !important;
        }
        .section-space.testimonial .entry-content p,.section-space.testimonial .entry-content h4
        {
            color: <?php echo get_theme_mod('testimonial_description_color', '#ffffff'); ?> !important;
        }
        .section-space.testimonial.testimonial-1 .entry-content p a,.testimonial .testmonial-block .name, .testimonial .testmonial-block .name a,.testimonial.testimonial-1 .testimonial-block .inner-box .content-column .author-info .author-name a,#testimonial-carousel2 .name,#testimonial-carousel3 .name,#testimonial-carousel4 .name
        {
            color: <?php echo get_theme_mod('testi_clients_name_color', '#ffffff'); ?> !important;
        }
        .section-space.testimonial.testimonial-1 .entry-content p,.testimonial .testmonial-block .designation,.testimonial.testimonial-1 .testimonial-block .inner-box .content-column .author-info .designation,#testimonial-carousel2 .designation,#testimonial-carousel3 .designation,#testimonial-carousel4 .designation
        {
            color: <?php echo get_theme_mod('testi_clients_designation_color', '#ffffff'); ?> !important;
        }
    <?php } else { ?>
        .testimonial .section-title
        {
            color: '#ffffff';
        }

        .testimonial .entry-content .description
        {
            color: '#ffffff';
        }
    <?php } ?>

    /* CTA 1 SECTION */

    <?php if (get_theme_mod('apply_cta1_clr_enable', false) == true): ?>
        body .callout .section-header .section-title{
            color: <?php echo get_theme_mod('home_cta1_title_color', '#ffffff'); ?>;
        }
        body .callout .btn-light {
            background-color: <?php echo get_theme_mod('home_cta1_btn_color', '#ffffff'); ?>;
        }
        body .callout .btn-light{
            color: <?php echo get_theme_mod('cta1_bg_text_color', '#1d1d1d'); ?> ;
        }

        body .callout .btn-light:hover{
        background-color: <?php echo get_theme_mod('home_cta1_btn_hover_color', '#1d1d1d'); ?>;
        }

        body .callout .btn-light:hover{
        color: <?php echo get_theme_mod('cta1_bg_text_hover_color', '#ffffff'); ?>;
        }
        
        /*.cta_content .btn-light, .cta_content a:after  {
            color: <?php //echo get_theme_mod('cta1_bg_section_color', '#00BFFF'); ?>  !important;
        }*/

    <?php endif; ?>

    /* CTA 2 SECTION */
    .cta-2 .title {
        color: #ffffff;
    }
    .cta-2 p {
        color: #ffffff;
    }

    <?php if (get_theme_mod('apply_cta2_clr_enable', false) == true): ?>
        .cta .section-subtitle{
            color: <?php echo get_theme_mod('home_cta2_title_color', '#ffffff'); ?> !important;
        }
        .cta h2.section-title {
            color: <?php echo get_theme_mod('home_cta2_subtitle_color', '#ffffff'); ?> !important;
        }
        #cta-video {
            border: 0.3rem solid <?php echo get_theme_mod('home_cta2_btn1_color', '#fff'); ?>  !important;
    
        }
        .cta-block .video-btn a#cta-video:after {
            border-left: 1.563rem solid <?php echo get_theme_mod('home_cta2_btn1_color', '#fff'); ?>  !important;
        }
        /*.cta .btn-default:hover{
            background: <?php echo get_theme_mod('home_cta2_btn1_hover_color', '#ffffff'); ?>  !important;
            border: 1px solid <?php echo get_theme_mod('home_cta2_btn1_hover_color', '#ffffff'); ?>;
        }*/
    <?php endif; ?>
    
        

    /* Funfact SECTION */
    .funfact .title {
        color: '#ffffff';
    }

    .funfact .funfact-inner .funfact-title, .funfact .funfact-inner .description.text-white{
        color: '#ffffff';
    }
    <?php if (get_theme_mod('apply_funfact_clr_enable', false) == true): ?>

        .funfact .funfact-inner .funfact-title{
            color: <?php echo get_theme_mod('funfact_count_color', '#ffffff'); ?> !important;
        }

        .funfact .description{
            color: <?php echo get_theme_mod('funfact_count_desc_color', '#ffffff'); ?> !important;
        }

        .funfact .funfact-block h2.title {
            color: <?php echo get_theme_mod('funfact_count_title_color', '#ffffff'); ?> !important;
        }     

        .funfact .funfact-block p {
            color: <?php echo get_theme_mod('funfact_count_subtitle_color', '#ffffff'); ?> !important;
        }

    <?php endif; ?>


    /* Blog Page */
    <?php if ((get_theme_mod('apply_blg_clr_enable', false) == true) && (!is_single())): ?> 
        .standard-view .entry-title a, .entry-title.template-blog-grid-view a, .entry-title.template-blog-grid-view-sidebar a, .entry-title.template-blog-list-view a,.section-space.blog:NOT(.home-blog) .list-view .entry-title a, .entry-title.blog-masonry-two-col a, .entry-title.blog-masonry-three-col a, .entry-title.blog-masonry-four-col a, .section-space.blog:NOT(.home-blog) .entry-title a,body .blog.bg-default .entry-header .entry-title a:not(a.home-blog-title) {
            color: <?php echo get_theme_mod('blog_post_page_title_color', '#333333'); ?>;
        }
        .dark .blog .entry-header .entry-title a.rm-h4:hover,.blog .entry-header .entry-title a.rm-h4:hover,body .blog.bg-default .entry-header .entry-title a.rm-h4:hover,body .blog .entry-header .entry-title.template-blog-list-view a.rm-h4:hover {
            color: <?php echo get_theme_mod('blog_post_page_title_hover_color', '#56ABAB'); ?>;
        }
        .blog .entry-header .entry-meta a, .blog .entry-meta > span a, .blog .entry-meta a, .blog .entry-meta a span{
            color: <?php echo get_theme_mod('blog_post_page_meta_link_color', '#727272'); ?>;
        }
        .blog .entry-header .entry-meta a:hover, .blog .entry-meta a:hover,.blog .entry-meta .author:hover{
            color: <?php echo get_theme_mod('blog_post_page_meta_link_hover_color', '#56ABAB'); ?>;
        }

        .section-module.blog .entry-meta .cat-links a, .section-module.blog .standard-view .entry-meta .author a, .section-module.blog .list-view .entry-meta .author a, .section-module.blog.grid-view .entry-meta .author a, .section-module.blog .entry-meta .comment-links a::before, .entry-meta .posted-on a, .entry-meta .comment-links a, .section-module.blog .entry-meta .comment-links a::before
        {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_color', '#061018'); ?>;
        }
        .section-space.blog .entry-meta .cat-links a:hover, .section-module.blog .standard-view .entry-meta .author a:hover, .section-module.blog .list-view .entry-meta .author a:hover, .section-module.blog .entry-meta .comment-links a:hover::before, .section-module.blog .entry-meta a:hover, .section-module.blog.grid-view .entry-meta .author a:hover
        {
            color: <?php echo get_theme_mod('blog_post_page_meta_link_hover_color', '#56ABAB'); ?>;
        }
    <?php endif; ?>

    /* Single Post/Page */
    <?php if (get_theme_mod('apply_blg_single_clr_enable', false) == true && (is_single())): ?>
        .single-post .standard-view .entry-title a,.single-post .entry-header h4.blog-title , body.single-post .blog .entry-header h4{ 
            color: <?php echo get_theme_mod('single_post_page_title_color', '#333333'); ?>;
        }
       
        .single-post .entry-meta a,.single-post .entry-meta span, .blog-single .entry-meta .cat-links a, .section-module.blog .standard-view .entry-meta .author a, .section-module.blog .list-view .entry-meta .author a, .blog-single .entry-meta .comment-links a::before{
            color: <?php echo get_theme_mod('single_post_page_meta_link_color', '#727272'); ?>;
        }
        .single-post .entry-meta a:hover,.entry-meta a:hover span, .single-post .entry-meta .comment-links a:hover, .single-post .entry-meta .posted-on a:hover, .blog-single .entry-meta .cat-links a:hover, .section-module.blog .standard-view .entry-meta .author a:hover, .section-module.blog .list-view .entry-meta .author a:hover, .blog-single .entry-meta .comment-links a:hover::before{
            color: <?php echo get_theme_mod('single_post_page_meta_link_hover_color', '#56ABAB'); ?>!important;
        }
    <?php endif; ?>

    /* Sidebar */
    <?php if (get_theme_mod('apply_sibar_link_hover_clr_enable', false) == true): ?>
        body .sidebar .widget .widget-title,body .sidebar .widget.widget_block :is(h1,h2,h3,h4,h5,h6),body .sidebar .widget .wp-block-search__label
         {
            color: <?php echo get_theme_mod('sidebar_widget_title_color', '#ffffff'); ?>;
        }
        body .sidebar p, body .sidebar .wp-block-latest-posts__post-excerpt,body .sidebar .wp-block-latest-posts__post-full-content,
        body .sidebar .wp-block-latest-posts__post-date,body .sidebar .wp-block-latest-posts__post-author {
            color: <?php echo get_theme_mod('sidebar_widget_text_color', '#ffffff'); ?>!important;
        }
        body .sidebar a {
            color: <?php echo get_theme_mod('sidebar_widget_link_color', '#ffffff'); ?> !important;
        }
        body .sidebar.s-l-space .sidebar a:hover, body .sidebar .widget a:hover, body .sidebar .widget a:focus,body .sidebar .widget .wp-block-tag-cloud a:hover {
            color: <?php echo get_theme_mod('sidebar_widget_link_hover_color', '#56ABAB'); ?> !important;
        }
    <?php endif; ?>

    /* Footer Widgets */
    <?php if (get_theme_mod('apply_ftrsibar_link_hover_clr_enable', false) == true) { ?>
        body .site-footer {
            background-color: <?php echo get_theme_mod('footer_widget_background_color', '#121212'); ?>;
        }
        .footer-sidebar .widget .widget-title, .footer-sidebar .widget.widget_block :is(h1,h2,h3,h4,h5,h6), 
        .footer-sidebar .widget .wp-block-search__label{
            color: <?php echo get_theme_mod('footer_widget_title_color', '#ffffff'); ?> !important;
        }
        
        body .footer-sidebar p,  body .footer-sidebar .widget, body .footer-sidebar .widget_text p, body .footer-sidebar .widget_text p.description, body .footer-sidebar .widget_text address p,body .footer-sidebar .wp-block-latest-posts__post-author,body .footer-sidebar .wp-block-latest-posts__post-date,body .footer-sidebar .wp-block-latest-posts__post-excerpt {
            color: <?php echo get_theme_mod('footer_widget_text_color', '#ffffff'); ?>;
        }
        body .footer-sidebar .widget a, body .footer-sidebar .widget_recent_entries .post-date, body .footer-sidebar .widget_text address a, body .footer-sidebar .widget address i  {
            color: <?php echo get_theme_mod('footer_widget_link_color', '#ffffff'); ?>;
        }
    
        body .footer-sidebar .widget a:hover, body .footer-sidebar .widget_text address a:hover{
            color: <?php echo get_theme_mod('footer_widget_link_hover_color', '#56ABAB'); ?>;
        }
        body .footer-sidebar .widget .wp-block-tag-cloud a{
            color: <?php echo get_theme_mod('footer_widget_link_color', '#ffffff');?>;
        }
         body .footer-sidebar .widget .wp-block-tag-cloud a:hover,body .site-footer .footer-sidebar .widget .wp-block-tag-cloud a.tag-cloud-link:hover{
            color: <?php echo get_theme_mod('footer_widget_link_hover_color', '#56ABAB');?>!important;
        }
        
    <?php } else { ?>
        .site-footer p {
            color: #fff;
        }
    <?php } ?>

/* Footer Bar */
        body .site-info {
        border-top: <?php echo get_theme_mod('footer_bar_border',0);?>px <?php echo get_theme_mod('footer_border_style','solid');?> <?php echo get_theme_mod('wphester_footer_border_clr','#fff');?>
        }
        body .site-info .footer-sidebar .widget-title,
        body .site-footer .site-info .widget-title,
        body .site-footer .site-info .widget.widget_block h1,
        body .site-footer .site-info .widget.widget_block h2,
        body .footer-sidebar .site-info .widget.widget_block h3,
        body .footer-sidebar .site-info .widget.widget_block h4,
        body .footer-sidebar .site-info .widget.widget_block h5,
        body .footer-sidebar .site-info .widget.widget_block h6{
        color: <?php echo get_theme_mod('advance_footer_bar_title_color','#fff'); ?>!important;
    }
        <?php if (get_theme_mod('apply_foot_hover_anchor_enable', false) == true):
        ?>
        
        body .site-info {
            background-color: <?php echo get_theme_mod('footer_bar_background_color', '#000000'); ?>;
        }
        body .site-info p, body .site-info span, .footer-sidebar .widget_text p {
            color: <?php echo get_theme_mod('footer_bar_text_color', '#ffffff'); ?>;
        }
        body .site-info a,body .site-info .widget a, body .site-info span a {
            color: <?php echo get_theme_mod('footer_bar_link_color', '#fff'); ?> ;
        }

        body .site-info a:hover,body .site-info .widget_recent_entries li a:hover,body .site-info .footer-sidebar li:hover a,body .site-info .footer-sidebar .widget:not(.widget_calendar) a:hover, body .site-info span a:hover,body .site-info p.copyright-section a:hover {
            color: <?php echo get_theme_mod('footer_bar_link_hover_color', '#56ABAB'); ?>;
        }
    <?php endif; 
    if (get_theme_mod('search_btn_enable', true) == true ){ if(!is_rtl()) { ?>
    .cart-header {
        border-left: 1px solid #747474;
        padding: 0 0 0 0.5rem;
    } 
    <?php } else{?>
        .cart-header {
        border-right: 1px solid #747474;
        padding: 0 0 0 0.5rem;
    } 
    <?php }
    }?>
    body .header-sticky.shrink1 .layout5.header-logo,
    body .header-sticky.stickymenu1 .layout5.header-logo,
    body .header-sticky.stickymenu .layout5.header-logo,
    body .header-sticky.shrink1 .index6.bottom-header,
    body .header-sticky.stickymenu1 .index6.bottom-header,
    body .header-sticky.stickymenu .index6.bottom-header,
    body .header-sticky.shrink1 .layout3.header-sidebar .bottom-header,
    body .header-sticky.stickymenu1 .layout3.header-sidebar .bottom-header,
    body .header-sticky.stickymenu .layout3.header-sidebar .bottom-header,
    body .header-sticky.shrink1.navbar2.navbar,
    body .header-sticky.stickymenu1.navbar2.navbar,
    body .header-sticky.stickymenu.navbar2.navbar
    {
        opacity: <?php echo get_theme_mod('sticky_header_opacity', '1.0'); ?>;
        <?php if (get_theme_mod('sticky_header_height', 0) > 0): ?>;
            padding-top: <?php echo get_theme_mod('sticky_header_height', 0); ?>px;
            padding-bottom: <?php echo get_theme_mod('sticky_header_height', 0); ?>px;
        <?php endif; ?>
    }
body .header-sticky.six.stickymenu, body .header-sticky.five.stickymenu{
    padding-top: 0;
    padding-bottom: 0;
}
body .header-sticky.six.stickymenu .header-logo.index5,body .header-sticky.stickymenu .header-logo.index2 {
padding-top: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
}
body .header-sticky.six.stickymenu nav.navbar1,body .header-sticky.stickymenu nav.navbar2 {
    padding-bottom: <?php echo esc_html(get_theme_mod('sticky_header_height', 0)); ?>px;
}  
.custom-logo{width: <?php echo esc_html(get_theme_mod('wphester_logo_length',154));?>px; height: auto;}
body .navbar-brand.sticky-logo img{width: <?php echo esc_html(get_theme_mod('wphester_logo_length',154));?>px; height: auto !important;}
body .navbar-brand.sticky-logo-mbl img{width: <?php echo esc_html(get_theme_mod('wphester_logo_length',154));?>px; height: auto !important;}
body .main-header-btn{ -webkit-border-radius: <?php echo esc_html(get_theme_mod('after_menu_btn_border',0));?>px;border-radius: <?php echo esc_html(get_theme_mod('after_menu_btn_border',0));?>px;}

</style>