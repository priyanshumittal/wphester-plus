<?php
/**
 * List of available hooks
 *
 */
 
/* Top Header section */
/**
 * Hook just before the Top Header
 *
 */ 
function wphester_plus_before_header_section_trigger() {
	do_action( 'wphester_plus_before_header_section_hook' );
}
/**
 * Hook just after the Top Header
 *
 */
function wphester_plus_after_header_section_trigger() {
	do_action( 'wphester_plus_after_header_section_hook' );
}


/* Slider section */
/**
 * Hook just before the Slider
 *
 */ 
function wphester_plus_before_slider_section_trigger() {
	do_action( 'wphester_plus_before_slider_section_hook' );
}
/**
 * Hook just after the Slider
 *
 */
function wphester_plus_after_slider_section_trigger() {
	do_action( 'wphester_plus_after_slider_section_hook' );
}

//* Service section */
/**
 * Hook just before the Service
 *
 */ 
function wphester_plus_before_service_section_trigger() {
	do_action( 'wphester_plus_before_services_section_hook' );
}
/**
 * Hook just after the Service
 *
 */
function wphester_plus_after_service_section_trigger() {
	do_action( 'wphester_plus_after_services_section_hook' );
}

/* Fun section */
/**
 * Hook just before the Gallery
 *
 */ 
function wphester_plus_before_fun_section_trigger() {
    do_action( 'wphester_plus_before_fun_section_hook' );
}
/**
 * Hook just after the Gallery
 *
 */
function wphester_after_fun_section_trigger() {
    do_action( 'wphester_plus_after_fun_section_hook' );
} 

/* Portfolio section */
/**
 * Hook just before the Portfolio section
 *
 */ 
function wphester_plus_before_portfolio_section_trigger() {
	do_action( 'wphester_plus_before_portfolio_section_hook' );
}
/**
 * Hook just after the Portfolio section
 *
 */
function wphester_plus_after_portfolio_section_trigger() {
	do_action( 'wphester_plus_after_portfolio_section_hook' );
}

/* Blog section */
/**
 * Hook just before the Blog
 *
 */ 
function wphester_plus_before_news_section_trigger() {
    do_action( 'wphester_plus_before_news_section_hook' );
}
/**
 * Hook just after the Blog
 *
 */
function wphester_plus_after_news_section_trigger() {
    do_action( 'wphester_plus_after_news_section_hook' );
}


/* CTA section  */
/**
 * Hook just before the Callout
 *
 */ 
function wphester_plus_before_cta_section_trigger() {
    do_action( 'wphester_plus_before_cta_section_hook' );
}
/**
 * Hook just after the Callout
 *
 */
function wphester_plus_after_cta_section_trigger() {
    do_action( 'wphester_plus_after_cta_section_hook' );
}

/* About section  */
/**
 * Hook just before the Callout
 *
 */ 
function wphester_plus_before_about_section_trigger() {
    do_action( 'wphester_plus_before_about_section_hook' );
}
/**
 * Hook just after the Callout
 *
 */
function wphester_plus_after_about_section_trigger() {
    do_action( 'wphester_plus_after_about_section_hook' );
}


/* Testimonial section */
/**
 * Hook just before the Testimonial
 *
 */ 
function wphester_plus_before_testimonial_section_trigger() {
    do_action( 'wphester_plus_before_testimonial_section_hook' );
}
/**
 * Hook just after the Testimonial
 *
 */
function wphester_plus_after_testimonial_section_trigger() {
    do_action( 'wphester_plus_after_testimonial_section_hook' );
}

/* Team section */
/**
 * Hook just before the Team
 *
 */ 
function wphester_plus_before_team_section_trigger() {
    do_action( 'wphester_plus_before_team_section_hook' );
}
/**
 * Hook just after the Team
 *
 */
function wphester_plus_after_team_section_trigger() {
    do_action( 'wphester_plus_after_team_section_hook' );
}

/* Shop section */
/**
 * Hook just before the Shop
 *
 */ 
function wphester_plus_before_wooproduct_section_trigger() {
	do_action( 'wphester_plus_before_wooproduct_section_hook' );
}
/**
 * Hook just after the Shop
 *
 */
function wphester_plus_after_wooproduct_section_trigger() {
	do_action( 'wphester_plus_after_wooproduct_section_hook' );
}

/* Callout section */
/**
 * Hook just before the Shop
 *
 */ 
function wphester_plus_before_callout_section_trigger() {
    do_action( 'wphester_plus_before_callout_section_hook' );
}
/**
 * Hook just after the Shop
 *
 */
function wphester_plus_after_callout_section_trigger() {
    do_action( 'wphester_plus_after_callout_section_hook' );
}

/* Client section */
/**
 * Hook just before the Client
 *
 */ 
function wphester_plus_before_client_section_trigger() {
	do_action( 'wphester_plus_before_client_section_hook' );
}
/**
 * Hook just after the Client
 *
 */
function wphester_plus_after_client_section_trigger() {
	do_action( 'wphester_plus_after_client_section_hook' );
}

/* Footer section */
/**
 * Hook just before the Footer
 *
 */ 
function wphester_plus_before_footer_section_trigger() {
	do_action( 'wphester_plus_before_footer_section_hook' );
}
/**
 * Hook just after the Footer
 *
 */
function wphester_plus_after_footer_section_trigger() {
	do_action( 'wphester_plus_after_footer_section_hook' );
}