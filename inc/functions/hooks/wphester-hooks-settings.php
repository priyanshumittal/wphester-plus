<?php
/**
 * Add a Hook panel in the main WordPress menu
 *
 * Based on WordPress Settings API https://codex.wordpress.org/Settings_API
 *
 */

/**
 * Class WPHester_Plus_Hooks_Settings
 */
class WPHester_Plus_Hooks_Settings {

	/**
	 * Settings base
	 *
	 * @access private
	 * @var string
	 */
	private $settings_base;

	/**
	 * Hooks Settings
	 *
	 * @access private
	 * @var string
	 */
	private $hooks_settings;

	/**
	 * WPHester_Plus_Hooks_Settings constructor.
	 */
	public function __construct() {

		$this->settings_base = '';

		// Initialise settings
		add_action( 'admin_init', array( $this, 'init' ) );

		// Register plugin settings
		add_action( 'admin_init', array( $this, 'register_settings' ) );

		// Add settings page to menu
		add_action( 'admin_menu', array( $this, 'add_menu_item' ) );
	}

	/**
	 * Initialise settings
	 *
	 * @return void
	 */
	public function init() {
		$this->hooks_settings = $this->hooks_settings_fields();
	}

	/**
	 * Add settings page to admin menu
	 *
	 * @return void
	 */
	public function add_menu_item() {
		$page = add_theme_page( esc_html__( 'WPHester Plus hooks', 'wphester-plus' ), esc_html__( 'WPHester Plus hooks', 'wphester-plus' ), 'edit_theme_options', 'WPHester_Plus_Hooks_Settings', array( $this, 'build_hooks_settings_page' ) );
		add_action( 'admin_print_styles-' . $page, array( $this, 'hooks_settings_assets' ) );
	}

	/**
	 * Enqueue scripts and styles needed
	 */
	public function hooks_settings_assets() {
		wp_enqueue_script( 'wphester-hooks-seetings-js', WPHESTERP_PLUGIN_URL . '/inc/functions/hooks/js/functions.js', array( 'jquery' ) );
		wp_enqueue_style( 'wphester-hooks-settings-css', WPHESTERP_PLUGIN_URL . '/inc/functions/hooks/css/hooks.css', array() );
	}

	/**
	 * Build hooks settings fields
	 *
	 * @return array Fields displayed on hooks settings page
	 */
	private function hooks_settings_fields() {
		if ( class_exists( 'WooCommerce' ) ) {
		$hooks_settings['standard'] = array(
			'title'  => '',
			'fields' => array(
			
			
			    // Top Header Section
			
				array(
					'name' => esc_html__( 'Before Header', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_header_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Header', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_header_section_hook',
					'type' => 'textarea',
				),

				// Slider Section
				
				array(
					'name' => esc_html__( 'Before Slider Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_slider_section_hook',
					'type' => 'textarea',
				),

				array(
					'name' => esc_html__( 'After Slider Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_slider_section_hook',
					'type' => 'textarea',
				),
				
				// Service Section
				
				array(
					'name' => esc_html__( 'Before Services Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_services_section_hook',
					'type' => 'textarea',
				),

				array(
					'name' => esc_html__( 'After Services Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_services_section_hook',
					'type' => 'textarea',
				),
							
				
				// Fun Section				

				array(
					'name' => esc_html__( 'Before Fun Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_fun_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Fun Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_fun_section_hook',
					'type' => 'textarea',
				),
				
                
                // Portfolio Section
				
				array(
					'name' => esc_html__( 'Before Portfolio Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_portfolio_section_hook',
					'type' => 'textarea',
				),

				array(
					'name' => esc_html__( 'After Portfolio Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_portfolio_section_hook',
					'type' => 'textarea',
				),

				// Blog Section
				

				array(
					'name' => esc_html__( 'Before News Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_news_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After News Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_news_section_hook',
					'type' => 'textarea',
				),
				
				// CTA section
				
				
				array(
					'name' => esc_html__( 'Before CTA Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_cta2_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After CTA Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_cta2_section_hook',
					'type' => 'textarea',
				),

								
				// About section
				
				
				array(
					'name' => esc_html__( 'Before About Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_about_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After About Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_about_section_hook',
					'type' => 'textarea',
				),

								
				// Testimonials Section
				
				
				array(
					'name' => esc_html__( 'Before Testimonials Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_testimonial_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Testimonials Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_testimonial_section_hook',
					'type' => 'textarea',
				),

	            // Team Section
				
				array(
					'name' => esc_html__( 'Before Team Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_team_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Team section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_team_section_hook',
					'type' => 'textarea',
				),

				
				// Shop Section				
				
				array(
					'name' => esc_html__( 'Before Shop Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_wooproduct_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Shop Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_wooproduct_section_hook',
					'type' => 'textarea',
				),

	
				// Callout Section
	
				array(
					'name' => esc_html__( 'Before Callout Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_callout_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Callout Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_callout_section_hook',
					'type' => 'textarea',
				),


				// Client Section
	
				array(
					'name' => esc_html__( 'Before Client Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_client_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Client Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_client_section_hook',
					'type' => 'textarea',
				),
				
				
				
			    // Footer Section
				
				
				array(
					'name' => esc_html__( 'Before Footer', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_footer_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Footer', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_footer_section_hook',
					'type' => 'textarea',
				),
				


			),
		);
}else{

	$hooks_settings['standard'] = array(
			'title'  => '',
			'fields' => array(

			    // Top Header Section
			
				array(
					'name' => esc_html__( 'Before Header', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_header_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Header', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_header_section_hook',
					'type' => 'textarea',
				),

				// Slider Section
				
				array(
					'name' => esc_html__( 'Before Slider Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_slider_section_hook',
					'type' => 'textarea',
				),

				array(
					'name' => esc_html__( 'After Slider Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_slider_section_hook',
					'type' => 'textarea',
				),
				
				// Service Section
				
				array(
					'name' => esc_html__( 'Before Services Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_services_section_hook',
					'type' => 'textarea',
				),

				array(
					'name' => esc_html__( 'After Services Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_services_section_hook',
					'type' => 'textarea',
				),
							
				
				// Fun Section				

				array(
					'name' => esc_html__( 'Before Fun Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_fun_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Fun Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_fun_section_hook',
					'type' => 'textarea',
				),
				
                
                // Portfolio Section
				
				array(
					'name' => esc_html__( 'Before Portfolio Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_portfolio_section_hook',
					'type' => 'textarea',
				),

				array(
					'name' => esc_html__( 'After Portfolio Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_portfolio_section_hook',
					'type' => 'textarea',
				),

				// Blog Section
				

				array(
					'name' => esc_html__( 'Before News Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_news_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After News Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_news_section_hook',
					'type' => 'textarea',
				),
				
				// CTA section
				
				
				array(
					'name' => esc_html__( 'Before CTA Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_cta2_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After CTA Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_cta2_section_hook',
					'type' => 'textarea',
				),

								
				// About section
				
				
				array(
					'name' => esc_html__( 'Before About Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_about_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After About Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_about_section_hook',
					'type' => 'textarea',
				),

								
				// Testimonials Section
				
				
				array(
					'name' => esc_html__( 'Before Testimonials Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_testimonial_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Testimonials Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_testimonial_section_hook',
					'type' => 'textarea',
				),

	            // Team Section
				
				array(
					'name' => esc_html__( 'Before Team Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_team_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Team section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_team_section_hook',
					'type' => 'textarea',
				),

				
				// Callout Section
	
				array(
					'name' => esc_html__( 'Before Callout Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_callout_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Callout Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_callout_section_hook',
					'type' => 'textarea',
				),


				// Client Section
	
				array(
					'name' => esc_html__( 'Before Client Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_client_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Client Section', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_client_section_hook',
					'type' => 'textarea',
				),
				
				
				
			    // Footer Section
				
				
				array(
					'name' => esc_html__( 'Before Footer', 'wphester-plus' ),
					'id'   => 'wphester_plus_before_footer_section_hook',
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'After Footer', 'wphester-plus' ),
					'id'   => 'wphester_plus_after_footer_section_hook',
					'type' => 'textarea',
				),
				


			),
		);
}

		$hooks_settings = apply_filters( 'WPHester_Plus_Hooks_Settings_fields', $hooks_settings );

		return $hooks_settings;
	}

	/**
	 * Register settings
	 */
	public function register_settings() {

		if ( ! empty( $this->hooks_settings ) ) {

			if ( is_array( $this->hooks_settings ) ) {

				foreach ( $this->hooks_settings as $section => $data ) {

					/* Add a new section on the Hooks Settings page for each Hook */
					add_settings_section(
						$section, $data['title'], array(
							$this,
							'hooks_settings_section_callback',
						), 'WPHester_Plus_Hooks_Settings'
					);

					foreach ( $data['fields'] as $field ) {

						// Register field
						register_setting( 'WPHester_Plus_Hooks_Settings', 'wphester_hooks', '' );

						// Add field to page
						add_settings_field(
							'wphester_hooks[' . $field['id'] . ']', $field['name'], array(
								$this,
								'display_field',
							), 'WPHester_Plus_Hooks_Settings', $section, array(
								'field' => $field,
							)
						);
					}
				}
			}
		}
	}

	/**
	 * Callback for add_settings_section
	 */
	public function hooks_settings_section_callback() {
		$html = '';
		echo $html;
	}

	/**
	 * Generate HTML for displaying fields
	 *
	 * @param  array $args Field data.
	 * @return void
	 */
	public function display_field( $args ) {

		$field = $args['field'];

		$html = '';

		$option_name = $this->settings_base . $field['id'];
		$option      = get_option( 'wphester_hooks' );

		$data = '';
		if ( isset( $option[ $option_name ] ) ) {
			$data = $option[ $option_name ];
		} elseif ( isset( $field['default'] ) ) {
			$data = $field['default'];
		}

		switch ( $field['type'] ) {

			case 'textarea':
				$checked = '';
				if ( isset( $option[ $field['id'] . '_php' ] ) && 'true' == $option[ $field['id'] . '_php' ] ) {
					$checked = 'checked="checked"';
				}
				$html .= '<textarea class="wphester_hook_field_textarea" id="wphester_hooks[' . esc_attr( $field['id'] ) . ']" name="wphester_hooks[' . esc_attr( $field['id'] ) . ']" placeholder="' . ( ( ! empty( $field['description'] ) ) ? esc_attr( $field['description'] ) : '' ) . '" >' . esc_textarea( $data ) . '</textarea>';
				$html .= '<div class="execute"><input type="checkbox" name="wphester_hooks[' . esc_attr( $field['id'] ) . '_php]" id="wphester_hooks[' . esc_attr( $field['id'] ) . '_php]" value="true" ' . $checked . ' /> <label for="wphester_hooks[' . esc_attr( $field['id'] ) . '_php]">' . esc_html__( 'Execute PHP', 'wphester-plus' ) . '</label></div>';
				break;

			case 'checkbox':
				break;

		}

		echo $html;
	}

	/**
	 * Build hooks settings page
	 */
	public function build_hooks_settings_page() {

		$html = '';

		$html .= '<div class="wrap" id="WPHester_Plus_Hooks_Settings">';

		$html .= '<h1 class="wp-heading-inline">' . esc_html__( 'WPHester Plus hooks', 'wphester-plus' ) . '</h1>';

		$html .= '<input type="text" id="wphester_search_hooks" onkeyup="wphester_filter_hooks()" placeholder="Search hooks...">';

		$html .= '<div id="poststuff">';
		$html .= '<div id="post-body" class="metabox-holder columns-2">';
		$html .= '<form method="post" action="options.php" enctype="multipart/form-data">';

		/* Main page content */
		$html .= '<div id="post-body-content">';
		ob_start();
		settings_fields( 'WPHester_Plus_Hooks_Settings' );
		do_settings_sections( 'WPHester_Plus_Hooks_Settings' );
		$html .= ob_get_clean();
		$html .= '</div><!-- #post-body-content -->';

		/* Side box */
		$html .= '<div id="postbox-container-1">';
		$html .= '<div class="postbox">';
		$html .= '<h3 class="hndle">' . esc_html__( 'WPHester Plus hooks', 'wphester-plus' ) . '</h3>';
		$html .= '<div class="inside">';
		$html .= '<div class="submitbox" id="submitpost">';
		$html .= '<div id="minor-publishing">';
		$html .= '<p>' . esc_html__( 'Shortcodes are allowed. If you check the Execute PHP checkbox, you can also use PHP.', 'wphester-plus' ) . '</p>';
		$html .= '</div><!-- #minor-publishing -->';
		$html .= '<div id="major-publishing-actions">';
		$html .= '<div id="publishing-action">';
		$html .= '<input name="Submit" type="submit" class="button button-primary button-large" value="' . esc_attr( __( 'Save hooks', 'wphester-plus' ) ) . '" />';
		$html .= '</div><!-- .publishing-action -->';
		$html .= '<div class="clear"></div>';
		$html .= '</div><!-- #major-publishing-actions -->';
		$html .= '</div><!-- #submitpost -->';
		$html .= '</div><!-- .inside -->';
		$html .= '</div><!-- .postbox -->';
		$html .= '</div><!-- #postbox-container-1 -->';

		$html .= '</form>';
		$html .= '</div><!-- #post-body -->';
		$html .= '</div><!-- #poststuff -->';
		$html .= '</div><!-- #WPHester_Plus_Hooks_Settings -->';

		echo $html;
	}
}

$WPHester_Plus_Hooks_Settings = new WPHester_Plus_Hooks_Settings();