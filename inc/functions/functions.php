<?php
//  WPHester plus customizer register function
if (!function_exists('wphester_plus_customize_register')) :

$repeater_path = trailingslashit(WPHESTERP_PLUGIN_DIR) . '/inc/functions/customizer-repeater/functions.php';
if (file_exists($repeater_path)) {
    require_once( $repeater_path );
}

$page_editor_path = trailingslashit(WPHESTERP_PLUGIN_DIR) . '/inc/functions/customizer/customizer-page-editor/customizer-page-editor.php';
if (file_exists($page_editor_path)) {
    require_once( $page_editor_path );
}

function wphester_plus_customize_register($wp_customize) {

    $sections_customizer_data = array('slider','services','fun','portfolio','news','cta2','about','team','testimonial','wooproduct','cta1','client');
    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';

    if (!empty($sections_customizer_data)) {
        foreach ($sections_customizer_data as $section_customizer_data) {
            require('customizer/' . $section_customizer_data . '-section.php');
        }
    }

	//  customizer setting files including
    require_once ( 'customizer/customizer_layout_manager.php' );
    require_once ( 'customizer/customizer_theme_style.php' );
}
add_action('customize_register', 'wphester_plus_customize_register');
endif;
			
//  home sections file including
		 	 
$sections_data = array('slider','services','fun','portfolio','news','cta2','about','team','testimonial','wooproduct','cta1','client');
				
	    if (!empty($sections_data))
			{ 
			    foreach($sections_data as $section_data)
					{ 
						require_once('sections/'.$section_data.'-section.php');
					}	
			}
				
require_once('custom-style/custom-css.php');
require_once('pagination/wphester_plus_pagination.php');
require_once('breadcrumbs/breadcrumbs.php');
// Adding customizer files
 require_once ('helper-function-plus.php');
 require_once ('customizer/custom-control.php');
 require_once ('layout-functions.php');
 require_once ('customizer/customizer_sections_plus_settings.php' );
 require_once ('customizer/blog-options-plus.php' );
 require_once ('customizer/template.php');
 require_once ('customizer/footer-options.php' );
 require_once ('customizer/general_settings.php' );
 require_once ('customizer/customizer_typography.php' );
 require_once ('customizer/customizer_color_back_settings.php');

require_once('customizer/repeater-default-value.php');

//Alpha Color Control
require_once('customizer/customizer-alpha-color-picker/class-wphester-customize-alpha-color-control.php');

//Customizer Page Editor
require_once('customizer/customizer-page-editor/class/class-wphester-page-editor.php');

// Adding hooks files
require_once('hooks/functions.php');
require_once('hooks/wphester-hooks-settings.php');
require_once('hooks/wphester-hooks.php');


// Theme title
if (!function_exists('wphester_plus_head_title')) {

    function wphester_plus_head_title($title, $sep) {
        global $paged, $page;

        if (is_feed())
            return $title;

        // Add the site name
        $title .= get_bloginfo('name');

        // Add the site description for the home / front page
        $site_description = get_bloginfo('description');
        if ($site_description && ( is_home() || is_front_page() ))
            $title = "$title $sep $site_description";

        // Add a page number if necessary.
        if (( $paged >= 2 || $page >= 2 ) && !is_404())
            $title = "$title $sep " . sprintf(esc_html__('Page', 'wphester-plus'), max($paged, $page));

        return $title;
    }

}
add_filter('wp_title', 'wphester_plus_head_title', 10, 2);
function wphester_plus_customizer_live_preview() {
    wp_enqueue_script(
            'wphester-customizer-preview', WPHESTERP_PLUGIN_URL . '/inc/js/customizer.js', array(
        'jquery',
        'customize-preview',
            ), 999, true
    );
}

add_action('customize_preview_init', 'wphester_plus_customizer_live_preview');

add_action("customize_register", "wphester_plus_remove_defult_setting_customize_register");

function wphester_plus_remove_defult_setting_customize_register($wp_customize) {
    $wp_customize->remove_control("header_image");
}

// custom background
function wphester_plus_custom_background_function() {
    $page_bg_image_url = get_theme_mod('predefined_back_image', 'bg-img1.png');
    if ($page_bg_image_url != '') {
        echo '<style>body.boxed{ background-image:url("'.WPHESTERP_PLUGIN_URL.'/inc/images/bg-pattern/' . $page_bg_image_url . '");}</style>';
    }
}

add_action('wp_head', 'wphester_plus_custom_background_function', 10, 0);

function wphester_plus_alpha_remove_class($wp_classes) {
    unset($wp_classes[array_search("blog", $wp_classes)]);

    return $wp_classes;
}

add_filter('body_class', 'wphester_plus_alpha_remove_class');

//Post Navigation Menu
if (get_theme_mod('post_nav_style_setting', 'pagination') != "pagination") {
    /*
     * initial posts dispaly
     */

    function wphester_plus_script_load_more($args = array()) {
        //initial posts load
    global $template;
    $row='';
    $row_template = array("template-blog-grid-view-sidebar.php", "template-blog-grid-view.php", "template-blog-masonry-two-column.php", "template-blog-masonry-three-column.php");
    if (in_array(basename($template), $row_template))   
    {
        $row='row';
    }
        echo '<div id="ajax-content" class="'.$row.' content-area">';
            wphester_plus_ajax_script_load_more($args);
        echo '</div>';
        echo '<span id="ajax-content2" >';
        echo '</span>';
        echo '<a href="#" id="loadMore" class="'.get_theme_mod('post_nav_style_setting','pagination').'='.basename($template).'" data-page="1" data-url="'.admin_url("admin-ajax.php").'" >Load More</a>';
        //echo basename($template);
    }

    /*
     * create short code.
     */
    add_shortcode('ajax_posts', 'wphester_plus_script_load_more');

    /*
     * load more script call back
     */

    function wphester_plus_ajax_script_load_more($args) {
        global $template;
        //init ajax
        $ajax = false;
        //check ajax call or not
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
                strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $ajax = true;
        }
        //number of posts per page default
        $num = get_option('posts_per_page');
        //page number
        $paged =  empty($_POST['page']) ? 0 : $_POST['page'] + 1;
        //args
        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => $num,
            'paged' => $paged
        );
        $blog_i=0;
        $page_template = empty($_POST['ajaxPage_template']) ? '' : $_POST['ajaxPage_template'];
        //query
        $query = new WP_Query($args);
        //check
        if ($query->have_posts()):
            //loop articales
            while ($query->have_posts()): $query->the_post();
                //include articles template

                if (($page_template == 'template-blog-full-width.php') || (basename($template) == 'template-blog-full-width.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-content.php');
                } elseif (($page_template == 'template-blog-grid-view-sidebar.php') || (basename($template) == 'template-blog-grid-view-sidebar.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-blog-grid-content.php');
                } elseif (($page_template == 'template-blog-grid-view.php') || (basename($template) == 'template-blog-grid-view.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-blog-grid-view-content.php');
                } elseif (($page_template == 'template-blog-left-sidebar.php') || (basename($template) == 'template-blog-left-sidebar.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-content.php');
                } elseif (($page_template == 'template-blog-list-view-sidebar.php') || (basename($template) == 'template-blog-list-view-sidebar.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-list-view-content.php');
                } elseif (($page_template == 'template-blog-list-view.php') || (basename($template) == 'template-blog-list-view.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-list-view-content.php');
                } elseif (($page_template == 'template-blog-right-sidebar.php') || (basename($template) == 'template-blog-right-sidebar.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-content.php');
                } elseif (($page_template == 'template-blog-masonry-two-column.php') || (basename($template) == 'template-blog-masonry-two-column.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-masonary-content.php');
                } elseif (($page_template == 'template-blog-masonry-three-column.php') || (basename($template) == 'template-blog-masonry-three-column.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-masonary-three-col-content.php');
                } elseif (($page_template == 'template-blog-switcher.php') || (basename($template) == 'template-blog-switcher.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-switcher-content.php');
                } elseif (($page_template == 'template-blog-switcher-sidebar.php') || (basename($template) == 'template-blog-switcher-sidebar.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/ajax-switcher-content.php');
                } elseif (($page_template == 'home.php') || (basename($template) == 'home.php')) {
                    include (WPHESTERP_PLUGIN_DIR.'/inc/template-parts/content.php');
                }
                $blog_i++;
            endwhile;
        else:
            echo 0;
        endif;
        //reset post data
        wp_reset_postdata();
        //check ajax call
        if ($ajax)
            die();
    }

    /*
     * load more script ajax hooks
     */
    add_action('wp_ajax_nopriv_wphester_plus_ajax_script_load_more', 'wphester_plus_ajax_script_load_more');
    add_action('wp_ajax_wphester_plus_ajax_script_load_more', 'wphester_plus_ajax_script_load_more');

    /*
     * enqueue js script
     */
    add_action('wp_enqueue_scripts', 'wphester_plus_ajax_enqueue_script');
    /*
     * enqueue js script call back
     */

    function wphester_plus_ajax_enqueue_script() {
        global $template;
        if ((basename($template) == 'template-blog-full-width.php') || (basename($template) == 'template-blog-grid-view-sidebar.php') || (basename($template) == 'template-blog-grid-view.php') || (basename($template) == 'template-blog-left-sidebar.php') || (basename($template) == 'template-blog-list-view-sidebar.php') || (basename($template) == 'template-blog-list-view.php') || (basename($template) == 'template-blog-right-sidebar.php') || (basename($template) == 'template-blog-masonry-two-column.php') || (basename($template) == 'template-blog-masonry-three-column.php') || (basename($template) == 'template-blog-switcher.php') || (basename($template) == 'template-blog-switcher-sidebar.php') || (basename($template) == 'home.php')) {
            wp_enqueue_script('script_ajax',WPHESTERP_PLUGIN_URL.'/inc/js/script_ajax.js', array('jquery'), '1.0', true);
        }
    }

}


//Woocommerce single product 
if (!function_exists('wphester_plus_template_single_title')) {
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
    add_action('woocommerce_single_product_summary', 'wphester_plus_template_single_title', 5);

    function wphester_plus_template_single_title() {
        the_title('<h2 class="product_title entry-title">', '</h2>');
    }

}

add_filter('woocommerce_pagination_args', 'wphester_plus_filter_function_woocommerce_arr', 12);

function wphester_plus_filter_function_woocommerce_arr($array) {

    $array = array(// WPCS: XSS ok.
        'prev_text' => (is_rtl()) ? '&rarr;' : '&larr;',
        'next_text' => (is_rtl()) ? '&larr;' : '&rarr;',
        'type' => 'list',
    );

    return $array;
}

function wphester_slide_sidebars() {
    ?>
    <div style="display: none">
        <?php
        if ( is_customize_preview() ) {
            dynamic_sidebar( 'slider-widget-area' );

        }
        ?>
    </div>
    <?php
}
add_action( 'wphester_slider_sidebar','wphester_slide_sidebars' );

function wphester_plus_container_style(){?>
    <style type="text/css">
        .container.container_default{max-width: <?php echo get_theme_mod('container_width_pattern',1140);?>px;}
        .container.slider-caption{max-width: <?php echo get_theme_mod('container_slider_width',1140);?>px;}
        .wphester-cta1-container.container{max-width: <?php echo get_theme_mod('container_cta1_width',1140);?>px;}
        .wphester-service-container.container{max-width: <?php echo get_theme_mod('container_service_width',1140);?>px;}
        .wphester-cta2-container.container{max-width: <?php echo get_theme_mod('container_cta2_width',1140);?>px;}
        .wphester-portfolio-container.container{max-width: <?php echo get_theme_mod('container_portfolio_width',1140);?>px;}
        .wphester-about-container.container{max-width: <?php echo get_theme_mod('container_about_width',1140);?>px;}
        .wphester-tesi-container.container{max-width: <?php echo get_theme_mod('container_testimonial_width',1140);?>px;}
        .wphester-newz.container{max-width: <?php echo get_theme_mod('container_home_blog_width',1140);?>px;}
        .wphester-fun-container.container{max-width: <?php echo get_theme_mod('container_fun_fact_width',1140);?>px;}
        .wphester-team-container.container{max-width: <?php echo get_theme_mod('container_team_width',1140);?>px;}
        .wphester-shop-container.container{max-width: <?php echo get_theme_mod('container_shop_width',1140);?>px;}
        .wphester-client-container.container{max-width: <?php echo get_theme_mod('container_clients_width',1140);?>px;}
        </style>    
<?php
}
add_action('wp_head','wphester_plus_container_style',11);

//Admin customizer preview
if ( ! function_exists( 'wphester_plus_customizer_preview_scripts' ) ) {
    function wphester_plus_customizer_preview_scripts() {
        wp_enqueue_script( 'wphester-plus-customizer-preview', WPHESTERP_PLUGIN_URL . 'inc/js/customizer-preview.js', array( 'customize-preview', 'jquery' ) );
    }
}
add_action( 'customize_preview_init', 'wphester_plus_customizer_preview_scripts' );