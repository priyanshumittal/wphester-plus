<?php
//call the action for the client section
add_action('wphester_plus_client_action','wphester_plus_client_section');
//function for the client section
function wphester_plus_client_section()
{
$client_section_enable = get_theme_mod('client_section_enable', true);
    if($client_section_enable != false)
    {
        // Client Callback
            $client_items=get_theme_mod('client_items',4);
            $atts=array(
                      'items' => $client_items,
                    );
            $client_section=wphester_client_callback($atts);
            echo $client_section;
    }
}