<?php
//call the action for the team section
add_action('wphester_plus_team_action','wphester_plus_team_section');
//function for the services section
function wphester_plus_team_section()
{
$team_section_enable = get_theme_mod('team_section_enable', true);
    if ($team_section_enable != false) {
     // Team Callback
            $atts=array(
                    'format' => 'slide',
                    'style' => get_theme_mod('home_team_design_layout',1),
                    'items' =>'3',
                    'col' =>'3',
                    );

            $team_section=wphester_team_callback($atts);
            echo $team_section;
    }
}