<?php
//call the action for the wooproduct section
add_action('wphester_plus_wooproduct_action','wphester_plus_wooproduct_section');
//function for the wooproduct section
function wphester_plus_wooproduct_section()
{
if ( class_exists( 'WooCommerce' ) ) 
{
	$shop_section_enable = get_theme_mod('shop_section_enable', true);
	if($shop_section_enable != false)
	{
        $shop_animation_speed = get_theme_mod('shop_animation_speed', 3000);
        $shop_smooth_speed = get_theme_mod('shop_smooth_speed', 1000);
        $shop_nav_style = get_theme_mod('shop_nav_style', 'bullets');
        $isRTL = (is_rtl()) ? (bool) true : (bool) false;
        $shopettings = array('shop_animation_speed' => $shop_animation_speed,
            'shop_smooth_speed' => $shop_smooth_speed,
            'shop_nav_style' => $shop_nav_style,
            'rtl' => $isRTL);
        wp_register_script('wphester-shop',WPHESTERP_PLUGIN_URL.'/inc/js/front-page/shop.js',array('jquery'));
		wp_localize_script('wphester-shop','shop_settings',$shopettings);
		wp_enqueue_script('wphester-shop');?>
		<section class="section-space shop bg-default-lite">
			<div class="wphester-shop-container container">
				<?php $home_shop_section_title = get_theme_mod('home_shop_section_title',__('Featured Products','wphester-plus'));
				$home_shop_section_discription = get_theme_mod('home_shop_section_discription',__('Our amazing products','wphester-plus')); 
				if((!empty($home_shop_section_title)) || (!empty($home_shop_section_discription)) ) 
				{ 
				?>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-xs-12">
							<div class="section-header">
								<?php if (!empty($home_shop_section_title)) { ?>
	                                <h2 class="section-title"><?php echo esc_html($home_shop_section_title); ?></h2>                                
	                            <?php } ?>
								<?php if(!empty($home_shop_section_discription)):?>
									<p class="section-subtitle"><?php echo esc_html($home_shop_section_discription); ?></p>
								<?php endif; ?>
							</div>
						</div>						
					</div>	
				<?php 
				}
				$args	= array(
					'post_type' => 'product',
				);
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'product_visibility',
						'field'    => 'name',
						'terms'    => 'exclude-from-catalog',
						'operator' => 'NOT IN',
					),
				);
				?>	
				<div class="row">
					<div id="shop-carousel" class="owl-carousel owl-theme col-md-12">	
						<?php
						$product_id=1;
						$loop = new WP_Query( $args );
						//print_r($loop);
						while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
							<div class="item <?php the_title(); ?>" data-profile="<?php echo esc_attr($loop->post->ID); ?>">
								<div class="products">
									<div class="item-img">
										<?php //print_r($product);
										the_post_thumbnail('full',array('class'=>'img-fluid'));?>
										<span class="woocommerce-price"><?php echo get_woocommerce_currency_symbol();?><?php echo $product->get_price();?></span>
										<div class="add-to-cart">
											<?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>
										</div> 									 	
									</div>
									<div class="product_meta">
										<span class="posted_in">
										  <?php 
										     $terms = get_the_terms( $product->get_id(), 'product_cat' );//print_r($terms);
										       foreach ($terms as $term) {
										            $product_cat = $term->name;
										      		$link = get_term_link( $term->term_id, 'product_cat' );
										            echo '<a href="'.$link.'" rel="wphester-cat tag">'.$product_cat.'</a>';							             
										          }?></span>      
										 <p class="stars">
										    	<?php 
										    	$avrg_str=$product->get_average_rating();
										    	
										    	for($i=1;$i<=$avrg_str;$i++){
										    	 echo '<a class="star-'.$i.'" href="#">'.$i.'</a>';
										    	}
										    	?>
										 </p>
                                    	<h5 class="woocommerce-loop-product__title"><a href="<?php the_permalink(); ?>" title="" tabindex="-1"><?php the_title(); ?></a></h5>										
									</div>
								</div>
							</div>
						<?php $product_id++; 
						if($product_id>8)
						{
							break;
						}
					endwhile; ?>
						<?php  wp_reset_postdata(); ?>
					</div>									
				</div>	
			</div>
		</section>
	<?php } 
}
}