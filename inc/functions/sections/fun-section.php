<?php
//call the action for the fun-acts section
add_action('wphester_plus_fun_action','wphester_plus_fun_section');
//function for the fun-acts section
function wphester_plus_fun_section()
{
$funfact_section_enabled = get_theme_mod('funfact_section_enabled', true);
    if($funfact_section_enabled != false)
    {
        // Funfact Callback
            $atts=array(
                      'col' => '4',
                    );

            $funfact_section=wphester_funfact_callback($atts);
            echo $funfact_section;
    }
}