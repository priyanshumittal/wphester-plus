<?php
//call the action for the cta2 section
add_action('wphester_plus_cta_action','wphester_plus_cta_section');
//function for the cta2 section
function wphester_plus_cta_section()
{
$cta_section_enable  = get_theme_mod('cta2_section_enable', true);
	if($cta_section_enable != false) 
	{ 
		// CTA2 Callback
	        $cta_section=wphester_cta_callback();
	        echo $cta_section;
	}
}