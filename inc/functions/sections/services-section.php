<?php
//call the action for the services section
add_action('wphester_plus_services_action','wphester_plus_services_section');
//function for the services section
function wphester_plus_services_section()
{
$wphester_home_service_enabled = get_theme_mod('home_service_section_enabled', true);
    if($wphester_home_service_enabled != false)
    {
        // Service Callback
            $atts=array(
                      'style' => get_theme_mod('home_serive_design_layout',1),
                      'col' => get_theme_mod('home_service_col',3),
                );

            $service_section=wphester_service_callback($atts);
            echo $service_section; 
    }
}