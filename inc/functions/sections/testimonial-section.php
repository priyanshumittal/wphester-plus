<?php
//call the action for the testimonial section
add_action('wphester_plus_testimonial_action','wphester_plus_testimonial_section');
//function for the testimonial section
function wphester_plus_testimonial_section()
{
$testimonial_section_enable = get_theme_mod('testimonial_section_enable', true);
    if($testimonial_section_enable != false)
    {
        // Testimonial Callback
            $atts=array(
                    'format' => 'slide',
                    'style' => get_theme_mod('home_testimonial_design_layout',1),
                    'items' => get_theme_mod('home_testimonial_slide_item',1),
                    'col' =>'1',
                    );

            $testimonial_section=wphester_testimonial_callback($atts);
            echo $testimonial_section;
    } 
}