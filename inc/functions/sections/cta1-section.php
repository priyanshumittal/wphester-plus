<?php
//call the action for the cta1 section
add_action('wphester_plus_callout_action','wphester_plus_callout_section');
//function for the cta1 section
function wphester_plus_callout_section()
{
$callout_section_enable  = get_theme_mod('cta1_section_enable', true);
	if($callout_section_enable != false){ 
		// CTA1 Callback
	        $callout_section=wphester_callout_callback();
	        echo $callout_section; 
	} 
}