<?php
//call the action for the about section
add_action('wphester_plus_about_action','wphester_plus_about_section');
//function for the about section
function wphester_plus_about_section()
{
$about_section_enable  = get_theme_mod('about_section_enable', true);
	if($about_section_enable != false){
		// About Callback
	        $about_section=wphester_about_callback();
	        echo $about_section; 
	} 
}