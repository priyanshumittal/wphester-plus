<?php
// define function for custom color setting
function wphester_plus_custom_light() {

  $link_color = get_theme_mod('link_color','#56ABAB');
  list($r, $g, $b) = sscanf($link_color, "#%02x%02x%02x");
  $r = $r - 50;
  $g = $g - 25;
  $b = $b - 40;

  if ( $link_color != '#ff0000' ) :
  ?>
<style type="text/css">
<?php if(!is_rtl()):?>
blockquote 
{
border-left: 0.1875rem solid <?php echo esc_attr($link_color); ?>;
}
<?php else:?>
blockquote 
{
border-right: 0.1875rem solid <?php echo esc_attr($link_color); ?>;
}
<?php endif;?> 
dl dd a,
dl dd a:hover,
dl dd a:focus,
ul li a:focus,
ul li a:hover {
    color: <?php echo esc_attr($link_color); ?>;
}

.entry-meta .tag-links a:hover,
.entry-meta .tag-links a:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.entry-content a:hover,
.entry-content a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

a:hover,
a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.entry-meta a:hover,
.entry-meta a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

input[type="text"]:focus,
input[type="email"]:focus,
input[type="url"]:focus,
input[type="password"]:focus,
input[type="search"]:focus,
input[type="number"]:focus,
input[type="tel"]:focus,
input[type="range"]:focus,
input[type="date"]:focus,
input[type="month"]:focus,
input[type="week"]:focus,
input[type="time"]:focus,
input[type="datetime"]:focus,
input[type="datetime-local"]:focus,
input[type="color"]:focus,
textarea:focus {
    border-color: <?php echo esc_attr($link_color); ?>;
}

button,
input[type="button"],
input[type="submit"] {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

button.secondary,
input[type="reset"],
input[type="button"].secondary,
input[type="reset"].secondary,
input[type="submit"].secondary {
    color: <?php echo esc_attr($link_color); ?>;
}

button:hover,
button:focus,
input[type="button"]:hover,
input[type="button"]:focus,
input[type="submit"]:hover,
input[type="submit"]:focus {
    color: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

button.secondary:hover,
button.secondary:focus,
input[type="reset"]:hover,
input[type="reset"]:focus,
input[type="button"].secondary:hover,
input[type="button"].secondary:focus,
input[type="reset"].secondary:hover,
input[type="reset"].secondary:focus,
input[type="submit"].secondary:hover,
input[type="submit"].secondary:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.btn-default {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}


/*.btn-default:hover,.btn-default:focus, .btn-default:active { border: 1px solid <?php echo esc_attr($link_color); ?>;color:<?php echo esc_attr($link_color); ?>; }*/

.btn-light:hover,
.btn-light:focus,
.btn-light:active {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.btn-light:not(:disabled):not(.disabled).active,
.btn-light:not(:disabled):not(.disabled):active,
.show>.btn-light.dropdown-toggle {
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.btn-default-dark {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.btn-default-dark:hover,
.btn-default-dark:focus,
.btn-default-dark:active {
    border: 1px solid <?php echo esc_attr($link_color); ?>;
    color: <?php echo esc_attr($link_color); ?>;
}

.btn-border {
    border: 0.125rem solid <?php echo esc_attr($link_color); ?>;
}

.btn-border:hover,
.btn-border:focus,
.btn-border:active {
    border: 2px solid <?php echo esc_attr($link_color); ?>;
    color: <?php echo esc_attr($link_color); ?>;
}

input:-webkit-autofill,
input:-webkit-autofill:hover,
input:-webkit-autofill:focus,
textarea:-webkit-autofill,
textarea:-webkit-autofill:hover,
textarea:-webkit-autofill:focus,
select:-webkit-autofill,
select:-webkit-autofill:hover,
select:-webkit-autofill:focus {
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}


/*.top-header li a:hover,.top-header li a:focus{color:<?php echo esc_attr($link_color); ?>;}*/

.custom-social-icons li>a:focus,
.custom-social-icons li>a:hover {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.contact .custom-social-icons li>a:hover,
.contact .custom-social-icons li>a:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
}

#searchbar_fullscreen .btn {
    background-color: <?php echo esc_attr($link_color); ?>;
}

#searchbar_fullscreen .close {
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.navbar .search-box-outer .dropdown-menu {
    border-top: solid 1px <?php echo esc_attr($link_color); ?>;
}

.search-form input[type="submit"] {
    background: <?php echo esc_attr($link_color); ?> none repeat scroll 0 0;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.owl-carousel .owl-prev:hover,
.owl-carousel .owl-prev:focus,
.owl-carousel .owl-next:hover,
.owl-carousel .owl-next:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.section-separator {
    background: <?php echo esc_attr($link_color); ?>;
}

.section-header .section-subtitle {
    color: <?php echo esc_attr($link_color); ?>;
}

.section-header .section-subtitle:after {
    background: <?php echo esc_attr($link_color); ?>;
}

.services .card-body:before {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.services .service-icon i.fa {
    color: <?php echo esc_attr($link_color); ?>;
}

.services .btn-small {
    color: <?php echo esc_attr($link_color); ?>;
}

.services .card .service-big-icon {
    color: <?php echo esc_attr($link_color); ?>;
}

.services .post:hover,
.services .post:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.services .post-thumbnail i.fa {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.services .post:hover .post-thumbnail i.fa,
.services .post:focus .post-thumbnail i.fa {
    color: <?php echo esc_attr($link_color); ?>;
}

.services3 .post-thumbnail i.fa {
    background: <?php echo esc_attr($link_color); ?>;
    box-shadow: <?php echo esc_attr($link_color); ?> 0px 0px 0px 1px;
}

.services3 .post:hover .post-thumbnail i.fa,
.services3 .post:focus .post-thumbnail i.fa {
    color: <?php echo esc_attr($link_color); ?>;
}

.services4 .post-thumbnail i.fa {
    color: <?php echo esc_attr($link_color); ?>;
}

.funfact-inner:before {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.testimonial-1 .avatar {
    background: <?php echo esc_attr($link_color); ?>;
}

.testimonial-block .entry-content:before {
    color: <?php echo esc_attr($link_color); ?>;
}

.testimonial.testimonial-1 .rating {
    color: <?php echo esc_attr($link_color); ?>;
}

.testimonial-2 .testimonial-block {
    border-left: 0.25rem solid <?php echo esc_attr($link_color); ?>;
}

.testimonial-2 .testimonial-block:before {
    border-top: 1.563rem solid <?php echo esc_attr($link_color); ?>;
}

#cta-video {
    color: <?php echo esc_attr($link_color); ?>;
    border: 0.3rem solid <?php echo esc_attr($link_color); ?>;
}

.video-btn a:after {
    border-left: 1.563rem solid <?php echo esc_attr($link_color); ?>;
}

.blog .blog-btn .btn-small:hover,
.blog .blog-btn .btn-small:focus,
.blog .blog-btn .btn-small:hover i,
.blog .blog-btn .btn-small:focus i {
    color: <?php echo esc_attr($link_color); ?>;
}

.post-content {
    padding: 1.25rem 1.5rem;
}

.entry-date {
    background: <?php echo esc_attr($link_color); ?>;
}

.entry-meta span:after {
    color: <?php echo esc_attr($link_color); ?>;
}

.entry-content p a:hover,
.entry-content p a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.blog .btn-small i {
    color: <?php echo esc_attr($link_color); ?>;
}

.entry-meta i {
    color: <?php echo esc_attr($link_color); ?>;
}

.blog a:hover i,
.blog a:hover span,
.blog a:focus i,
.blog a:focus span {
    color: <?php echo esc_attr($link_color); ?>;
}

.sidebar .widget .custom-social-icons li>a {
    color: <?php echo esc_attr($link_color); ?>;
}

.sidebar .widget .custom-social-icons li>a:hover,
.sidebar .widget .custom-social-icons li>a:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.sidebar .widget ul li a:hover,
.sidebar .widget ul li a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}


/* .sidebar .widget .widget-title {
    background-color: <?php echo esc_attr($link_color); ?>;
} */

.widget .search-submit,
.widget .search-field [type=submit],.sidebar .wp-block-search .wp-block-search__button {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.sidebar .woocommerce-widget-layered-nav li:before,
.sidebar .widget_recent_reviews li:before,
.sidebar .widget_top_rated_products li:before,
.sidebar .widget_products li:before,
.sidebar .widget_nav_menu li:before,
.sidebar .widget_pages li:before,
.sidebar .widget_product_categories li:before,
.sidebar .widget_links li:before,
.sidebar .widget_categories li:before,
.sidebar .widget_archive li:before,
.sidebar .widget_recent_entries li:before,
.sidebar .widget_meta li:before {
    color: <?php echo esc_attr($link_color); ?>;
}

.sidebar .widget_recent_comments li:before {
    color: <?php echo esc_attr($link_color); ?>;
}

.widget .tagcloud a:hover,
.widget .tagcloud a:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.related-post .entry-title a:hover,
.related-post .entry-title a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.related-post .single-post .fa {
    color: <?php echo esc_attr($link_color); ?>;
}

.comment-form .comment-reply-title {
    color: <?php echo esc_attr($link_color); ?>;
}

.comment-form .blog-form-group:after {
    color: <?php echo esc_attr($link_color); ?>;
}

.comment-form .blog-form-group-textarea:after {
    color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar .widget_text.site-info a:hover,
.footer-sidebar .widget_text.site-info a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.pagination a.active {
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.pagination a:before {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.slideouticons label.mainlabel {
    background: <?php echo esc_attr($link_color); ?>;
}

.slideouticons .icon-wrapper ul li a:hover,
.slideouticons .icon-wrapper ul li a:focus {
    background: <?php echo esc_attr($link_color); ?>;
}

.slideouticons input:checked~label.mainlabel {
    color: <?php echo esc_attr($link_color); ?>;
}

.team .team-grid .list-inline-item a {
    color: <?php echo esc_attr($link_color); ?>;
}

.team .team-grid .list-inline-item a:hover {
    background: <?php echo esc_attr($link_color); ?>;
}

.team .portfolio-thumbnail ul li a:hover i,
.team .portfolio-thumbnail ul li a:focus i,
.team2 .list-inline li a:hover i,
.team2 .list-inline li a:focus i {
    color: <?php echo esc_attr($link_color); ?>;
}

.team .name,
.team2 .team-grid .card-body h4,
.team3 .team-grid .card-body h4,
.team4 .team-grid .card-body h4,
.team3 .team-grid .card-body .list-inline li>a:hover,
.team3 .team-grid .card-body .list-inline li>a:focus,
.team4 .team-grid .card-body .list-inline li>a:hover,
.team4 .team-grid .card-body .list-inline li>a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.woocommerce ul.products li.product .onsale,
.products span.onsale,
.woocommerce span.onsale {
    background: <?php echo esc_attr($link_color); ?>;
}

span.woocommerce-price {
    background: <?php echo esc_attr($link_color); ?>;
}

.add-to-cart a {
    background: <?php echo esc_attr($link_color); ?>;
}

.add-to-cart a:hover,
.add-to-cart a:focus {
    background: <?php echo esc_attr($link_color); ?>;
}

.shop .product_meta .posted_in a:hover,
.shop .product_meta .posted_in a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.product-price .woocommerce-Price-amount {
    color: <?php echo esc_attr($link_color); ?>;
}

.products .onsale:hover,
.products .onsale:focus {
    background: <?php echo esc_attr($link_color); ?>;
}

.callout .bg-btn {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.contact-icon i {
    color: <?php echo esc_attr($link_color); ?>;
}

.cont-info address>a:hover,
.cont-info address>a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.cont-info address>i {
    color: <?php echo esc_attr($link_color); ?>;
}

.contant-form .wpcf7-form-control-wrap:after {
    color: <?php echo esc_attr($link_color); ?>;
}

.contact-page.contact-info .contact-icon i {
    color: <?php echo esc_attr($link_color); ?>;
}

.contact-info a:hover address,
.contact-info a:focus address {
    color: <?php echo esc_attr($link_color); ?>;
}

.contact-info-section .contact-icon i.fa {
    color: <?php echo esc_attr($link_color); ?>;
}

.contact4 .contact-widget:hover,
.contact4 .contact-widget:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.site-info a:hover,
.site-info a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.footer-social-links {
    background: <?php echo esc_attr($link_color); ?>;
}
.page-breadcrumb li ,
.page-breadcrumb li a:hover, .page-breadcrumb li a:focus ,
.page-breadcrumb .icon::before, .page-breadcrumb .breadcrumb_last,nav.rank-math-breadcrumb span, .page-breadcrumb.text-center span.post-page.current-item{
  color: <?php echo esc_attr($link_color); ?>;
}
.page-breadcrumb li{
    color: <?php echo esc_attr($link_color); ?>;
}

.page-breadcrumb li a:hover,
.page-breadcrumb li a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.page-breadcrumb .icon::before {
    color: <?php echo esc_attr($link_color); ?>;
}

.list-style-four li:before {
    color: <?php echo esc_attr($link_color); ?>;
}

.about-subtitle {
    color: <?php echo esc_attr($link_color); ?>;
}

.about-header .btn-default:hover,
.about-header .btn-default:focus {
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.portfolio .md-pills .nav-link.active,
.portfolio .md-pills .nav-link:hover,
.portfolio .md-pills .nav-link:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.portfolio .tab-content .portfolio-thumbnail i {
    background-color: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.portfolio button:hover,
.portfolio button:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.error-page .btn-small {
    color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar .woocommerce .posted_in a:hover,
.footer-sidebar .woocommerce-product-rating a:hover,
.footer-sidebar .woocommerce .tagged_as a:hover,
.footer-sidebar .woocommerce-cart table.cart td a:hover,
.footer-sidebar .woocommerce ul.cart_list li a:hover,
.footer-sidebar .woocommerce ul.product_list_widget li a:hover {
    color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar .widget_text.site-info .custom-social-icons li>a {
    color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar .widget_text.site-info .custom-social-icons li>a:hover,
.footer-sidebar .widget_text.site-info .custom-social-icons li>a:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar a:hover,
.footer-sidebar a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar .woocommerce-widget-layered-nav ul a:before,
.footer-sidebar .widget_recent_reviews ul a:before,
.footer-sidebar .widget_top_rated_products ul a:before,
.footer-sidebar .widget_products ul a:before,
.footer-sidebar .widget_nav_menu ul a:before,
.footer-sidebar .widget_pages ul a:before,
.footer-sidebar .widget_product_categories ul a:before,
.footer-sidebar .widget_links ul a:before,
.footer-sidebar .widget_categories ul a:before,
.footer-sidebar .widget_archive ul a:before,
.footer-sidebar .widget_recent_entries ul a:before,
.footer-sidebar .widget_meta ul a:before {
    color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar .widget_recent_comments ul li:before {
    color: <?php echo esc_attr($link_color); ?>;
}

.scroll-up a {
    background: <?php echo esc_attr($link_color); ?>;
    color: #ffffff;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.subscribe-form .btn-default:hover,
.subscribe-form .btn-default:focus {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.site-footer .subscribe-form .input-group .form-control {
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.layout4 .header-sidebar .bottom-header .contact-widget .title {
    color: <?php echo esc_attr($link_color); ?>;
}

.layout4 .navbar-nav:not(.sm-collapsible) .sm-nowrap>li.show>.dropdown-item {
    background-color: <?php echo esc_attr($link_color); ?> !important;
}

@media (max-width: 1024px) {
    .layout4 .navbar.navbar-light {
        background-color: <?php echo esc_attr($link_color); ?>;
    }
}

.navbar {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.footer-sidebar .widget .widget-title:after {
    background: <?php echo esc_attr($link_color); ?>;
}

.navbar.navbar3 .nav .nav-item:hover .nav-link,
.navbar.navbar3 .nav .nav-item:focus .nav-link,
.navbar.navbar3 .nav .nav-item.active .nav-link {
    color: <?php echo esc_attr($link_color); ?>;
}

.navbar.navbar3 ul li a .menu-text:hover:after,
.navbar.navbar3 ul li a .menu-text:focus:after,
.navbar.navbar3 .nav li.active .nav-link .menu-text:after,
.navbar3 .navbar-nav .show .dropdown-menu>.active>.menu-text:after,
.navbar3 .navbar-nav .show .dropdown-menu>.active>.menu-text:focus {
    background: <?php echo esc_attr($link_color); ?>;
}

.navbar3 .dropdown-item.active,
.navbar3 .dropdown-item:active,
.navbar3 .dropdown-item:hover,
.navbar3 .dropdown-item:focus,
.navbar3 .navbar-nav .dropdown-item:hover,
.navbar3 .navbar-nav .dropdown-item:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.navbar3 .navbar-nav:not(.sm-collapsible) .sm-nowrap>li.show>.dropdown-item {
    background-color: <?php echo esc_attr($link_color); ?> !important;
}

.navbar3 a.text-dark:focus,
.navbar3 a.text-dark:hover {
    color: <?php echo esc_attr($link_color); ?>!important;
}

.layout4 .navbar .navbar-collapse,
.layout4 .navbar .options-box {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.owl-theme .owl-dots .owl-dot.active span {
    background-color: <?php echo esc_attr($link_color); ?>;
}


/*.error-page .overlay{
  background-color: rgba(92,162,223, 0.7);
}
*/

.btn-color {
    background: transparent;
    color: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.btn-color:hover {
    background-color: <?php echo esc_attr($link_color); ?>;
    color: #ffffff;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.btn-color-2 {
    background: <?php echo esc_attr($link_color); ?>;
    color: #ffffff;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.btn-color-2:hover,.btn-color-2:focus {
    background-color: transparent;
    color: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.woocommerce-loop-product__title:hover,
.woocommerce-loop-product__title:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.woocommerce ul.products li.product .button,
.owl-item .item .cart .add_to_cart_button {
    background: <?php echo esc_attr($link_color); ?>;
}

.woocommerce div.product form.cart .button,
.woocommerce a.button,
.woocommerce a.button:hover,
.woocommerce a.button,
.woocommerce .woocommerce-Button,
.woocommerce .cart input.button,
.woocommerce input.button.alt,
.woocommerce button.button,
.woocommerce #respond input#submit,
.woocommerce .cart input.button:hover,
.woocommerce .cart input.button:focus,
.woocommerce input.button.alt:hover,
.woocommerce input.button.alt:focus,
.woocommerce input.button:hover,
.woocommerce input.button:focus,
.woocommerce button.button:hover,
.woocommerce button.button:focus,
.woocommerce #respond input#submit:hover,
.woocommerce #respond input#submit:focus,
.woocommerce-cart .wc-proceed-to-checkout a.checkout-button {
    background: <?php echo esc_attr($link_color); ?>;
}

.woocommerce ul.product_list_widget li a:hover,
.woocommerce ul.product_list_widget li a:focus,
.woocommerce .posted_in a:hover,
.woocommerce .posted_in a:focus {
    color: <?php echo esc_attr($link_color); ?>;
}

.woocommerce-cart.woocommerce-page thead th {
    border-bottom: 2px solid <?php echo esc_attr($link_color); ?>;
}

.woocommerce #respond input#submit.disabled:hover,
.woocommerce #respond input#submit:disabled:hover,
.woocommerce #respond input#submit:disabled[disabled]:hover,
.woocommerce a.button.disabled:hover,
.woocommerce a.button:disabled:hover,
.woocommerce a.button:disabled[disabled]:hover,
.woocommerce button.button.disabled:hover,
.woocommerce button.button:disabled:hover,
.woocommerce button.button:disabled[disabled]:hover,
.woocommerce input.button.disabled:hover,
.woocommerce input.button:disabled:hover,
.woocommerce input.button:disabled[disabled]:hover {
    background-color: <?php echo esc_attr($link_color); ?>;
    opacity: 0.7;
}

.woocommerce-info {
    border-top-color: <?php echo esc_attr($link_color); ?>;
}

.woocommerce-info::before {
    color: <?php echo esc_attr($link_color); ?>;
}

.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt {
    background-color: <?php echo esc_attr($link_color); ?>;
    color: #fff;
    -webkit-font-smoothing: antialiased;
}

/*.woocommerce #respond input#submit.alt:hover,
.woocommerce a.button.alt:hover,
.woocommerce button.button.alt:hover,
.woocommerce input.button.alt:hover {
    background-color: #ffffff;
    color: <?php echo esc_attr($link_color); ?> !important;
    border-color: <?php echo esc_attr($link_color); ?> !important;
}*/

.woocommerce-order-received .page .post {
    box-shadow: 0px 0px 0px 2px <?php echo esc_attr($link_color); ?>;
}

.woocommerce-order-received ul.order_details li {
    border-right: 1px dashed <?php echo esc_attr($link_color); ?>;
}

/*.woocommerce-account.admin-bar .page .post {
    box-shadow: 0px 0px 0px 2px <?php echo esc_attr($link_color); ?>;
}*/

.panel-outer .search-form input[type="submit"]:hover,
.panel-outer .search-form input[type="submit"]:focus {
    color: #ffffff;
}

.woocommerce .product_cat-singles .cart .single_add_to_cart_button:hover {
    color: #ffffff !important;
}

.woocommerce nav.woocommerce-pagination ul li a:focus,
.woocommerce nav.woocommerce-pagination ul li a:hover,
.woocommerce nav.woocommerce-pagination ul li span.current {
    background: <?php echo esc_attr($link_color); ?>;
    color: #ffffff;
}

.sidebar .wp-block-tag-cloud .tag-cloud-link:hover,
.sidebar .wp-block-tag-cloud .tag-cloud-link:focus {
    background-color: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}

.navbar6.navbar .nav .nav-item:hover .nav-link,
.navbar6.navbar .nav .nav-item:focus .nav-link,
.navbar6.navbar .nav .nav-item.active .nav-link {
    color: #ffffff;
    background-color: <?php echo esc_attr($link_color); ?>;
}

.services5 .post-thumbnail i.fa {
    background: <?php echo esc_attr($link_color); ?>;
}

.services5 .post:hover .post-thumbnail i.fa {
    color: <?php echo esc_attr($link_color); ?>;
}

.services5 .post:hover {
    background: <?php echo esc_attr($link_color); ?>;
}
 .navbar1.navbar .nav .nav-item:hover .nav-link,
 .navbar1.navbar .nav .nav-item:focus .nav-link,
 .navbar1.navbar .nav .nav-item.active .nav-link {
    color: #000000;
}
.navbar1.custom .nav .nav-item.active .nav-link{color: #000000 !important;}

.navbar5.navbar .nav .nav-item:hover .nav-link,
 .navbar5.navbar .nav .nav-item:focus .nav-link,
 .navbar5.navbar .nav .nav-item.active .nav-link {
    color: #000000;
}
.navbar5 .nav .nav-item.active .nav-link{color: #000000 !important;}
.dark .navbar.navbar5 .nav .nav-item.active .nav-link{color: <?php echo esc_attr($link_color); ?> !important;}
.dark .navbar1.custom .nav .nav-item.active .nav-link{color: <?php echo esc_attr($link_color); ?> !important;}

.dark .navbar1.navbar .nav .nav-item:hover .nav-link,
.dark .navbar1.navbar .nav .nav-item:focus .nav-link,
.dark .navbar1.navbar .nav .nav-item.active .nav-link {
    color: <?php echo esc_attr($link_color); ?>;
}

.dark .navbar1.navbar ul li a .menu-text:hover:after,
.dark .navbar1.navbar ul li a .menu-text:focus:after,
.dark .navbar1.navbar .nav li.active .nav-link .menu-text:after {
    background-color: <?php echo esc_attr($link_color); ?>;
}

.dark .navbar.navbar1 .cart-header>a .cart-total {
    background: <?php echo esc_attr($link_color); ?>;
}
/*.dark .navbar .cart-header>a.cart-total {
    background: <?php echo esc_attr($link_color); ?>;
}*/
.navbar-nav .dropdown-item:hover,
.navbar-nav .dropdown-item.active,
.navbar-nav .dropdown-item:active,
.navbar-nav .dropdown-item:focus {
    background-color: transparent;
    color: <?php echo esc_attr($link_color); ?>;
}

.navbar ul li a.text-dark:focus,
.navbar ul li a.text-dark:hover {
    color: <?php echo esc_attr($link_color); ?> !important;
}

.navbar ul li a.text-dark,
.navbar ul li a.bg-light {
    color: #ffffff !important;
    background-color: <?php echo esc_attr($link_color); ?> !important;
}

.navbar2.navbar .nav .nav-item:hover .nav-link,
.navbar2.navbar .nav .nav-item:focus .nav-link,
.navbar2.navbar .nav .nav-item.active .nav-link {
    color: <?php echo esc_attr($link_color); ?>;
}

.navbar2.navbar ul li a .menu-text:hover:after,
.navbar2.navbar ul li a .menu-text:focus:after,
.navbar2.navbar .nav li.active .nav-link .menu-text:after,
.navbar2.navbar ul li a.dropdown-item .menu-text:hover:after,
.navbar2.navbar ul li a.dropdown-item .menu-text:focus:after,
.navbar2 .navbar-nav .show .dropdown-menu>.active>.menu-text:after,
.navbar2 .navbar-nav .show .dropdown-menu>.active>.menu-text:focus {
    background: <?php echo esc_attr($link_color); ?>;
}

.dark .navbar.navbar5 .nav .nav-item:hover .nav-link,
.dark .navbar.navbar5 .nav .nav-item:focus .nav-link,
.dark .navbar.navbar5 .nav .nav-item.active .nav-link {
    color: <?php echo esc_attr($link_color); ?>;
}

.dark .navbar.navbar5 ul li a .menu-text:hover:after,
.dark .navbar.navbar5 ul li a .menu-text:focus:after,
.dark .navbar.navbar5 .nav li.active .nav-link .menu-text:after,
.dark .navbar.navbar5 .navbar-nav .show .dropdown-menu>.active>.menu-text:after,
.dark .navbar.navbar5 .navbar-nav .show .dropdown-menu>.active>.menu-text:focus {
    width: 100%;
    background: <?php echo esc_attr($link_color); ?>;
    left: 0;
}


/* ================ All Header Button Css ============================ */

.dark .navbar1 .main-header-btn {
    color: #ffffff;
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.dark .navbar1 .main-header-btn:hover,
.dark .navbar1 .main-header-btn:focus,
.dark .navbar2 .main-header-btn:hover,
.dark .navbar2 .main-header-btn:focus  {
    color: <?php echo esc_attr($link_color); ?>;
    background-color: #ffffff;
    border-color: #ffffff;
}

.navbar2 .main-header-btn {
    color: #ffffff;
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.navbar3 .main-header-btn {
    color: #ffffff;
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.navbar3 .main-header-btn:hover,
.navbar3 .main-header-btn:focus {
    color: <?php echo esc_attr($link_color); ?>;
    background-color: #ffffff;
    border-color: #ffffff;
}

.dark .navbar5 .main-header-btn {
    color: #ffffff;
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.dark .navbar5 .main-header-btn:hover,
.dark .navbar5 .main-header-btn:focus {
    color: <?php echo esc_attr($link_color); ?>;
    background-color: #ffffff;
    border-color: #ffffff;
}

.navbar6 .main-header-btn {
    color: #ffffff;
    background-color: <?php echo esc_attr($link_color); ?>;
    border-color: <?php echo esc_attr($link_color); ?>;
}

.dark .navbar6 .main-header-btn:hover,
.dark .navbar6 .main-header-btn:focus {
    color: <?php echo esc_attr($link_color); ?>;
    background-color: #ffffff;
    border-color: #ffffff;
}
.woocommerce-info{
    border-top-color: <?php echo esc_attr($link_color); ?>;
}
.woocommerce-info::before {
   color: <?php echo esc_attr($link_color); ?>;
}
.woocommerce .widget_price_filter .ui-slider .ui-slider-range{
  color: <?php echo esc_attr($link_color); ?>;
}

.woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span:hover {
    background-color: <?php echo esc_attr($link_color); ?>;
}
.woocommerce .widget_price_filter .ui-slider .ui-slider-range,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,
.woocommerce ul.products li.product .button, 
.woocommerce div.product form.cart .button,
.woocommerce .widget_price_filter .price_slider_amount .button,
.woocommerce a.button.alt,.woocommerce .coupon .button:hover,.woocommerce a.button.alt:hover,
.woocommerce button.button.alt:hover,.woocommerce button.button.alt,.woocommerce #review_form #respond .form-submit input,
.woocommerce .woocommerce-message .button,.woocommerce a.button, .woocommerce a.button:hover {
   background-color: <?php echo esc_attr($link_color); ?>;
}

.woocommerce ul.products li.product .price ins, 
.woocommerce div.product p.price ins, 
.woocommerce ul.products li.product .price .woocommerce-Price-amount.amount, 
.woocommerce .variations td.label, 
.woocommerce .woocommerce-ordering select, 
.woocommerce-cart table.cart td.actions .coupon .input-text, 
.select2-container .select2-choice{
  color:<?php echo esc_attr($link_color); ?> ;
}
.woocommerce nav.woocommerce-pagination ul li a:before {
   background-color: <?php echo esc_attr($link_color); ?>;
}

/*.navbar .nav .nav-item:hover .nav-link, .navbar .nav .nav-item.active .nav-link, .dropdown-menu > li.active > a, .navbar .nav .nav-item.current_page_parent .nav-link {
    color: <?php echo esc_attr($link_color); ?>;
}*/
#testimonial-carousel2 .name:hover,
#testimonial-carousel3 .name:hover,
#testimonial-carousel4 .name:hover{
  color: <?php echo esc_attr($link_color); ?>;
}
.navbar.navbar6 .cart-header>a.cart-total {
    background: <?php echo esc_attr($link_color); ?>;
}
.navbar1 .navbar-nav .dropdown-item .menu-text:hover:after {
    background: <?php echo esc_attr($link_color); ?>;
}
.sidebar .widget .widget-title::before {
    background: <?php echo esc_attr($link_color); ?>;
}
.page-numbers.current {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;    
}
.pagination a.next:hover,
.pagination a.previous:hover,
.pagination a:hover,
.pagination a:focus {
    border-color: <?php echo esc_attr($link_color); ?>;
    background-color: <?php echo esc_attr($link_color); ?>;
}
.woocommerce-pagination ul li a.page-numbers:hover,
.woocommerce-pagination ul li a.page-numbers:focus {
    border-color: <?php echo esc_attr($link_color); ?>;
    background-color: <?php echo esc_attr($link_color); ?>;
}
.footer-sidebar .wp-block-tag-cloud .tag-cloud-link:hover,
.footer-sidebar .wp-block-tag-cloud .tag-cloud-link:focus {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}
footer {
    border-bottom: 4px solid <?php echo esc_attr($link_color); ?>;
}
.wp-block-tag-cloud .tag-cloud-link:hover,
.wp-block-tag-cloud .tag-cloud-link:focus {
    background: <?php echo esc_attr($link_color); ?>;
    border: 1px solid <?php echo esc_attr($link_color); ?>;
}
.wp-block-search__button:hover .search-icon,
.wp-block-search__button:focus .search-icon {
    fill: <?php echo esc_attr($link_color); ?>;
}
.counter-content {
    background: <?php echo esc_attr($link_color); ?>;    
}
.counter-content:before {
    border-color: transparent transparent transparent <?php echo esc_attr($link_color); ?>;    
}
.page-link:hover {
    border-color: <?php echo esc_attr($link_color); ?>;
    background-color: #e2e2e2;
}
.widget_block ul li::before,.widget_block ol li::before{
    color: <?php echo esc_attr($link_color); ?>;
}
.sidebar .wp-block-search .wp-block-search__label::before,
.sidebar .wc-block-product-search .wc-block-product-search__label::before,
.sidebar .widget.widget_block h1::before,
.sidebar .widget.widget_block h2::before,
.sidebar .widget.widget_block h3::before,
.sidebar .widget.widget_block h4::before,
.sidebar .widget.widget_block h5::before,
.sidebar .widget.widget_block h6::before {
    background: <?php echo esc_attr($link_color); ?>;    
}
.footer-sidebar .wp-block-search .wp-block-search__label::after,
.footer-sidebar .widget.widget_block h1::after,
.footer-sidebar .widget.widget_block h2::after,
.footer-sidebar .widget.widget_block h3::after,
.footer-sidebar .widget.widget_block h4::after,
.footer-sidebar .widget.widget_block h5::after,
.footer-sidebar .widget.widget_block h6::after {
    background: <?php echo esc_attr($link_color); ?>;
}
#loadMore:hover
{
  color: <?php echo esc_attr($link_color); ?>;
}
.section-space.error-page img{
    background:<?php echo esc_attr($link_color); ?>;
}
.spice-slider .widget-slide .wp-block-calendar table th {
    background: <?php echo esc_attr($link_color); ?>;
}
.widget-slide .widget.widget_calendar, .widget-slide .widget .widget_calendar caption, .widget-slide .widget.widget_calendar a {
    color: <?php echo esc_attr($link_color); ?>;
}
.wphester-preloader-cube .wphester-cube:before{background: <?php echo esc_attr($link_color); ?>;}
#preloader2 .square {background: <?php echo esc_attr($link_color); ?>;}
.loader-5 span:nth-child(2),.loader-5 span:nth-child(3)
{
    background: <?php echo esc_attr($link_color); ?>;
}
.cart-header>a.cart-total{
    background-color: <?php echo esc_attr($link_color); ?>;
}
.site-info .footer-sidebar a:hover, .site-info .footer-sidebar a:focus{
    color: <?php echo esc_attr($link_color); ?>;
}
.navbar .nav .nav-item:hover .nav-link, .navbar .nav .nav-item.active .nav-link, body .navbar .nav .dropdown-menu > li.active > a, .navbar .nav .nav-item.current_page_parent .nav-link {
    color: <?php echo esc_attr($link_color); ?>;
}
.navbar .nav .nav-item .dropdown-item .navbar-nav .sm-nowrap > li.show > .dropdown-item, .navbar-nav .sm-nowrap > li.current_page_parent > .dropdown-item, .navbar .nav.navbar-nav .sm-nowrap > li.current_page_parent > .dropdown-item, .navbar-nav .current_page_ancestor .nav-link, .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark:hover, .navbar-nav .sm-nowrap > li.show > .dropdown-item.text-dark {
    color: <?php echo esc_attr($link_color); ?> !important;
}
.navbar .nav .nav-item:hover .nav-link, .navbar .nav .nav-item.active .nav-link, body .navbar .nav .dropdown-menu > li.active > a, .navbar .nav .nav-item.current_page_parent .nav-link {
    color: <?php echo esc_attr($link_color); ?>;
}
.navbar-nav:not(.sm-collapsible) .sm-nowrap > li.show > .dropdown-item {
    background-color: transparent!important;
}
.page-breadcrumb span a:hover, nav.rank-math-breadcrumb a:hover {
    color: <?php echo esc_attr($link_color); ?>;
}

        </style>
        <?php
    endif;
}

?>