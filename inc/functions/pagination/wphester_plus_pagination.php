<?php

class wphester_plus_pagination {

    function wphester_plus_page() {
        global $post;
        global $wp_query, $wp_rewrite, $loop, $template;
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        if(basename($template)!='search.php') {                    
            if ($wp_query->max_num_pages == 0) {
               $wp_query = $loop;
            }
        }
        $faPrevious = (!is_rtl()) ? '<i class="fa fa-angle-double-left"></i>' : '<i class="fa fa-angle-double-right"></i>';
        $faNext = (!is_rtl()) ? '<i class="fa fa-angle-double-right"></i>' : '<i class="fa fa-angle-double-left"></i>';


        if (!is_rtl()) {
            the_posts_pagination(array(
                'prev_text' => __($faPrevious, 'wphester-plus'),
                'next_text' => __($faNext, 'wphester-plus'),
           ));
        } else {
            the_posts_pagination(array(
                'prev_text' => __($faPrevious, 'wphester-plus'),
                'next_text' => __($faNext, 'wphester-plus'),
            ));
        }
    }

}

class WPHester_Portfolio_Page {

    function WPHester_Port_Page($curpage, $post_type_data, $total, $posts_per_page) {
        $faPrevious = (!is_rtl()) ? "fa fa-angle-double-left" : "fa fa-angle-double-right";
        $faNext = (!is_rtl()) ? "fa fa-angle-double-right" : "fa fa-angle-double-left";
        $count = $total - $posts_per_page;
        if ($count <= 0) {
            return;
        } else {
            ?>
            <div class="pagination">
                 <?php if ($curpage != 1) {
                        echo '<a class="previous" alt="Previous" href="' . get_pagenum_link(($curpage - 1 > 0 ? $curpage - 1 : 1)) . '" ><i class="'.$faPrevious.'"></i></a>';
                    }
                    ?>
                    <?php
                    for ($i = 1; $i <= $post_type_data->max_num_pages; $i++) {
                        echo '<a class="' . ($i == $curpage ? 'active ' : '') . '" href="' . get_pagenum_link($i) . '" alt="'.$i.'">' . $i . '</a>';
                    }
                    if ($i - 1 != $curpage) {
                        echo '<a class="next" alt="next" href="' . get_pagenum_link(($curpage + 1 <= $post_type_data->max_num_pages ? $curpage + 1 : $post_type_data->max_num_pages)) . '"><i class="'.$faNext.'"></i></a>';
                    }
                    ?>
               
            </div>
        <?php
        }
    }

}