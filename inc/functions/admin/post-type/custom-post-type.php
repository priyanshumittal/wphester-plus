<?php
/************* Home portfolio custom post type ************************/		
function wphester_plus_project_type()
		{	register_post_type( 'wphester_portfolio',
				array(
				'labels' => array(
				'name' => __('Projects', 'wphester-plus'),
				'add_new' => __('Add New', 'wphester-plus'),
                'add_new_item' => __('Add New Project','wphester-plus'),
				'edit_item' => __('Add New','wphester-plus'),
				'new_item' => __('New Link','wphester-plus'),
				'all_items' => __('All Projects','wphester-plus'),
				'view_item' => __('View Link','wphester-plus'),
				'search_items' => __('Search Links','wphester-plus'),
				'not_found' =>  __('No Links found','wphester-plus'),
				'not_found_in_trash' => __('No Links found in Trash','wphester-plus'), 
				
			),
				'supports' => array('title','thumbnail','editor','excerpt'),
				'show_in_nav_menus' => false,
				'public' => true,
				'rewrite' => array('slug' => 'wphester_portfolio'),
				'menu_position' => 11,
				'public' => true,
				'menu_icon' => WPHESTERP_PLUGIN_URL . '/inc/images/portfolio.png',
			)
	);
}
add_action( 'init', 'wphester_plus_project_type' );
?>