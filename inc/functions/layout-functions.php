<?php
if (!function_exists('starter_team_json')) {

    function starter_team_json() {
        return json_encode(array(
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-1.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/portfolio/project-20.jpg',
                    'membername' => 'Danial Wilson',
                    'designation' => esc_html__('Senior Manager', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_26d7ea7f40c56',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-37fb908374e06',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-47fb9144530fc',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9750e1e09',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-67fb0150e1e256',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-2.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Amanda Smith',
                    'designation' => esc_html__('Founder & CEO', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d1ea2f40c66',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9133a7772',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9160rt683',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916zzooc9',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916qqwwc784',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-3.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/portfolio/project-11.jpg',
                    'membername' => 'Victoria Wills',
                    'designation' => esc_html__('Web Master', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c76',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-4.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Travis Marcus',
                    'designation' => esc_html__('UI Developer', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c86',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
            ));
    }

}
if (!function_exists('starter_service_json')) {

    function starter_service_json() {
        return json_encode(array(
            array(
                'icon_value' => 'fa-headphones',
                'title' => esc_html__('Unlimited Support', 'wphester-plus'),
                'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'wphester-plus'),
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b56',
            ),
            array(
                'icon_value' => 'fa-mobile',
                'title' => esc_html__('Pixel Perfect Design', 'wphester-plus'),
                'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'wphester-plus'),
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b66',
            ),
            array(
                'icon_value' => 'fa fa-cogs',
                'title' => esc_html__('Powerful Options', 'wphester-plus'),
                'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'wphester-plus'),
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b86',
            ),
            array(
                'icon_value' => 'fa-desktop',
                'title' => esc_html__('Powerful Options', 'wphester-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_52d7ea8f40b86',
            ),
            array(
                'icon_value' => 'fa-headphones',
                'title' => esc_html__('Powerful Options', 'wphester-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_53d7ea9f40b86',
            ),
            array(
                'icon_value' => 'fa-desktop',
                'title' => esc_html__('Powerful Options', 'wphester-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_59d7ea4f40b86',
            ),
        ));
    }

}
if (!function_exists('starter_funfact_json')) {

    function starter_funfact_json() {
        return json_encode(array(
            array(
                'title' => '105',
                'text' => esc_html__('Team Members', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b56',
            ),
            array(
                'title' => '215',
                'text' => esc_html__('Client’s Feedback', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b66',
            ),
            array(
                'title' => '15',
                'text' => esc_html__('Winning Awards', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b86',
            ),
            array(
                'title' => '310',
                'text' => esc_html__('Completed Works', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b87',
            ),
        ));
    }

}

if (!function_exists('starter_client_json')) {

    function starter_client_json() {
        return json_encode(array(
            array(
                'link' => '#',
                'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-1.png',
                'open_new_tab' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b96',
            ),
            array(
                'link' => '#',
                'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-2.png',
                'open_new_tab' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b97',
            ),
            array(
                'link' => '#',
                'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-3.png',
                'open_new_tab' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b98',
            ),
            array(
                'link' => '#',
                'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-4.png',
                'open_new_tab' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b99',
            ),
            array(
                'link' => '#',
                'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-5.png',
                'open_new_tab' => 'no',
                'id' => 'customizer_repeater_56d7ea7f40b100',
            ),
        ));
    }

}

if (!function_exists('wphester_plus_dummy_portfolio_fn')) {

    function wphester_plus_dummy_portfolio_fn($portfolio_col) {
        ?>

     <div id="content" class="tab-content" role="tablist">

            <div id="all" class="tab-pane fade show in active" role="tabpanel" aria-labelledby="tab-A">
                <div class="row">

                    <?php
                    for ($i = 1; $i <= 6; $i++) {
                        ?>
                        <div class="col-lg-<?php echo $portfolio_col; ?> col-md-6 col-sm-12">  
                            <div class="card"> 
                                <figure class="portfolio-thumbnail">
                                    <img src="<?php echo WPHESTERP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" class="card-img-top" alt="Business Consultant">
                                    <a data-toggle="modal" data-target="#basicExampleModal<?php echo $i; ?>"><i>+</i></a>
                                </figure>
                                <figcaption>
                                    <div class="entry-header"><h4 class="entry-title"><a href="#">Finance Planing</a></h4></div>
                                    <p>business, finance </p>
                                </figcaption>                                
                            </div>
                        </div>
                        <?php
                    }
                    ?>


                </div>
            </div>


            <div id="bussiness" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-B">
                <div class="row">
                    <?php
                    for ($i = 7; $i <= 12; $i++) {
                        ?>
                        <div class="col-lg-<?php echo $portfolio_col; ?> col-md-6 col-sm-12">  
                            <div class="card"> 
                                <figure class="portfolio-thumbnail">
                                    <img src="<?php echo WPHESTERP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" class="card-img-top" alt="Business Consultant">
                                    <a data-toggle="modal" data-target="#basicExampleModal<?php echo $i; ?>"><i>+</i></a>
                                </figure>
                                <figcaption>
                                    <div class="entry-header"><h4 class="entry-title"><a href="#">Finance Planing</a></h4></div>
                                    <p>business, finance </p>
                                </figcaption>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>


            <div id="branding" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
                <div class="row">
                    <?php
                    for ($i = 13; $i <= 18; $i++) {
                        ?>
                        <div class="col-lg-<?php echo $portfolio_col; ?> col-md-6 col-sm-12">   
                            <div class="card">
                                <figure class="portfolio-thumbnail">
                                <img src="<?php echo WPHESTERP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" class="card-img-top" alt="Business Consultant">
                                <a data-toggle="modal" data-target="#basicExampleModal<?php echo $i; ?>"><i>+</i></a>
                            </figure>
                            <figcaption>
                                <div class="entry-header"><h4 class="entry-title"><a href="#">Finance Planing</a></h4></div>
                                <p>business, finance </p>
                            </figcaption>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php
        for ($i = 1; $i <= 18; $i++) {
            ?>
            <!-- Modal -->
            <div class="modal fade" id="basicExampleModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body p-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            <!-- Grid row -->
                            <div class="row">

                                <!-- Grid column -->
                                <div class="col-md-6 py-5 pl-5">

                                    <article class="post text-center">
                                        <div class="entry-header">
                                            <h2 class="entry-title"><a href="#" alt="Multi-purpose">Finance Planing</a></h2>
                                        </div>
                                        <div class="entry-content">
                                            <p>sellus facilisis, nunc in lacinia auctor, eeros lacus aliquet velit, quis lobortis risus nunc nec nisi maecans et turpis vitae velit.volutpat.</p>
                                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor</p>
                                        </div>
                                    </article>

                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">

                                    <div class="view rounded-right">
                                        <img class="img-fluid" src="<?php echo WPHESTERP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" alt="Sample image">
                                    </div>

                                </div>
                                <!-- Grid column -->

                            </div>
                            <!-- Grid row -->

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }

}

add_action('wphester_plus_dummy_portfolio_layout', 'wphester_plus_dummy_portfolio_fn');



if (!function_exists('wphester_plus_starter_contact_social_json')) {

    function wphester_plus_starter_contact_social_json() {
        return json_encode(array(
        array(
                        'icon_value' => 'fa-brands fa-facebook-f',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b76',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-x-twitter',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b77',
                        ),
                        array(
                        'icon_value' =>'fa-brands fa-linkedin',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b78',
                        ),
                        array(
                        'icon_value' => 'fa fa-instagram',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-youtube',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
    ));
    }
}
if (!function_exists('wphester_plus_ribon_content_social_json')) {

    function wphester_plus_ribon_content_social_json() {
        return json_encode(array(
              array(
                'icon_value' => 'fa-brands fa-facebook-f',
                'title'      => esc_html__( 'Facebook', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b76',
                ),
                array(
                'icon_value' => 'fa-brands fa-x-twitter',
                'title'      => esc_html__( 'Twitter', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b77',
                ),
                array(
                'icon_value' => 'fa-brands fa-linkedin',
                'title'      => esc_html__( 'LinkedIn', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b78',
                ),
                array(
                'icon_value' => 'fa-brands fa-instagram',
                'title'      => esc_html__( 'Instagram', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),
                array(
                'icon_value' => 'fa-brands fa-whatsapp',
                'title'      => esc_html__( 'Whatsapp', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b79',
                ),                
                array(
                'icon_value' => 'fa-brands fa-pinterest-p',
                'title'      => esc_html__( 'Pinterest', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),
                array(
                'icon_value' => 'fa-brands fa-youtube',
                'title'      => esc_html__( 'Youtube', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),                
        ) );
    }
}   ?>