<?php

//About Section
$wp_customize->add_section('home_about_page_section', array(
    'title' => esc_html__('About section settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 16,
));

// Enable about section
$wp_customize->add_setting('about_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_plus_sanitize_checkbox',
    ));

$wp_customize->add_control(new wphester_Toggle_Control($wp_customize, 'about_section_enable',
                array(
            'label' => esc_html__('Enable/Disable About Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'home_about_page_section',
                )
));

//About section content
if ( class_exists( 'WPHester_Plus_Page_Editor' ) ) {
$about_image = WPHESTERP_PLUGIN_URL.'/inc/images/about.jpg';
$default = '<div class="row">
                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="about-block">
                            <div class="section-header">
                                <h2 class="section-title">'. esc_html__('We Are A Successful & Sustainable Consulting','wphester-plus').'</h2>
                                <p class="section-subtitle">'. esc_html__('Our Introduction','wphester-plus').'</p>
                            </div>
                            <div class="entry-content">
                                <p>'. esc_html__('We are ready to provide you with any financial, legal and auditing help as well as prepare a business plan','wphester-plus').'</p>
                                <ul class="list-style-four">
                                    <li>'. esc_html__('Business, Strategic and Succession Planning','wphester-plus').'</li>
                                    <li>'. esc_html__('Human Resources Consulting','wphester-plus').'</li>
                                    <li>'. esc_html__('Business Expansion & Acquisitions','wphester-plus').'</li>
                                    <li>'. esc_html__('Marketing & Sales Including E-Commerce','wphester-plus').'</li>
                                </ul>
                                <a href="#" class="btn-small btn-color-2 mt-4" alt="Check-it-out">'. esc_html__('ABOUT US','wphester-plus').'</a>
                            </div>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Featured image-->
                    <div class="col-md-6">
                        <div class="image-box">
                            <figure class="thumbnail">
                                <div id="counter" class="counter-content">
                                    <p class="iq-counter-info">
                                        <span class="timer" data-to="23" data-speed="5000">'. esc_html__('23','wphester-plus').'</span>
                                    </p>
                                    <p class="counter-content-text">'. esc_html__('Years Experience in Consulting','wphester-plus').'</p>
                                </div>
                                <img src="'.esc_url($about_image).'" class="img-fluid" alt="img1">
                            </figure>
                            <div class="about-shadow"></div>
                        </div>
                    </div>';


$wp_customize->add_setting(
    'about_section_content', array(
        'default'           => $default,
        'sanitize_callback' => 'wp_kses_post',
        'transport'         => $selective_refresh,
    )
);

$wp_customize->add_control(
    new WPHester_Plus_Page_Editor(
        $wp_customize, 'about_section_content', array(
            'label'    => esc_html__( 'About Content', 'innofit-plus' ),
            'section'  => 'home_about_page_section',
            'priority' => 10,
            'needsync' => true,
            'active_callback' => 'wphester_plus_abt_callback'
        )
    )
);
}

/**
 * Add selective refresh for Front page section section controls.
 */

$wp_customize->selective_refresh->add_partial( 'about_section_content', array(
        'selector'            => '.about-section .section-subtitle',
        'settings'            => 'about_section_content',
        
) );
?>