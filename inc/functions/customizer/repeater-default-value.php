<?php

// wphester slider content data
if (!function_exists('wphester_plus_slider_default_customize_register')) :
    add_action('customize_register', 'wphester_plus_slider_default_customize_register');

    function wphester_plus_slider_default_customize_register($wp_customize) {

        //wphester lite slider data
        if (get_theme_mod('home_slider_subtitle') != '' || get_theme_mod('home_slider_title') != '' || get_theme_mod('home_slider_discription') != '' || get_theme_mod('home_slider_image') != '') {

            $home_slider_subtitle = get_theme_mod('home_slider_subtitle');
            $home_slider_title = get_theme_mod('home_slider_title');
            $home_slider_discription = get_theme_mod('home_slider_discription');
            $home_slider_btn_target = get_theme_mod('home_slider_btn_target');
            $home_slider_btn_txt = get_theme_mod('home_slider_btn_txt');
            $home_slider_btn_link = get_theme_mod('home_slider_btn_link');
            $home_slider_image = get_theme_mod('home_slider_image');
            $home_slider_align_split = get_theme_mod('slider_content_alignment');
            $home_slider_caption='customizer_repeater_slide_caption_'.$home_slider_align_split;
            
            $wphester_plus_slider_content_control = $wp_customize->get_setting('wphester_plus_slider_content');
            if (!empty($wphester_plus_slider_content_control)) {
                $wphester_plus_slider_content_control->default = json_encode(array(
                    array(
                        'subtitle' => !empty($home_slider_subtitle) ? $home_slider_subtitle : 'WELCOME TO THE HESTER THEME',
                        'title' => !empty($home_slider_title) ? $home_slider_title : 'We Provide Quality Business <br> Solution',
                        'text' => !empty($home_slider_discription) ? $home_slider_discription : 'Welcome to WPHester',
                        'abtsliderbutton_text' => !empty($home_slider_btn_txt) ? $home_slider_btn_txt : 'Learn More',
                        'abtsliderlink' => !empty($home_slider_btn_link) ? $home_slider_btn_link : '#',
                        'image_url' => !empty($home_slider_image) ? $home_slider_image : WPHESTERP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                        'abtslider_open_new_tab' => !empty($home_slider_btn_target) ? $home_slider_btn_target : false,
                        'abtbutton_text' => !empty($home_slider_btn_txt2)? $home_slider_btn_txt2 :__('About Us', 'wphester-plus'),
                        'abtlink' => !empty($home_slider_btn_link2)? $home_slider_btn_link2 :'#',
                        'abtopen_new_tab' => !empty($home_slider_btn_target2)? $home_slider_btn_target2 : false,
                        'home_slider_caption' => !empty($home_slider_align_split)? $home_slider_caption : 'customizer_repeater_slide_caption_center',
                        'id'         => 'customizer_repeater_56d7ea7f40b50',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b50',
                    ),
                ));
            }
        } else {
            //wphester slider data
            $wphester_plus_slider_content_control = $wp_customize->get_setting('wphester_plus_slider_content');
            if (!empty($wphester_plus_slider_content_control)) {
                $wphester_plus_slider_content_control->default = json_encode(array(
                    array(
                        'subtitle' => __('WELCOME TO THE HESTER THEME', 'wphester-plus'),                
                        'title' => __('We provide solutions to <br> <span>grow your business</span>', 'wphester-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'wphester-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'wphester-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/slider/slide-1.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'wphester-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_left',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b96',
                    ),
                    array(
                        'subtitle' => __('WELCOME TO THE HESTER THEME', 'wphester-plus'),
                        'title' => __('We create stunning <br><span>WordPress themes</span>', 'wphester-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'wphester-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'wphester-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/slider/slide-2.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'wphester-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_center',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b97',
                    ),
                    array(
                        'subtitle' => __('WELCOME TO THE HESTER THEME', 'wphester-plus'),                
                        'title' => __('We provide solutions to <br> <span>grow your business</span>', 'wphester-plus'),
                        'text' => __('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br> tempor incididunt ut labore et dolore magna aliqua.', 'wphester-plus'),
                        'abtsliderbutton_text' => __('Learn More', 'wphester-plus'),
                        'abtsliderlink' => '#',
                        'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/slider/slide-3.jpg',
                        'abtslider_open_new_tab' => 'yes',
                        'abtbutton_text' => __('About Us', 'wphester-plus'),
                        'abtlink' => '#',
                        'abtopen_new_tab' => 'yes',
                        'home_slider_caption' => 'customizer_repeater_slide_caption_right',
                        'sidebar_check' => 'no',
                        'id' => 'customizer_repeater_56d7ea7f40b98',
                    ),
                ));
            }
        }
    }

endif;

// wphester default service data
if (!function_exists('wphester_service_default_customize_register')) :
    function wphester_service_default_customize_register($wp_customize) {
        $wphester_service_content_control = $wp_customize->get_setting('wphester_service_content');
        if (!empty($wphester_service_content_control)) {
            $wphester_service_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Unlimited Support', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'icon_value' => 'fa-solid fa-mobile-screen',
                    'title' => esc_html__('Pixel Perfect Design', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'icon_value' => 'fa-solid fa-gears',
                    'title' => esc_html__('Powerful Options', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                array(
                    'icon_value' => 'fa-desktop',
                    'title' => esc_html__('Powerful Options', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_52d7ea8f40b86',
                ),
                array(
                    'icon_value' => 'fa-headphones',
                    'title' => esc_html__('Powerful Options', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_53d7ea9f40b86',
                ),
                array(
                    'icon_value' => 'fa-desktop',
                    'title' => esc_html__('Powerful Options', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                    'choice' => 'customizer_repeater_icon',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_59d7ea4f40b86',
                ),
            ));
        }
    }
    add_action('customize_register', 'wphester_service_default_customize_register');
endif;

// wphester default Contact deatail
if (!function_exists('wphester_plus_contact_detail_default_customize_register')) :
    function wphester_plus_contact_detail_default_customize_register($wp_customize) {
        $wphester_plus_contact_detail_content_control = $wp_customize->get_setting('wphester_plus_contact_detail_content');
        if (!empty($wphester_plus_contact_detail_content_control)) {
            $wphester_plus_contact_detail_content_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-solid fa-location-dot',
                    'title' => 'Address',
                    'text' => __('17504 Carlton Cuevas Rd, Gulfport, MS, 39503', 'wphester-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b60',
                ),
                array(
                    'icon_value' => 'fa-solid fa-mobile-screen',
                    'title' => 'Phone',
                    'text' => __('(007) 123 456 7890<br>(007) 444 333 6678', 'wphester-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b61',
                ),
                array(
                    'icon_value' => 'fa-solid fa-envelope',
                    'title' => 'Email',
                    'text' => __('info@honeypress.com <br>support@honeypress.com', 'wphester-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b62',
                ),        
            ));
        }
    }
    add_action('customize_register', 'wphester_plus_contact_detail_default_customize_register');
endif;

// busicare default Contact data
if (!function_exists('wphester_plus_social_links_default_customize_register')) :

    function wphester_plus_social_links_default_customize_register($wp_customize) {

        $wphester_social_links_control = $wp_customize->get_setting('wphester_social_links');
        if (!empty($wphester_social_links_control)) {
            $wphester_social_links_control->default = json_encode(array(
                        array(
                        'icon_value' => 'fa-brands fa-facebook-f',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b76',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-x-twitter',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b77',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-linkedin',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b78',
                        ),
                        array(
                        'icon_value' => 'fa-brands fa-instagram',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
                        array(
                        'icon_value' => 'fa fa-youtube-play',
                        'link'       => '#',
                        'open_new_tab' => 'yes',
                        'id'         => 'customizer_repeater_56d7ea7f40b80',
                        ),
        ));
        }
    }

    add_action('customize_register', 'wphester_plus_social_links_default_customize_register');

endif;

// wphester default Funfact data
if (!function_exists('wphester_funfact_default_customize_register')) :

    function wphester_funfact_default_customize_register($wp_customize) {

        $wphester_funfact_content_control = $wp_customize->get_setting('wphester_funfact_content');
        if (!empty($wphester_funfact_content_control)) {
            $wphester_funfact_content_control->default = json_encode(array(
                array(
                    'title' => '105',
                    'text' => esc_html__('Team Members', 'wphester-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b56',
                ),
                array(
                    'title' => '215',
                    'text' => esc_html__('Client’s Feedback', 'wphester-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b66',
                ),
                array(
                    'title' => '15',
                    'text' => esc_html__('Winning Awards', 'wphester-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b86',
                ),
                array(
                    'title' => '310',
                    'text' => esc_html__('Completed Works', 'wphester-plus'),
                    'id' => 'customizer_repeater_56d7ea7f40b87',
                ),
            ));
        }
    }

    add_action('customize_register', 'wphester_funfact_default_customize_register');

endif;

// wphester default Contact data
if (!function_exists('wphester_contact_default_customize_register')) :

    function wphester_contact_default_customize_register($wp_customize) {

        $wphester_plus_contact_content_control = $wp_customize->get_setting('wphester_plus_contact_content');
        if (!empty($wphester_plus_contact_content_control)) {
            $wphester_plus_contact_content_control->default = json_encode(array(
                array(
                'icon_value' => 'fa-solid fa-location-dot',
                'title' => 'Address',
                'text' => __('17504 Carlton Cuevas Rd, Gulfport, MS, 39503', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b60',
            ),
            array(
                'icon_value' => 'fa-solid fa-mobile-screen',
                'title' => 'Phone',
                'text' => __('(007) 123 456 7890<br>(007) 444 333 6678', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b61',
            ),
            array(
                'icon_value' => 'fa-solid fa-envelope',
                'title' => 'Email',
                'text' => __('info@honeypress.com <br>support@honeypress.com', 'wphester-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b62',
            ),   
            ));
        }
    }

    add_action('customize_register', 'wphester_contact_default_customize_register');

endif;

// wphester default Contact data
if (!function_exists('wphester_social_links_default_customize_register')) :

    function wphester_social_links_default_customize_register($wp_customize) {

        $wphester_social_links_control = $wp_customize->get_setting('wphester_social_links');
        if (!empty($wphester_social_links_control)) {
            $wphester_social_links_control->default = json_encode(array(
                array(
                    'icon_value' => 'fa-brands fa-facebook-f',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b76',
                ),
                array(
                    'icon_value' => 'fa-brands fa-x-twitter',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b77',
                ),
                array(
                    'icon_value' => 'fa-brands fa-linkedin',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b78',
                ),
                array(
                    'icon_value' => 'fa fa-youtube-play',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b79',
                ),
                array(
                    'icon_value' => 'fa-brands fa-instagram',
                    'link' => '#',
                    'open_new_tab' => 'yes',
                    'id' => 'customizer_repeater_56d7ea7f40b80',
                ),
            ));
        }
    }

    add_action('customize_register', 'wphester_social_links_default_customize_register');

endif;


// wphester default Ribon data
if (!function_exists('wphester_ribon_default_customize_register')) :

    function wphester_ribon_default_customize_register($wp_customize) {

        $wphester_ribon_content_control = $wp_customize->get_setting('wphester_ribon_content');
        if (!empty($wphester_ribon_content_control)) {
            $wphester_ribon_content_control->default = json_encode(array(
                array(
                'icon_value' => 'fa-brands fa-facebook-f',
                'title'      => esc_html__( 'Facebook', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b76',
                ),
                array(
                'icon_value' => 'fa-brands fa-x-twitter',
                'title'      => esc_html__( 'Twitter', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b77',
                ),
                array(
                'icon_value' => 'fa-brands fa-linkedin',
                'title'      => esc_html__( 'LinkedIn', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b78',
                ),
                array(
                'icon_value' => 'fa-brands fa-instagram',
                'title'      => esc_html__( 'Instagram', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),
                array(
                'icon_value' => 'fa-brands fa-whatsapp',
                'title'      => esc_html__( 'Whatsapp', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b79',
                ),                
                array(
                'icon_value' => 'fa-brands fa-pinterest-p',
                'title'      => esc_html__( 'Pinterest', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),
                array(
                'icon_value' => 'fa-brands fa-youtube',
                'title'      => esc_html__( 'Youtube', 'wphester-plus' ),
                'link'       => '#',
                'open_new_tab' => 'yes',
                'id'         => 'customizer_repeater_56d7ea7f40b80',
                ),
            ));
        }
    }

    add_action('customize_register', 'wphester_ribon_default_customize_register');

endif;


// WPHester Testimonial content data
if (!function_exists('wphester_testimonial_default_customize_register')) :
    add_action('customize_register', 'wphester_testimonial_default_customize_register');

    function wphester_testimonial_default_customize_register($wp_customize) {

        //wphester default testimonial data.
        $wphester_testimonial_content_control = $wp_customize->get_setting('wphester_testimonial_content');
        if (!empty($wphester_testimonial_content_control)) {
            $wphester_testimonial_content_control->default = json_encode(array(
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet, temp consectetur adipisicing elit.',
                    'clientname' => __('Amanda Smith', 'wphester-plus'),
                    'designation' => __('Developer', 'wphester-plus'),
                    'home_testimonial_star' => '4.5',
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_77d7ea7f40b96',
                    'home_slider_caption' => 'customizer_repeater_star_4.5',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet, temp consectetur adipisicing elit.',
                    'clientname' => __('Travis Cullan', 'wphester-plus'),
                    'designation' => __('Team Leader', 'wphester-plus'),
                    'home_testimonial_star' => '5',
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_88d7ea7f40b97',
                    'home_slider_caption' => 'customizer_repeater_star_5',
                ),
                array(
                    'title' => 'Exellent Theme & Very Fast Support',
                    'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet, temp consectetur adipisicing elit.',
                    'clientname' => __('Victoria Wills', 'wphester-plus'),
                    'designation' => __('Volunteer', 'wphester-plus'),
                    'home_testimonial_star' => '3.5',
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                    'id' => 'customizer_repeater_11d7ea7f40b98',
                    'open_new_tab' => 'no',
                    'home_slider_caption' => 'customizer_repeater_star_3.5',
                ),
            ));
        }
    }

endif;


// WPHester Team content data
if (!function_exists('wphester_team_default_customize_register')) :
    add_action('customize_register', 'wphester_team_default_customize_register');

    function wphester_team_default_customize_register($wp_customize) {
        //wphester default team data.
        $wphester_team_content_control = $wp_customize->get_setting('wphester_team_content');
        if (!empty($wphester_team_content_control)) {
            $wphester_team_content_control->default = json_encode(array(
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-1.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/portfolio/project-20.jpg',
                    'membername' => 'Danial Wilson',
                    'designation' => esc_html__('Senior Manager', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_26d7ea7f40c56',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-37fb908374e06',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-47fb9144530fc',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9750e1e09',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-67fb0150e1e256',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-2.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Amanda Smith',
                    'designation' => esc_html__('Founder & CEO', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d1ea2f40c66',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9133a7772',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9160rt683',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916zzooc9',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916qqwwc784',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-3.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/portfolio/project-11.jpg',
                    'membername' => 'Victoria Wills',
                    'designation' => esc_html__('Web Master', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c76',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/team/team-4.jpg',
                    'image_url2' => WPHESTERP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Travis Marcus',
                    'designation' => esc_html__('UI Developer', 'wphester-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c86',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
            ));
        }
    }

endif;

//Client section
if (!function_exists('wphester_client_default_customize_register')) :
    add_action('customize_register', 'wphester_client_default_customize_register');

    function wphester_client_default_customize_register($wp_customize) {
        //wphester default client data.
        $wphester_client_content_control = $wp_customize->get_setting('wphester_clients_content');
        if (!empty($wphester_client_content_control)) {
            $wphester_client_content_control->default = json_encode(array(
                array(
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-1.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b96',
                ),
                array(
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-2.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b97',
                ),
                array(
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-3.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b98',
                ),
                array(
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-4.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b99',
                ),
                array(
                    'link' => '#',
                    'image_url' => WPHESTERP_PLUGIN_URL . '/inc/images/sponsors/client-5.png',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40b100',
                ),
            ));
        }
    }

endif;