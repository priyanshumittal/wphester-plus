<?php

//Callout Section
$wp_customize->add_section('home_cta1_page_section', array(
    'title' => esc_html__('Callout section settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 20,
));

// Enable call to action section
$wp_customize->add_setting('cta1_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_plus_sanitize_checkbox',
    ));

$wp_customize->add_control(new wphester_Toggle_Control($wp_customize, 'cta1_section_enable',
                array(
            'label' => esc_html__('Enable/Disable Callout Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'home_cta1_page_section',
                )
));

$wp_customize->add_setting(
        'home_cta1_title',
        array(
            'default' => esc_html__('GET YOUR FREE CONSULTANT NOW', 'wphester-plus'),
            'transport' => $selective_refresh,
        )
);
$wp_customize->add_control('home_cta1_title', array(
    'label' => esc_html__('Tagline', 'wphester-plus'),
    'section' => 'home_cta1_page_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_cta1_callback',
    ));

$wp_customize->add_setting(
        'home_cta1_btn_text',
        array(
            'default' => esc_html__('GET FREE QUOTE', 'wphester-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_cta1_btn_text',
        array(
            'label' => esc_html__('Button Text', 'wphester-plus'),
            'section' => 'home_cta1_page_section',
            'type' => 'text',
            'active_callback' => 'wphester_plus_cta1_callback'
));

$wp_customize->add_setting(
        'home_cta1_btn_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_cta1_btn_link',
        array(
            'label' => esc_html__('Button Link', 'wphester-plus'),
            'section' => 'home_cta1_page_section',
            'type' => 'text',
            'active_callback' => 'wphester_plus_cta1_callback'
));

$wp_customize->add_setting(
        'home_cta1_btn_link_target',
        array('sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
));

$wp_customize->add_control(
        'home_cta1_btn_link_target',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Open link in a new tab', 'wphester-plus'),
            'section' => 'home_cta1_page_section',
            'active_callback' => 'wphester_plus_cta1_callback'
        )
);


/**
 * Add selective refresh for Front page pricing section controls.
 */
$wp_customize->selective_refresh->add_partial('home_cta1_title', array(
    'selector' => '.callout .section-title',
    'settings' => 'home_cta1_title',
    'render_callback' => 'wphester_plus_home_cta1_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_cta1_btn_text', array(
    'selector' => '.callout a.cta_btn',
    'settings' => 'home_cta1_btn_text',
    'render_callback' => 'wphester_plus_home_cta1_btn_text_render_callback',
));
function wphester_plus_home_cta1_title_render_callback() {
    return get_theme_mod('home_cta1_title');
}
function wphester_plus_home_cta1_btn_text_render_callback() {
    return get_theme_mod('home_cta1_btn_text');
}
?>