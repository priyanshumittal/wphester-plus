<?php

/**
 * Single Blog Options Customizer
 *
 * @package wphester
 */
function wphester_plus_single_blog_customizer($wp_customize) {

    /************************* Blog Button Title*********************************/
$wp_customize->add_setting( 'wphester_blog_button_title',
    array(
        'default'           => esc_html__('Read More','wphester-plus'),
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wphester_sanitize_text',    
        )
    );
$wp_customize->add_control( 'wphester_blog_button_title',
    array(
        'label'    => esc_html__( 'Read More Button Text', 'wphester-plus' ),
        'section'  => 'wphester_blog_section',
        'type'     => 'text',   
        'priority' =>4,
        )
    );

/************************* Enable Author*********************************/
$wp_customize->add_setting( 'wphester_enable_blog_author',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wphester_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize,  'wphester_enable_blog_author',
    array(
        'label'    => esc_html__( 'Hide/Show Author', 'wphester-plus' ),
        'section'  => 'wphester_blog_section',
        'type'     => 'toggle', 
        'priority' => 5,
        )
    ));

/************************* Enable Date*********************************/
$wp_customize->add_setting( 'wphester_enable_blog_date',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wphester_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize,  'wphester_enable_blog_date',
    array(
        'label'    => esc_html__( 'Hide/Show Date', 'wphester-plus' ),
        'section'  => 'wphester_blog_section',
        'type'     => 'toggle', 
        'priority' => 6,
        )
    ));

/************************* Enable Category*********************************/
$wp_customize->add_setting( 'wphester_enable_blog_category',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wphester_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize,   'wphester_enable_blog_category',
    array(
        'label'    => esc_html__( 'Hide/Show Category', 'wphester-plus' ),
        'section'  => 'wphester_blog_section',
        'type'     => 'toggle', 
        'priority' => 7,
        )
    ));

/************************* Enable blog*********************************/
$wp_customize->add_setting('wphester_enable_blog_comnt',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'wphester_enable_blog_comnt',
            array(
                'label' => esc_html__('Hide/Show Comments', 'wphester-plus'),
                'type' => 'toggle',
                'section' => 'wphester_blog_section',
                'priority' => 8,
            )
    ));
/************************* Enable Continue Reading Button*********************************/
$wp_customize->add_setting( 'wphester_enable_blog_read_button',
    array(
        'default'           => true,
        'capability'        => 'edit_theme_options',
        'sanitize_callback' => 'wphester_sanitize_checkbox',    
        )
    );
$wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize, 'wphester_enable_blog_read_button',
    array(
        'label'    => esc_html__( 'Hide/Show Read More Button', 'wphester-plus' ),
        'section'  => 'wphester_blog_section',
        'type'     => 'toggle', 
        'priority' => 9,
        )
    ));

    /*     * *********************** Related Post  ******************************** */

    $wp_customize->add_setting('wphester_enable_related_post',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_plus_sanitize_checkbox',
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'wphester_enable_related_post',
                    array(
                'label' => esc_html__('Enable/Disable Related Posts', 'wphester-plus'),
                'type' => 'toggle',
                'section' => 'wphester_single_blog_section',
                'priority' => 1,
                    )
    ));

    /*     * *********************** Related Post Title  ******************************** */

    $wp_customize->add_setting('wphester_related_post_title',
            array(
                'default' => esc_html__('Related Posts', 'wphester-plus'),
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control('wphester_related_post_title',
            array(
                'label' => esc_html__('Related Posts Title', 'wphester-plus'),
                'type' => 'text',
                'section' => 'wphester_single_blog_section',
                'priority' => 2,
                'active_callback' => 'wphester_plus_rt_post_callback',
            )
    );

    /*     * *********************** Related Post Option ******************************** */

    $wp_customize->add_setting('wphester_related_post_option',
            array(
                'default' => esc_html__('categories', 'wphester-plus'),
                'sanitize_callback' => 'wphester_sanitize_select'
            )
    );

    $wp_customize->add_control('wphester_related_post_option',
            array(
                'label' => esc_html__('Related Posts Option', 'wphester-plus'),
                'section' => 'wphester_single_blog_section',
                'settings' => 'wphester_related_post_option',
                'active_callback' => 'wphester_plus_rt_post_callback',
                'priority' => 3,
                'type' => 'select',
                'choices' => array(
                    'categories' => esc_html__('All', 'wphester-plus'),
                    'tags' => esc_html__('Related by tags', 'wphester-plus'),
                )
            )
    );
}

add_action('customize_register', 'wphester_plus_single_blog_customizer');