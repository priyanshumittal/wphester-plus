<?php
/**
 * Customize for taxonomy with dropdown, extend the WP customizer
 */
if (!class_exists('WP_Customize_Control'))
    return NULL;

function wphester_plus_sections_settings($wp_customize) {

    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';
    /* Sections Settings */
    $wp_customize->add_panel('section_settings', array(
        'priority' => 126,
        'capability' => 'edit_theme_options',
        'title' => esc_html__('Homepage Section Settings', 'wphester-plus'),
    ));
}

add_action('customize_register', 'wphester_plus_sections_settings');

function wphester_plus_home_page_sanitize_text($input) {
    return wp_kses_post(force_balance_tags($input));
}

function wphester_plus_sanitize_radio($input, $setting) {

    //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
    $input = sanitize_key($input);

    //get the list of possible radio box options 
    $choices = $setting->manager->get_control($setting->id)->choices;

    //return input if valid or return default option
    return ( array_key_exists($input, $choices) ? $input : $setting->default );
}
/* * *********************** Search Callback function ******************************** */

function search_icon_hide_show_callback($control) {
    if (true == $control->manager->get_setting('search_btn_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Topbar Callback function ******************************** */

function wphester_plus_topbar_callback($control) {
    if (true == $control->manager->get_setting('topbar_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Slider Callback function ******************************** */

function wphester_plus_slider_callback($control) {
    if (true == $control->manager->get_setting('home_page_slider_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Service Callback function ******************************** */

function wphester_plus_service_callback($control) {
    if (true == $control->manager->get_setting('home_service_section_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Portfolio Callback function ******************************** */

function wphester_plus_portfolio_callback($control) {
    if (true == $control->manager->get_setting('portfolio_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Testimonial Callback function ******************************** */

function wphester_plus_testimonial_callback($control) {
    if (true == $control->manager->get_setting('testimonial_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

function wphester_plus_testimonial_design_layout_callback($control) {
    if ( $control->manager->get_setting('home_testimonial_design_layout')->value() == '1') {
        return false;
    } else {
        return true;
    }
}
/* * *********************** Funfact Callback function ******************************** */

function wphester_plus_funfact_callback($control) {
    if (true == $control->manager->get_setting('funfact_section_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Latest News Callback function ******************************** */

function wphester_plus_news_callback($control) {
    if (true == $control->manager->get_setting('latest_news_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}


/* * *********************** CTA1 Callback function ******************************** */

function wphester_plus_cta1_callback($control) {
    if (true == $control->manager->get_setting('cta1_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}


/* * *********************** CTA2 Callback function ******************************** */

function wphester_plus_cta_callback($control) {
    if (true == $control->manager->get_setting('cta2_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Team Callback function ******************************** */

function wphester_plus_team_callback($control) {
    if (true == $control->manager->get_setting('team_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** WooProduct Callback function ******************************** */

function wphester_plus_wooproduct_callback($control) {
    if (true == $control->manager->get_setting('shop_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Sponsors Callback function ******************************** */

function wphester_plus_sponsors_callback($control) {
    if (true == $control->manager->get_setting('client_section_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Contact Callback function ******************************** */

function wphester_plus_contact_callback($control) {
    if (true == $control->manager->get_setting('home_contact_section_enabled')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Feature Callback function ******************************** */

function wphester_plus_featured_callback($control) {
    if (true == $control->manager->get_setting('wphester_enable_feature_section')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Footer Callback function ******************************** */

function wphester_plus_footer_callback($control) {
    if (true == $control->manager->get_setting('ftr_bar_enable')->value()) {
        return true;
    } else {
        return false;
    }
}


/* * *********************** Footer WIdgets enable or disbale function ******************************** */

function wphester_plus_ftr_widgets_hide_show_callback($control) {
    if (true == $control->manager->get_setting('ftr_widgets_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

function wphester_plus_footer_column_callback($control) {
    if ($control->manager->get_setting('advance_footer_bar_section')->value() == '1') {
        return false;
    }
    return true;
}
/************************* Scroll to top *********************************/

    function scroll_top_callback ( $control ) 
    {
        if( true == $control->manager->get_setting ('scrolltotop_setting_enable')->value()){
            return true;
        }
        else {
            return false;
        }       
    }

/* * *********************** Related Post Callback function ******************************** */

function wphester_plus_rt_post_callback($control) {
    if (true == $control->manager->get_setting('wphester_enable_related_post')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Different Stick Header  ******************************** */

function wphester_plus_sticky_header_callback($control) {
    if (true == $control->manager->get_setting('sticky_header_enable')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** Different Stick Header Logo ******************************** */

function wphester_plus_sticky_header_logo_callback($control) {
    if (true == $control->manager->get_setting('header_logo_placing')->value()) {
        return true;
    } else {
        return false;
    }
}
/* * *********************** Menu  Header Info  ******************************** */

function wphester_plus_menu_header_info_callback($control) {
    if ($control->manager->get_setting('header_logo_placing')->value() == 'seven' || $control->manager->get_setting('header_logo_placing')->value() == 'eight') {
        return true;
    } else {
        return false;
    }
}
/* * *********************** Breadcrumbm ******************************** */

function wphester_plus_breadcrumb_callback($control) {
    if (true == $control->manager->get_setting('banner_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

/* * *********************** Breadcrumbm overlay******************************** */

function wphester_plus_breadcrumb_overlay_callback($control) {
    if (true == $control->manager->get_setting('breadcrumb_image_overlay')->value()) {
        return true;
    } else {
        return false;
    }
}
/************************* About Template Team Callback function *********************************/

    function wphester_plus_abt_team_callback ( $control ) 
    {
        if( true == $control->manager->get_setting ('about_team_enable')->value()){
            return true;
        }
        else {
            return false;
        }       
    }

/************************* Ribbon Setting *********************************/

    function ribbon_callback ( $control ) 
    {
        if( true == $control->manager->get_setting ('ribon_setting_enable')->value()){
            return true;
        }
        else {
            return false;
        }       
    }
    
/************************* About Callback function *********************************/

    function wphester_plus_abt_callback ( $control ) 
    {
        if( true == $control->manager->get_setting ('about_section_enable')->value()){
            return true;
        }
        else {
            return false;
        }       
    }   


/* * *********************** Theme Customizer with Sanitize function ******************************** */

function wphester_plus_theme_option($wp_customize) {

    function wphester_plus_sanitize_text($input) {
        return wp_kses_post(force_balance_tags($input));
    }

    function wphester_plus_copyright_sanitize_text($input) {
        return wp_kses_post(force_balance_tags($input));
    }

    function wphester_plus_sanitize_checkbox($checked) {
        // Boolean check.
        return ( ( isset($checked) && true == $checked ) ? true : false );
    }

    function wphester_plus_sanitize_select($input, $setting) {

        //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
        $input = sanitize_key($input);

        //get the list of possible radio box options 
        $choices = $setting->manager->get_control($setting->id)->choices;

        //return input if valid or return default option
        return ( array_key_exists($input, $choices) ? $input : $setting->default );
    }

    if (!function_exists('wphester_plus_sanitize_number_range')) :

        /**
         * Sanitize number range.
         *
         * @since 1.0.0
         *
         * @see absint() https://developer.wordpress.org/reference/functions/absint/
         *
         * @param int  $input Number to check within the numeric range defined by the setting.
         * @param WP_Customize_Setting $setting WP_Customize_Setting instance.
         * @return int|string The number, if it is zero or greater and falls within the defined range; otherwise, the setting default.
         */
        function wphester_plus_sanitize_number_range($input, $setting) {

            // Ensure input is an absolute integer.
            $input = absint($input);

            // Get the input attributes associated with the setting.
            $atts = $setting->manager->get_control($setting->id)->input_attrs;

            // Get min.
            $min = ( isset($atts['min']) ? $atts['min'] : $input );

            // Get max.
            $max = ( isset($atts['max']) ? $atts['max'] : $input );

            // Get Step.
            $step = ( isset($atts['step']) ? $atts['step'] : 1 );

            // If the input is within the valid range, return it; otherwise, return the default.
            return ( $min <= $input && $input <= $max && is_int($input / $step) ? $input : $setting->default );
        }

    endif;

    function wphester_plus_sanitize_array($value) {
        if (is_array($value)) {
            foreach ($value as $key => $subvalue) {
                $value[$key] = esc_attr($subvalue);
            }
            return $value;
        }
        return esc_attr($value);
    }

}

add_action('customize_register', 'wphester_plus_theme_option');

if (isset($wp_customize->selective_refresh)) {
    $wp_customize->selective_refresh->add_partial(
            'blogname',
            array(
                'selector' => '.site-title a',
                'settings' => 'blogname',
                'render_callback' => 'wphester_customize_partial_blogname',
            )
    );
    $wp_customize->selective_refresh->add_partial(
            'blogdescription',
            array(
                'selector' => '.site-description',
                'settings' => 'blogdescription',
                'render_callback' => 'wphester_customize_partial_blogdescription',
            )
    );
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function wphester_plus_customize_partial_blogname() {
    return get_theme_mod('blogname');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function wphester_plus_customize_partial_blogdescription() {
    return get_theme_mod('blogdescription');
}
