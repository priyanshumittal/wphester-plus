<?php

//Callout Section
$wp_customize->add_section('home_cta2_page_section', array(
    'title' => esc_html__('CTA Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 15,
));

// Enable call to action section
$wp_customize->add_setting('cta2_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_sanitize_checkbox'
    ));

$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'cta2_section_enable',
                array(
            'label' => esc_html__('Enable/Disable CTA Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'home_cta2_page_section',
                )
));

//cta2 Background Image
$wp_customize->add_setting('callout_cta2_background', array(
    'default'=> WPHESTERP_PLUGIN_URL.'/inc/images/bg/cta-3.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'callout_cta2_background', array(
            'label' => esc_html__('Background Image', 'wphester-plus'),
            'section' => 'home_cta2_page_section',
            'settings' => 'callout_cta2_background',
            'active_callback' => 'wphester_plus_cta_callback'
        )));

// Image overlay
$wp_customize->add_setting('cta2_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('cta2_image_overlay', array(
    'label' => esc_html__('Enable CTA Image Overlay', 'wphester-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'checkbox',
    'active_callback' => 'wphester_plus_cta_callback'
));


//callout Background Overlay Color
$wp_customize->add_setting('cta2_overlay_section_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'rgba(0, 0, 0, 0.75)',
));

$wp_customize->add_control(new WPHester_Plus_Customize_Alpha_Color_Control($wp_customize, 'cta2_overlay_section_color', array(
            'label' => esc_html__('CTA Image Overlay Color', 'wphester-plus'),
            'palette' => true,
            'active_callback' => 'wphester_plus_cta_callback',
            'section' => 'home_cta2_page_section')
));

$wp_customize->add_setting(
        'home_cta2_title',
        array(
            'default' => esc_html__('Best Solutions', 'wphester-plus'),
            
        )
);
$wp_customize->add_control('home_cta2_title', array(
    'label' => esc_html__('Title', 'wphester-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_cta_callback'
    ));

$wp_customize->add_setting(
        'home_cta2_desc',
        array(
            'default' => __('Make you feel <br> good is our main priority!', 'wphester-plus'),
           
        )
);
$wp_customize->add_control('home_cta2_desc', array(
    'label' => esc_html__('Description', 'wphester-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'textarea',
    'active_callback' => 'wphester_plus_cta_callback'
    ));

//Button link 
$wp_customize->add_setting(
        'home_cta2_btn1_link',
        array(
            'default' => '<iframe width="560" height="315" src="https://www.youtube.com/embed/X7lTMMtoA74" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
        
));


$wp_customize->add_control(
        'home_cta2_btn1_link',
        array(
            'label' => esc_html__('Embed Video Code', 'wphester-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'textarea',
            'active_callback' => 'wphester_plus_cta_callback'
));

/**
 * Add selective refresh for Front page pricing section controls.
 */
$wp_customize->selective_refresh->add_partial('home_cta2_title', array(
    'selector' => '.cta .section-subtitle',
    'settings' => 'home_cta2_title',
    'render_callback' => 'home_cta2_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_desc', array(
    'selector' => '.cta .section-title',
    'settings' => 'home_cta2_desc',
    'render_callback' => 'home_cta2_desc_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_cta2_btn1_link', array(
    'selector' => '.cta #cta-video',
    'settings' => 'home_cta2_btn1_link',
    'render_callback' => 'home_cta2_btn1_link_render_callback',
));

function home_cta2_btn1_link_render_callback() {
    return get_theme_mod('home_cta2_btn1_link');
}
function home_cta2_title_render_callback() {
    return get_theme_mod('home_cta2_title');
}

function home_cta2_desc_render_callback() {
    return get_theme_mod('home_cta2_desc');
}
?>