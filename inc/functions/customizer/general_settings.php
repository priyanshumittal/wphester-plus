<?php

/**
 * General Settings Customizer
 *
 * @package wphester
 */
function wphester_plus_general_settings_customizer($wp_customize) {

    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';

    class WPHester_Plus_Header_Logo_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>
            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>
            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>
            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">
                <?php foreach ($this->choices as $value => $args) : ?>
                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />
                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) : ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                        <?php endif; ?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], WPHESTERP_PLUGIN_URL, get_stylesheet_directory_uri())); ?>"
                        <?php
                        if (!empty($args['label'])) :
                            echo 'alt="' . esc_attr($args['label']) . '"';
                        endif;
                        ?>
                             />
                    </label>
            <?php endforeach; ?>
            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();


                if(jQuery("#_customize-input-after_menu_multiple_option").val()=='menu_btn')
                {
                    jQuery("#customize-control-after_menu_btn_txt").show();
                    jQuery("#customize-control-after_menu_btn_link").show();
                    jQuery("#customize-control-after_menu_btn_new_tabl").show();
                    jQuery("#customize-control-after_menu_btn_border").show();
                    jQuery("#customize-control-after_menu_html").hide();  
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                }
                else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='html')
                {
                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                    jQuery("#customize-control-after_menu_html").show(); 
                }
                else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='top_menu_widget')
                {

                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_html").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").show();
                }
                else
                {
                    jQuery("#customize-control-after_menu_btn_txt").hide();
                    jQuery("#customize-control-after_menu_btn_link").hide();
                    jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                    jQuery("#customize-control-after_menu_btn_border").hide();
                    jQuery("#customize-control-after_menu_html").hide();
                    jQuery("#customize-control-after_menu_widget_area_section").hide();
                }
                
                //Js For Homepage Slider Variation
                if(jQuery("#_customize-input-slide_variation").val()=='slide')
                {
                    jQuery("#customize-control-slide_video_upload").hide();
                    jQuery("#customize-control-slide_video_url").hide();
                }
                else
                {
                    jQuery("#customize-control-slide_video_upload").show();
                    jQuery("#customize-control-slide_video_url").show();
                }
                });
            </script>
            <?php
        }
           
        /**
         * Loads the jQuery UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            add_action('customize_controls_print_styles', array($this, 'print_styles'));
        }

        /**
         * Outputs custom styles to give the selected image a visible border.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function print_styles() {
            ?>
            <style type="text/css" id="hybrid-customize-radio-image-css">
                .customize-control-radio-image .ui-buttonset {
                    text-align: center;
                }
                .footer_widget_area_placing-3, .footer_widget_area_placing-4, .footer_widget_area_placing-6
                {
                    max-width: 32% !important;
                }
                .footer_widget_area_placing-3 .wp-ui-highlight, .footer_widget_area_placing-4 .wp-ui-highlight, .footer_widget_area_placing-6 .wp-ui-highlight {
                    background-color: #E6E6E6 !important;
                }
                .customize-control-radio-image label {
                    display: inline-block;
                    max-width: 49%;
                    padding: 3px;
                    font-size: inherit;
                    line-height: inherit;
                    height: auto;
                    cursor: pointer;
                    border-width: 0;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0;
                    border-radius: 0;
                    white-space: nowrap;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    color: inherit;
                    background: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    vertical-align: inherit;
                }
                .customize-control-radio-image label:first-of-type {
                    float: left;
                }
                .customize-control-radio-image label:hover {
                    background: none;
                    border-color: inherit;
                    color: inherit;
                }
                .customize-control-radio-image label:active {
                    background: none;
                    border-color: inherit;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    -webkit-transform: none;
                    -ms-transform: none;
                    transform: none;
                }
                .customize-control-radio-image img { border: 1px solid transparent; }
                .customize-control-radio-image .ui-state-active img {
                    border-color: #5B9DD9;
                    -webkit-box-shadow: 0 0 3px rgba(0,115,170,.8);
                    box-shadow: 0 0 3px rgba(0,115,170,.8);
                }
            </style>
            <?php
        }

    }

    $wp_customize->add_panel('wphester_general_settings',
            array(
                'priority' => 124,
                'capability' => 'edit_theme_options',
                'title' => __('General Settings', 'wphester-plus')
            )
    );

          // Preloader
    $wp_customize->add_section(
        'preloader_section',
        array(
            'title' =>esc_html__('Preloader','wphester-plus'),
            'panel'  => 'wphester_general_settings',
            'priority'   => 1,
            
            )
    );

     $wp_customize->add_setting('preloader_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'wphester_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize, 'preloader_enable',
        array(
            'label'    => esc_html__( 'Enable/Disable Preloader', 'wphester-plus' ),
            'section'  => 'preloader_section',
            'type'     => 'toggle',
            'priority' => 1,
        )
    ));

    if ( class_exists( 'WPHester_Plus_Header_Logo_Customize_Control_Radio_Image' ) ) {
            $wp_customize->add_setting(
                'preloader_style', array(
                    'default'           => 1,
                )
            );

            $wp_customize->add_control(
                new WPHester_Plus_Header_Logo_Customize_Control_Radio_Image(
                    $wp_customize, 'preloader_style', array(
                        'label'    => esc_html__('Preloader Style', 'wphester-plus' ),
                        'description' => esc_html__('You can change the color skin of only first and second Preloader styles', 'wphester-plus' ),
                        'priority' => 2,
                        'section' => 'preloader_section',
                        'choices' => array(
                            1 => array(
                                'url' => trailingslashit( WPHESTERP_PLUGIN_URL ) . 'inc/images/loader/preloader1.png',
                            ),
                            2 => array(
                                'url' => trailingslashit( WPHESTERP_PLUGIN_URL ) . 'inc/images/loader/preloader2.png',
                            ),
                            3 => array(
                                'url' => trailingslashit( WPHESTERP_PLUGIN_URL ) . 'inc/images/loader/preloader3.png',

                            ),
                            4 => array(
                                'url' => trailingslashit( WPHESTERP_PLUGIN_URL ) . 'inc/images/loader/preloader4.png',
                                
                            ),
                            5 => array(
                                'url' => trailingslashit( WPHESTERP_PLUGIN_URL ) . 'inc/images/loader/preloader5.png',
                                
                            ),
                            6 => array(
                                'url' => trailingslashit( WPHESTERP_PLUGIN_URL ) . 'inc/images/loader/preloader6.png',
                                
                            ),
                        ),
                    )
                )
            );
        }





    // After Menu
    $wp_customize->add_section(
        'after_menu_setting_section',
        array(
            'title' =>esc_html__('After Menu','wphester-plus'),
            'panel'  => 'wphester_general_settings',
            'priority'   => 3,
            )
    );

    //Dropdown button or html option
    $wp_customize->add_setting(
    'after_menu_multiple_option',
    array(
        'default'           =>  'none',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'wphester_plus_sanitize_select',
    ));
    $wp_customize->add_control('after_menu_multiple_option', array(
        'label' => esc_html__('After Menu','wphester-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_multiple_option',
        'type'    =>  'select',
        'choices' =>  array(
            'none'      =>  esc_html__('None', 'wphester-plus'),
            'menu_btn'  => esc_html__('Button', 'wphester-plus'),
            'html'      => esc_html__('HTML', 'wphester-plus'),
            'top_menu_widget' => esc_html__('Widget', 'wphester-plus'),
            )
    ));

    //After Menu Button Text
    $wp_customize->add_setting(
    'after_menu_btn_txt',
    array(
        'default'           =>  '',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'wphester_plus_home_page_sanitize_text',
    ));
    $wp_customize->add_control('after_menu_btn_txt', array(
        'label' => esc_html__('Button Text','wphester-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_txt',
        'type' => 'text',
    ));

    //After Menu Button Text
    $wp_customize->add_setting(
    'after_menu_btn_link',
    array(
        'default'           =>  '#',
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'esc_url_raw',
    ));
    $wp_customize->add_control('after_menu_btn_link', array(
        'label' => esc_html__('Button Link','wphester-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_link',
        'type' => 'text',
    ));

    //Open in new tab
    $wp_customize->add_setting(
    'after_menu_btn_new_tabl',
    array(
        'default'           =>  false,
        'capability'        =>  'edit_theme_options',
        'sanitize_callback' =>  'wphester_sanitize_checkbox',
    ) );
    
    $wp_customize->add_control('after_menu_btn_new_tabl', array(
        'label' => esc_html__('Open link in a new tab','wphester-plus'),
        'section' => 'after_menu_setting_section',
        'setting' => 'after_menu_btn_new_tabl',
        'type'    =>  'checkbox'
    )); 

    //Border Radius
    $wp_customize->add_setting( 'after_menu_btn_border',
            array(
                'default' => 0,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'after_menu_btn_border',
            array(
                'label' => esc_html__( 'Button Border Radius', 'wphester-plus' ),
                'section' => 'after_menu_setting_section',
                'input_attrs' => array(
                    'min' => 0,
                    'max' => 30,
                    'step' => 1,),)
        ));

    //After Menu HTML section
    $wp_customize->add_setting('after_menu_html', 
        array(
        'default'=> '',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback'=> 'wphester_plus_home_page_sanitize_text',
        )
    );

    $wp_customize->add_control('after_menu_html', 
        array(
            'label'=> __('HTML','wphester-plus'),
            'section'=> 'after_menu_setting_section',
            'type'=> 'textarea',
        )
    );


    class WP_After_Menu_Widget_Customize_Control extends WP_Customize_Control {
        public $type = 'new_menu';
        /**
        * Render the control's content.
        */
        public function render_content() {
        ?>
         <h3><?php _e('To add widgets, Go to Widgets >> After Menu Widget Area','wphester-plus');?></h3>
        <?php
        }
    }
    $wp_customize->add_setting(
        'after_menu_widget_area_section',
        array(
            'capability'     => 'edit_theme_options',
            'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
        )   
    );
    $wp_customize->add_control( new WP_After_Menu_Widget_Customize_Control( $wp_customize, 'after_menu_widget_area_section', array( 
            'section' => 'after_menu_setting_section',
            'setting' => 'after_menu_widget_area_section',
        ))
    );

    //Enable/Disable Cart Icon
    $wp_customize->add_setting('cart_btn_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'wphester_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize, 'cart_btn_enable',
        array(
            'label'    => esc_html__( 'Enable/Disable Cart Icon', 'wphester-plus' ),
            'section'  => 'after_menu_setting_section',
            'type'     => 'toggle',
        )
    ));
    
    // Header Preset Section
    $wp_customize->add_section(
            'header_preset_setting_section',
            array(
                'title' => __('Header Presets', 'wphester-plus'),
                'panel' => 'wphester_general_settings',
                'priority' => 99,
            )
    );
    if (class_exists('WPHester_Plus_Header_Logo_Customize_Control_Radio_Image')) {
        $wp_customize->add_setting(
                'header_logo_placing', array(
            'default' => 'seven',
                )
        );
        $wp_customize->add_control(new WPHester_Plus_Header_Logo_Customize_Control_Radio_Image(
                        $wp_customize, 'header_logo_placing', array(
                    'label' => esc_html__('Header layout with logo placing', 'wphester-plus'),
                    'priority' => 6,
                    'section' => 'header_preset_setting_section',
                    'choices' => array(
                        'left' => array(
                            'url' => trailingslashit(WPHESTERP_PLUGIN_URL) . 'inc/images/header-preset/container-right.png',
                        ),
                        'right' => array(
                            'url' => trailingslashit(WPHESTERP_PLUGIN_URL) . 'inc/images/header-preset/container-left.png',
                        ),
                        'center' => array(
                            'url' => trailingslashit(WPHESTERP_PLUGIN_URL) . 'inc/images/header-preset/center.png',
                        ),
                        'full' => array(
                            'url' => trailingslashit(WPHESTERP_PLUGIN_URL) . 'inc/images/header-preset/full-left.png',
                        ),
                         'five' => array(
                            'url' => trailingslashit(WPHESTERP_PLUGIN_URL) . 'inc/images/header-preset/5.png',
                            
                        ),
                        'six' => array(
                            'url' => trailingslashit(WPHESTERP_PLUGIN_URL) . 'inc/images/header-preset/6.png',
                            
                        ),
                        'seven' => array(
                            'url' => trailingslashit(WPHESTERP_PLUGIN_URL) . 'inc/images/header-preset/7.png',
                            
                        ),
                    ),
                    )
                )
        );
    }

    //Menu Header Info First
    $wp_customize->add_setting(
            'menu_header_icon1', array(
        'default' => __('fa-map', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_icon1',
            array(
                'type' => 'text',
                'label' => __('Icon First', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
            );

    $wp_customize->add_setting(
            'menu_header_title1', array(
        'default' => __('Address', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_title1',
            array(
                'type' => 'text',
                'label' => __('Title First', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
    );

    $wp_customize->add_setting(
            'menu_header_text1', array(
        'default' => __('Art Street 235 Newyork', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_text1',
            array(
                'type' => 'textarea',
                'label' => __('Text First', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
    );

    //Menu Header Info Second
    $wp_customize->add_setting(
            'menu_header_icon2', array(
        'default' => __('fa-regular fa-clock', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',

    ));

    $wp_customize->add_control(
            'menu_header_icon2',
            array(
                'type' => 'text',
                'label' => __('Icon Second', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
        
    );

    $wp_customize->add_setting(
            'menu_header_title2', array(
        'default' => __('Opening Hours', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_title2',
            array(
                'type' => 'text',
                'label' => __('Title Second', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
    );

    $wp_customize->add_setting(
            'menu_header_text2', array(
        'default' => __('Mon - Sat 8.00 - 18.00', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_text2',
            array(
                'type' => 'textarea',
                'label' => __('Text Second', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
    );

    //Menu Header Info Third
    $wp_customize->add_setting(
            'menu_header_icon3', array(
        'default' => __('fa-phone', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_icon3',
            array(
                'type' => 'text',
                'label' => __('Icon Third', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
    );

    $wp_customize->add_setting(
            'menu_header_title3', array(
        'default' => __('Contact', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_title3',
            array(
                'type' => 'text',
                'label' => __('Title Third', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
    );

    $wp_customize->add_setting(
            'menu_header_text3', array(
        'default' => __('Mobile: 9876543210', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'menu_header_text3',
            array(
                'type' => 'textarea',
                'label' => __('Text Third', 'wphester-plus'),
                'section' => 'header_preset_setting_section',
                'active_callback' => 'wphester_plus_menu_header_info_callback',
            )
        
    );

    // Sticky Header 
    $wp_customize->add_section(
        'sticky_header_section',
        array(
            'title' =>__('Sticky Header','wphester-plus'),
            'panel'  => 'wphester_general_settings',
            'priority'   => 99,
            
            )
    );

     $wp_customize->add_setting('sticky_header_enable',
        array(
            'default' => false,
            'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize, 'sticky_header_enable',
        array(
            'label'    => __( 'Enable/Disable Sticky Header', 'wphester-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'toggle',
            'priority'              => 1,
        )
    ));

    //Differet logo for sticky header
    $wp_customize->add_setting('sticky_header_logo',
        array(
            'default' => false,
            'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize, 'sticky_header_logo',
        array(
            'label'    => __( 'Different Logo for Sticky Header', 'wphester-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'toggle',
            'active_callback' => 'wphester_plus_sticky_header_callback',
            'priority'  => 2,
        )
    ));

    // Stick Header logo for Desktop
    $wp_customize->add_setting( 'sticky_header_logo_desktop', array(
              'sanitize_callback' => 'esc_url_raw',
            ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sticky_header_logo_desktop', array(
              'label'    => __( 'Logo for desktop view', 'wphester-plus' ),
              'section'  => 'sticky_header_section',
              'settings' => 'sticky_header_logo_desktop',
              'active_callback' => function($control) {
                return (
                        wphester_plus_sticky_header_logo_callback($control) &&
                        wphester_plus_sticky_header_callback($control)
                        );
            },
              'priority'    => 3,
            ) ) );

    // Stick Header logo for Mobile
    $wp_customize->add_setting( 'sticky_header_logo_mbl', array(
              'sanitize_callback' => 'esc_url_raw',
            ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sticky_header_logo_mbl', array(
              'label'    => __( 'Logo for mobile view', 'wphester-plus' ),
              'section'  => 'sticky_header_section',
              'settings' => 'sticky_header_logo_mbl',
              'active_callback' => function($control) {
                return (
                        wphester_plus_sticky_header_logo_callback($control) &&
                        wphester_plus_sticky_header_callback($control)
                        );
            },
              'priority'    => 4,
            ) ) );

    //Sticky Header Animation Effect
    $wp_customize->add_setting( 'sticky_header_animation', array( 'default' => '') );
    $wp_customize->add_control( 'sticky_header_animation', 
        array(
            'label'    => __( 'Animation Effect', 'wphester-plus' ),
            'description' => esc_html__( 'Note: Shrink Effect will not work with Header Presets Five, Six and Seven.', 'wpkites-plus' ),        
            'section'  => 'sticky_header_section',
            'type'     => 'select',
             'active_callback' => 'wphester_plus_sticky_header_callback',
            'choices'=>array(
                ''=>__('None', 'wphester-plus'),
                'slide'=>__('Slide', 'wphester-plus'),
                'fade'=>__('Fade', 'wphester-plus'),
                'shrink'=>__('Shrink', 'wphester-plus'),
                )
    ));

    //Sticky Header Enable
    $wp_customize->add_setting( 'sticky_header_device_enable', array( 'default' => 'desktop') );
    $wp_customize->add_control( 'sticky_header_device_enable', 
        array(
            'label'    => __( 'Enable', 'wphester-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'select',
             'active_callback' => 'wphester_plus_sticky_header_callback',
            'choices'=>array(
                'desktop'=>__('Desktop', 'wphester-plus'),
                'mobile'=>__('Mobile', 'wphester-plus'),
                'both'=>__('Desktop + Mobile', 'wphester-plus')
                )
    ));
        
        //Sticky Header Opacity
    $wp_customize->add_setting( 'sticky_header_device_enable', array( 'default' => 'desktop') );
    $wp_customize->add_control( 'sticky_header_device_enable', 
        array(
            'label'    => __( 'Enable', 'wphester-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'select',
            'active_callback' => 'wphester_plus_sticky_header_callback',
            'choices'=>array(
                'desktop'=>__('Desktop', 'wphester-plus'),
                'mobile'=>__('Mobile', 'wphester-plus'),
                'both'=>__('Desktop + Mobile', 'wphester-plus')
                )
    ));

    $wp_customize->add_setting('sticky_header_opacity',
        array(
            'default' => 1.0,
            'capability'     => 'edit_theme_options',
            ));

    $wp_customize->add_control(new WPHester_Plus_Opacity_Control( $wp_customize, 'sticky_header_opacity',
        array(
            'label'    => __( 'Sticky Opacity', 'wphester-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'slider',
            'active_callback' => 'wphester_plus_sticky_header_callback',
            'min'   => 0.1,
            'max'   => 1.0,
        )
    ));
        
        //Sticky Header Height
    $wp_customize->add_setting('sticky_header_height',
        array(
            'default' => 0,
            'capability'     => 'edit_theme_options',
            )
    );

    $wp_customize->add_control(new WPHester_Plus_Slider_Control( $wp_customize, 'sticky_header_height',
        array(
            'label'    => __( 'Sticky Height', 'wphester-plus' ),
            'description'    => esc_html__( 'Note: Sticky Height will not work with shrink effect', 'wphester-plus' ),
            'section'  => 'sticky_header_section',
            'type'     => 'slider',
            'active_callback' => 'wphester_plus_sticky_header_callback',
            'min'   => 0,
            'max'   => 50,
        )
    ));
    
    // Search Effect settings
    $wp_customize->add_section(
            'search_effect_setting_section',
            array(
                'title' => __('Search Settings', 'wphester-plus'),
                'panel' => 'wphester_general_settings',
                'priority' => 100,
            )
    );
    //Enable/Disable Search Effect
    $wp_customize->add_setting('search_btn_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'search_btn_enable',
                    array(
                'label' => __('Enable/Disable Search Icon', 'wphester-plus'),
                'section' => 'search_effect_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));
    $wp_customize->add_setting('search_effect_style_setting',
            array(
                'default' => 'toggle',
                'sanitize_callback' => 'wphester_sanitize_select'
            )
    );
    $wp_customize->add_control('search_effect_style_setting',
            array(
                'label' => esc_html__('Choose Search Effect', 'wphester-plus'),
                'section' => 'search_effect_setting_section',
                'type' => 'radio',
                'active_callback' => 'search_icon_hide_show_callback',
                'choices' => array(
                    'toggle' => esc_html__('Toggle', 'wphester-plus'),
                    'popup_light' => esc_html__('Pop up light', 'wphester-plus'),
                    'popup_dark' => esc_html__('Pop up dark', 'wphester-plus'),
                )
            )
    );

    // add section to manage breadcrumb settings
    $wp_customize->add_section(
            'breadcrumb_setting_section',
            array(
                'title' => __('Breadcrumb Settings', 'wphester-plus'),
                'panel' => 'wphester_general_settings',
                'priority' => 100,
            )
    );

    // enable/disable banner
    $wp_customize->add_setting('banner_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'banner_enable',
                    array(
                'label' => __('Enable/Disable Banner', 'wphester-plus'),
                'section' => 'breadcrumb_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));

    // enable/disable banner
    $wp_customize->add_setting('breadcrumb_subtitle_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'breadcrumb_subtitle_enable',
                    array(
                'label' => __('Enable/Disable Breadcrumbs Subtitle', 'wphester-plus'),
                'section' => 'breadcrumb_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                'active_callback' => 'wphester_plus_breadcrumb_callback',
                    )
    ));


    // enable/disable breadcrumb
     $wp_customize->add_setting('breadcrumb_setting_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'breadcrumb_setting_enable',
                    array(
                'label' => __('Enable/Disable Breadcrumbs', 'wphester-plus'),
                'section' => 'breadcrumb_setting_section',
                'type' => 'toggle',
                'active_callback' => 'wphester_plus_breadcrumb_callback',
                'priority' => 2,
                    )
    ));
	
   //Breadcrumbs type
	$wp_customize->add_setting(
    'wphester_breadcrumb_type',
    array(
        'default'           =>  'yoast',
		'capability'        =>  'edit_theme_options',
		'sanitize_callback' =>  'wphester_sanitize_select',
    ));
	$wp_customize->add_control('wphester_breadcrumb_type', array(
		'label' => esc_html__('Breadcrumb type','wphester'),
		'description' => esc_html__( 'If you use other than "default" one you will need to install and activate respective plugins Breadcrumb','wphester-plus') . ' NavXT, Yoast SEO ' . __('and','wphester-plus') . ' Rank Math SEO',
        'section' => 'breadcrumb_setting_section',
		'setting' => 'wphester_breadcrumb_type',
		'type'    =>  'select',
		  'priority' => 3,
		'choices' =>  array(
			'default' => __( 'Default(Disable)','wphester-plus'),
            'yoast'  => 'Yoast SEO',
            'rankmath'  => 'Rank Math',
			'navxt'  => 'NavXT',
			)
	));
    
    // Image overlay
    $wp_customize->add_setting('breadcrumb_image_overlay',
            array(
                'default' => true,
                'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );
    $wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'breadcrumb_image_overlay',
                    array(
                'label' => __('Enable/Disable Banner Image Overlay', 'wphester-plus'),
                'section' => 'breadcrumb_setting_section',
                'type' => 'toggle',
                'active_callback' => 'wphester_plus_breadcrumb_callback',
                'priority' => 4,
                    )
    ));
    
    //breadcrumb overlay color
    $wp_customize->add_setting( 'breadcrumb_overlay_section_color', array(
        'sanitize_callback' => 'sanitize_text_field',
        'default' => 'rgba(0,0,0,0.6)',
        ) 
    );  
            
    $wp_customize->add_control(new WPHester_Plus_Customize_Alpha_Color_Control( $wp_customize,'breadcrumb_overlay_section_color', array(
        'label'      => __('Image Overlay Color','wphester-plus' ),
        'palette' => true,
        'active_callback' => 'wphester_plus_breadcrumb_callback',
        'section' => 'breadcrumb_setting_section')
    ) );
    //Bg Image for 404 page
    $wp_customize->add_setting( 'error_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'error_background_img', array(
              'label'    => __( '404 Page', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'error_background_img',
              'active_callback' => 'wphester_plus_breadcrumb_callback',
    ) ) );

    //Bg Image for search page
    $wp_customize->add_setting( 'search_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'search_background_img', array(
              'label'    => __( 'Search Page', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'search_background_img',
              'active_callback' => 'wphester_plus_breadcrumb_callback',
    ) ) );

    //Bg Image for date page
    $wp_customize->add_setting( 'date_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'date_background_img', array(
              'label'    => __( 'Date Archive', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'date_background_img',
              'active_callback' => 'wphester_plus_breadcrumb_callback',
    ) ) );

    //Bg Image for author page
    $wp_customize->add_setting( 'author_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'author_background_img', array(
              'label'    => __( 'Author Archive', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'author_background_img',
              'active_callback' => 'wphester_plus_breadcrumb_callback',
    ) ) );

    // Shop breadcrumb background image
    $wp_customize->add_setting( 'shop_breadcrumb_back_img', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'shop_breadcrumb_back_img', 
        array(
            'label'             =>  esc_html__( 'Shop Page', 'wphester-plus' ),
            'section'           =>  'breadcrumb_setting_section',
            'setting'           =>  'shop_breadcrumb_back_img',
            'active_callback'   =>  'wphester_plus_breadcrumb_callback',
        ) 
    ));

    // add section to manage ribbon settings
    $wp_customize->add_section(
        'ribon_setting_section',
        array(
            'title' =>__('Ribbon settings','wphester-plus'),
            'panel'  => 'wphester_general_settings',
            'priority'   => 101,
            
            )
    );
    
    // enable/disable footer ribbon
    $wp_customize->add_setting('ribon_setting_enable',
        array(
            'default' => true,
            'sanitize_callback' => 'wphester_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new wphester_Toggle_Control( $wp_customize, 'ribon_setting_enable',
        array(
            'label'    => __( 'Enable / Disable Footer Ribbon', 'wphester-plus' ),
            'section'  => 'ribon_setting_section',
            'type'     => 'toggle',
            'priority' => 1,
        )
    ));

    // Ribon title
    $wp_customize->add_setting(
        'ribon_title', array(
        'default' => __('Follow Us:', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
        ));

    $wp_customize->add_control(
        'ribon_title',
        array(
            'type' => 'text',
            'label' => __('Title','wphester-plus'),
            'section' => 'ribon_setting_section',
            'active_callback' => 'ribbon_callback',
        )
    );

    if ( class_exists( 'WPHester_Plus_Repeater' ) ) 
    {
        $wp_customize->add_setting( 'wphester_ribon_content', array() );

        $wp_customize->add_control( new WPHester_Plus_Repeater( $wp_customize, 'wphester_ribon_content', array(
            'label'                             => esc_html__( 'Social Icon Details', 'wphester-plus' ),
            'section'                           => 'ribon_setting_section',
            'active_callback' => 'ribbon_callback',
            'priority'                          => 10,
            'add_field_label'                   => esc_html__( 'Add new Social Icon', 'wphester-plus' ),
            'item_name'                         => esc_html__( 'Social Icon', 'wphester-plus' ),
            'customizer_repeater_icon_control'  => true,
            'customizer_repeater_title_control' => true,
            'customizer_repeater_link_control'  => true,
            'customizer_repeater_checkbox_control' => true,
        ) ) );
    }
    
    $wp_customize->selective_refresh->add_partial('ribon_title', array(
        'selector' => '.site-footer .footer-custom-social-icons li.text-white',
        'settings' => 'ribon_title',
        'render_callback' => 'ribon_title_render_callback'
    ));

    function ribon_title_render_callback() {
        return get_theme_mod('ribon_title');
    }
   
    // Container Setting
    $wp_customize->add_section(
        'container_width_section',
        array(
            'title' =>__('Container Settings','wphester-plus'),
            'panel'  => 'wphester_general_settings',
            'priority'   => 102,
            
            )
    );
    //Bg Image for 404 page
    $wp_customize->add_setting( 'error_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'error_background_img', array(
              'label'    => __( '404 Page', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'error_background_img',
    ) ) );

    //Bg Image for search page
    $wp_customize->add_setting( 'search_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'search_background_img', array(
              'label'    => __( 'Search Page', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'search_background_img',
    ) ) );

    //Bg Image for date page
    $wp_customize->add_setting( 'date_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'date_background_img', array(
              'label'    => __( 'Date Archive', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'date_background_img',
    ) ) );

    //Bg Image for author page
    $wp_customize->add_setting( 'author_background_img', array(
              'sanitize_callback' => 'esc_url_raw',
    ) );
            
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'author_background_img', array(
              'label'    => __( 'Author Archive', 'wphester-plus' ),
              'section'  => 'breadcrumb_setting_section',
              'settings' => 'author_background_img',
    ) ) );
    
    //Container width
    $wp_customize->add_setting( 'container_width_pattern',
            array(
                'default' => 1140,
                'transport' => 'postMessage',
                'sanitize_callback' => 'absint'
            )
        );
        $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_width_pattern',
            array(
                'label'    => __( 'Container Width', 'wphester-plus' ),
                'priority' => 1,
                'section' => 'container_width_section',
                'input_attrs' => array(
                    'min' => 600,
                    'max' => 1900,
                    'step' => 1,),)
        ));


    $wp_customize->add_setting( 'page_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'page_container_setting', 
        array(
            'label'    => __( 'Page Layout', 'wphester-plus' ),
            'priority' => 2,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'wphester-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'wphester-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'wphester-plus'),
                )
        ));
        $wp_customize->add_setting( 'post_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'post_container_setting', 
        array(
            'label'    => __( 'Blog Layout', 'wphester-plus' ),
            'priority' => 3,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'wphester-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'wphester-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'wphester-plus'),
                )
        ));
        $wp_customize->add_setting( 'single_post_container_setting', array( 'default' => 'default') );
        $wp_customize->add_control( 'single_post_container_setting', 
        array(
            'label'    => __( 'Single Post Layout', 'wphester-plus' ),
            'priority' => 4,
            'section'  => 'container_width_section',
            'type'     => 'select',
            'choices'=>array(
                'default'=>__('Default', 'wphester-plus'),
                'full_width_fluid'=>__('Full Width / Container Fluid', 'wphester-plus'),
                'full_width_streched'=>__('Full Width / Streatched', 'wphester-plus'),
                )
        ));


class WP_Home_Container_Customize_Control extends WP_Customize_Control {
    public $type = 'new_menu';
    /**
    * Render the control's content.
    */
    public function render_content() {
    ?>
     <h3><?php esc_attr_e('Homepage Sections Container Width','wphester-plus'); ?></h3>
    <?php
    }
}
$wp_customize->add_setting(
    'business_temp_container_width',
    array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )   
);
$wp_customize->add_control( new WP_Home_Container_Customize_Control( $wp_customize, 'business_temp_container_width', array( 
        'section' => 'container_width_section',
        'setting' => 'business_temp_container_width',
    ))
);

    //Container Width For Slider Section
    $wp_customize->add_setting( 'container_slider_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_slider_width',
        array(
            'label'    => __( 'Slider', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));    


    //Container Width For Service Section
    $wp_customize->add_setting( 'container_service_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_service_width',
        array(
            'label'    => __( 'Service', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    )); 
    
    //Container Width For Fun&Fact Section
    $wp_customize->add_setting( 'container_fun_fact_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_fun_fact_width',
        array(
            'label'    => __( 'FunFact', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));  
    
    //Container Width For Portfolio Section
    $wp_customize->add_setting( 'container_portfolio_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_portfolio_width',
        array(
            'label'    => __( 'Portfolio', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    )); 

    //Container Width For Blog Section
    $wp_customize->add_setting( 'container_home_blog_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_home_blog_width',
        array(
            'label'    => __( 'Blog', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    )); 

    //Container Width For Callout  Section
    $wp_customize->add_setting( 'container_cta2_width',
            array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_cta2_width',
        array(
            'label'    => __( 'CTA', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));

    //Container Width For About  Section
    $wp_customize->add_setting( 'container_about_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_about_width',
        array(
            'label'    => __( 'About', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));   

    
    //Container Width For Testimonial Section
    $wp_customize->add_setting( 'container_testimonial_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_testimonial_width',
        array(
            'label'    => __( 'Testimonial', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));

    //Container Width For TEAM Section
    $wp_customize->add_setting( 'container_team_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_team_width',
        array(
            'label'    => __( 'Team', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));

    //Container Width For SHOP Section
    $wp_customize->add_setting( 'container_shop_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_shop_width',
        array(
            'label'    => __( 'Shop', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    )); 


    //Container Width For Callout  Section
    $wp_customize->add_setting( 'container_cta1_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_cta1_width',
        array(
            'label'    => __( 'Callout', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));

    //Container Width For Client & Partners Section
    $wp_customize->add_setting( 'container_clients_width',
        array(
            'default' => 1140,
            'transport' => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new WPHester_Slider_Custom_Control( $wp_customize, 'container_clients_width',
        array(
            'label'    => __( 'Clients', 'wphester-plus' ),
            'section' => 'container_width_section',
            'input_attrs' => array(
                'min' => 600,
                'max' => 1900,
                'step' => 1,),)
    ));
    
    // Post Navigation effect
    $wp_customize->add_section(
        'post_navigation_section',
        array(
            'title' =>__('Post Navigation','wphester-plus'),
            'panel'  => 'wphester_general_settings',
            'priority'   => 102,
            
            )
    );
    $wp_customize->add_setting('post_nav_style_setting', 
    array(
        'default'           => 'pagination',
        'sanitize_callback' => 'wphester_plus_sanitize_select'
        )
    );

    $wp_customize->add_control('post_nav_style_setting', 
    array(      
        'label'     => esc_html__('Choose Style', 'wphester-plus'),      
        'section'   => 'post_navigation_section',
        'type'      => 'radio',
        'choices'   =>  array(
            'pagination'    => esc_html__('Pagination', 'wphester-plus'),
            'load_more'     => esc_html__('Load More', 'wphester-plus'),
            'infinite'  => esc_html__('Infinite Scroll', 'wphester-plus'),
            )
        )
    );

    // add section to manage scroll_to_top icon settings
    $wp_customize->add_section(
            'scrolltotop_setting_section',
            array(
                'title' => __('Scroll to Top Settings', 'wphester-plus'),
                'panel' => 'wphester_general_settings',
                'priority' => 102,
            )
    );

    // enable/disable scroll_to_top icon
     $wp_customize->add_setting('scrolltotop_setting_enable',
        array(
            'default' => true,
            'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new WPHester_Toggle_Control( $wp_customize, 'scrolltotop_setting_enable',
        array(
            'label'    => __( 'Enable/Disable Scroll to Top', 'wphester-plus' ),
            'section'  => 'scrolltotop_setting_section',
            'type'     => 'toggle',
            'priority'              => 1,
        )
    ));


    $wp_customize->add_setting('scroll_position_setting',
            array(
                'default' => 'right',
                'sanitize_callback' => 'wphester_plus_sanitize_select'
            )
    );

    $wp_customize->add_control('scroll_position_setting',
            array(
                'label' => esc_html__('Choose Position', 'wphester-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'type' => 'radio',
                'choices' => array(
                    'left' => esc_html__('Left', 'wphester-plus'),
                    'right' => esc_html__('Right', 'wphester-plus'),
                )
            )
    );


    $wp_customize->add_setting('wphester_plus_scroll_icon_class',
            array(
                'default' => esc_html__('fa fa-angle-up', 'wphester-plus'),
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control('wphester_plus_scroll_icon_class',
            array(
                'label' => esc_html__('Icon Class Name', 'wphester-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'type' => 'text',
            )
    );

    $wp_customize->add_setting('wphester_plus_scroll_border_radius',
            array(
                'default' => 3,
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'wphester_plus_sanitize_number_range',
            )
    );
    $wp_customize->add_control('wphester_plus_scroll_border_radius',
            array(
                'label' => esc_html__('Border Radius', 'wphester-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'type' => 'number',
                'input_attrs' => array('min' => 0, 'max' => 50, 'step' => 1, 'style' => 'width: 100%;'),
            )
    );

    // enable/disable scrolltotop color settings
    $wp_customize->add_setting(
            'apply_scrll_top_clr_enable',
            array('capability' => 'edit_theme_options',
                'default' => false,
    ));

    $wp_customize->add_control(
            'apply_scrll_top_clr_enable',
            array(
                'label' => esc_html__('Click here to apply the below color settings', 'wphester-plus'),
                'type' => 'checkbox',
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                
            )
    );

    $wp_customize->add_setting(
            'wphester__scroll_bg_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#56ABAB'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wphester__scroll_bg_color',
                    array(
                'label' => esc_html__('Background Color', 'wphester-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wphester__scroll_bg_color',
    )));


    $wp_customize->add_setting(
            'wphester__scroll_icon_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wphester__scroll_icon_color',
                    array(
                'label' => esc_html__('Icon Color', 'wphester-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wphester__scroll_icon_color',
    )));


    $wp_customize->add_setting(
            'wphester__scroll_bghover_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wphester__scroll_bghover_color',
                    array(
                'label' => esc_html__('Background Hover Color', 'wphester-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wphester__scroll_bghover_color',
    )));


    $wp_customize->add_setting(
            'wphester__scroll_iconhover_color', array(
        'capability' => 'edit_theme_options',
        'default' => '#56ABAB'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'wphester__scroll_iconhover_color',
                    array(
                'label' => esc_html__('Icon Hover Color', 'wphester-plus'),
                'section' => 'scrolltotop_setting_section',
                'active_callback' => 'scroll_top_callback',
                'settings' => 'wphester__scroll_iconhover_color',
    )));

   
}

add_action('customize_register', 'wphester_plus_general_settings_customizer');
?>