<?php

//Shop Section
$wp_customize->add_section('wphester_shop_section', array(
    'title' => __('Home Shop Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 19,
));

$wp_customize->add_setting('shop_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_sanitize_checkbox'
    ));

$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'shop_section_enable',
                array(
            'label' => __('Enable/Disable Shop Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'wphester_shop_section',
                )
));

// Shop section title
$wp_customize->add_setting('home_shop_section_title', array(
    'default' => __('Featured Products', 'wphester-plus'),
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_shop_section_title', array(
    'label' => __('Title', 'wphester-plus'),
    'section' => 'wphester_shop_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_wooproduct_callback'
));

//Shop section discription
$wp_customize->add_setting('home_shop_section_discription', array(
    'default' => __('Our amazing products', 'wphester-plus'),
));
$wp_customize->add_control('home_shop_section_discription', array(
    'label' => __('Sub Title', 'wphester-plus'),
    'section' => 'wphester_shop_section',
    'type' => 'textarea',
    'active_callback' => 'wphester_plus_wooproduct_callback'
));

//Navigation Type
$wp_customize->add_setting('shop_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('shop_nav_style', array(
    'label' => __('Navigation Style', 'wphester-plus'),
    'section' => 'wphester_shop_section',
    'type' => 'radio',
    'priority' => 12,
    'choices' => array(
        'bullets' => __('Bullets', 'wphester-plus'),
        'navigation' => __('Navigation', 'wphester-plus'),
        'both' => __('Both', 'wphester-plus'),
    ),
    'active_callback' => 'wphester_plus_wooproduct_callback'
));

// animation speed
$wp_customize->add_setting('shop_animation_speed', array('default' => 3000));
$wp_customize->add_control('shop_animation_speed',
        array(
            'label' => __('Animation Speed', 'wphester-plus'),
            'section' => 'wphester_shop_section',
            'type' => 'select',
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'wphester_plus_wooproduct_callback'
));

// smooth speed
$wp_customize->add_setting('shop_smooth_speed', array('default' => 1000));
$wp_customize->add_control('shop_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wphester-plus'),
            'section' => 'wphester_shop_section',
            'type' => 'select',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0'),
            'active_callback' => 'wphester_plus_wooproduct_callback'
));

$wp_customize->selective_refresh->add_partial('home_shop_section_title', array(
    'selector' => '.shop .section-header h2',
    'settings' => 'home_shop_section_title',
    'render_callback' => 'wphester_plus_home_shop_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_shop_section_discription', array(
    'selector' => '.shop .section-subtitle',
    'settings' => 'home_shop_section_discription',
    'render_callback' => 'wphester_plus_home_shop_section_discription_render_callback',
));

function wphester_plus_home_shop_section_title_render_callback() {
    return get_theme_mod('home_shop_section_title');
}

function wphester_plus_home_shop_section_discription_render_callback() {
    return get_theme_mod('home_shop_section_discription');
}
?>