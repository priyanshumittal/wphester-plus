<?php
$wp_customize->add_section('portfolio_section', array(
    'title' => __('Portfolio Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 13,
));

// Enable portfolio more btn
$wp_customize->add_setting('portfolio_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_sanitize_checkbox'
));

$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'portfolio_section_enable',
                array(
            'label' => __('Enable/Disable Portfolio Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'portfolio_section',
                )
));

//  section title
$wp_customize->add_setting('home_portfolio_section_title', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'default' => __('Our Latest Case Studies', 'wphester-plus'),
));

$wp_customize->add_control('home_portfolio_section_title', array(
    'label' => __('Title', 'wphester-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_portfolio_callback'
));

//  section subtitle
$wp_customize->add_setting('home_portfolio_section_subtitle', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'default' => __('Recent Projects', 'wphester-plus'),
));

$wp_customize->add_control('home_portfolio_section_subtitle', array(
    'label' => __('Sub Title', 'wphester-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_portfolio_callback'
));

//link
class WP_client_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    /**
     * Render the control's content.
     */
    public function render_content() {
        ?>
        <a href="<?php bloginfo('url'); ?>/wp-admin/edit.php?post_type=wphester_portfolio" class="button"  target="_blank"><?php _e('Click here to add project', 'wphester-plus'); ?></a>
        <?php
    }

}

$wp_customize->add_setting(
        'pro_project',
        array(
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )
);

$wp_customize->add_control(new WP_client_Customize_Control($wp_customize, 'pro_project', array(
            'section' => 'portfolio_section',
            'active_callback' => 'wphester_plus_portfolio_callback'
                ))
);

// Number of Portfolio in Portfolio's Section
$wp_customize->add_setting(
        'home_portfolio_numbers_options',
        array(
            'default' => 4,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
        )
);
$wp_customize->add_control('home_portfolio_numbers_options',
        array(
            'type' => 'number',
            'label' => __('Number of Projects on Portfolio Section', 'wphester-plus'),
            'section' => 'portfolio_section',
            'input_attrs' => array(
                'min' => '1', 'step' => '1', 'max' => '50',
            ),
            'active_callback' => 'wphester_plus_portfolio_callback'
        )
);

//Slide Item
$wp_customize->add_setting('home_portfolio_slide_item', array('default' => 4));
$wp_customize->add_control('home_portfolio_slide_item',
        array(
            'label' => __('Column Layout', 'wphester-plus'),
            'active_callback' => 'wphester_plus_portfolio_callback',
            'section' => 'portfolio_section',
            'type' => 'select',
            'choices' => array(
                6 => __('2 Column', 'wphester-plus'),
                4 => __('3 Column', 'wphester-plus'),
                3 => __('4 Column', 'wphester-plus'),
            )
));

//  section Button
$wp_customize->add_setting('home_portfolio_section_button', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'default' => __('Load More', 'wphester-plus'),
));

$wp_customize->add_control('home_portfolio_section_button', array(
    'label' => __('Button Title', 'wphester-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_portfolio_callback'
));

//Button link
$wp_customize->add_setting('home_portfolio_section_button_link', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'default' => '#',
));

$wp_customize->add_control('home_portfolio_section_button_link', array(
    'label' => __('Button Link', 'wphester-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_portfolio_callback'
));

//Button Open New Tab
$wp_customize->add_setting( 'home_portfolio_link_target',
    array(
        'default'           => false,
        'capability'        => 'edit_theme_options',   
        )
    );
$wp_customize->add_control('home_portfolio_link_target',
    array(
        'label'    => esc_html__( 'Open New Tab', 'wphester-plus' ),
        'section'  => 'portfolio_section',
        'type'     => 'checkbox', 
        )
    );

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('home_portfolio_section_title', array(
    'selector' => '#portfolio .section-title',
    'settings' => 'home_portfolio_section_title',
    'render_callback' => 'wphester_plus_home_portfolio_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_portfolio_section_subtitle', array(
    'selector' => '#portfolio .section-subtitle',
    'settings' => 'home_portfolio_section_subtitle',
    'render_callback' => 'wphester_plus_home_portfolio_section_subtitle_render_callback'
));

function wphester_plus_home_portfolio_section_title_render_callback() {
    return get_theme_mod('home_portfolio_section_title');
}

function wphester_plus_home_portfolio_section_subtitle_render_callback() {
    return get_theme_mod('home_portfolio_section_subtitle');
}