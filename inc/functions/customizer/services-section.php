<?php

$wp_customize->add_section('services_section', array(
    'title' => __('Services Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 11,
));


//Service Section

$wp_customize->add_setting('home_service_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'wphester_sanitize_checkbox'
));

$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'home_service_section_enabled',
                array(
            'label' => __('Enable/Disable Services Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'services_section',
                )
));

//Service section title
$wp_customize->add_setting('home_service_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Services We Provide', 'wphester-plus'),
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_service_section_title', array(
    'label' => __('Title', 'wphester-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_service_callback'
));

// Service section description
$wp_customize->add_setting('home_service_section_discription', array(
    'capability' => 'edit_theme_options',
    'default' => __('Our Best Services', 'wphester-plus'),
    'transport' => $selective_refresh,
));


$wp_customize->add_control('home_service_section_discription', array(
    'label' => __('Sub Title', 'wphester-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_service_callback'
));

//Style Design
$wp_customize->add_setting('home_serive_design_layout', array('default' => 1));
$wp_customize->add_control('home_serive_design_layout',
        array(
            'label' => __('Design Style', 'wphester-plus'),
            'section' => 'services_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design 1', 'wphester-plus'),
                2 => __('Design 2', 'wphester-plus'),
                3 => __('Design 3', 'wphester-plus'),
                4 => __('Design 4', 'wphester-plus')
            ),
            'active_callback' => 'wphester_plus_service_callback'
));

$wp_customize->add_setting('home_service_col',
        array(
            'default' => 3,
            'sanitize_callback' => 'wphester_plus_sanitize_select'
        )
);

$wp_customize->add_control('home_service_col',
        array(
            'label' => esc_html__('Column Layout', 'wphester-plus'),
            'section' => 'services_section',
            'type' => 'select',
            'active_callback' => 'wphester_plus_service_callback',
            'choices' => array(
                1 => esc_html__('1 Column', 'wphester-plus'),
                2 => esc_html__('2 Column', 'wphester-plus'),
                3 => esc_html__('3 Column', 'wphester-plus'),
                4 => esc_html__('4 Column', 'wphester-plus'),
            )
        )
);


if (class_exists('WPHester_Plus_Repeater')) {
    $wp_customize->add_setting('wphester_service_content', array());

    $wp_customize->add_control(new WPHester_Plus_Repeater($wp_customize, 'wphester_service_content', array(
                'label' => esc_html__('Service Content', 'wphester-plus'),
                'section' => 'services_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Service', 'wphester-plus'),
                'item_name' => esc_html__('Service', 'wphester-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'wphester_plus_service_callback'
    )));
}

$wp_customize->selective_refresh->add_partial('home_service_section_title', array(
    'selector' => '.services .section-title, .services2 .section-title, .services3 .section-title, .services4 .section-title',
    'settings' => 'home_service_section_title',
    'render_callback' => 'wphester_plus_home_service_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_service_section_discription', array(
    'selector' => '.services .section-subtitle, .services2 .section-subtitle, .services3 .section-subtitle, .services4 .section-subtitle',
    'settings' => 'home_service_section_discription',
    'render_callback' => 'wphester_plus_home_service_section_discription_render_callback'
));

$wp_customize->selective_refresh->add_partial('service_viewmore_btn_text', array(
    'selector' => '.services .view-more-services',
    'settings' => 'service_viewmore_btn_text',
    'render_callback' => 'wphester_plus_service_viewmore_btn_text_render_callback'
));

function wphester_plus_home_service_section_title_render_callback() {
    return get_theme_mod('home_service_section_title');
}

function wphester_plus_home_service_section_discription_render_callback() {
    return get_theme_mod('home_service_section_discription');
}

function wphester_plus_service_viewmore_btn_text_render_callback() {
    return get_theme_mod('service_viewmore_btn_text');
}