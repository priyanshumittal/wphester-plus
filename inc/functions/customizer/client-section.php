<?php

//Client Section

$wp_customize->add_section('home_client_section', array(
    'title' => esc_html__('Clients Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 21,
));

// Enable client section
$wp_customize->add_setting('client_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_sanitize_checkbox'
    ));

$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'client_section_enable',
                array(
            'label' => esc_html__('Enable/Disable Client Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'home_client_section',
                )
));



// clients & partners section title
$wp_customize->add_setting('home_client_section_title', array(
    'default' => esc_html__('They Trust Us', 'wphester-plus'),
    'sanitize_callback' => 'wphester_home_page_sanitize_text',
));
$wp_customize->add_control('home_client_section_title', array(
    'label' => esc_html__('Title', 'wphester-plus'),
    'section' => 'home_client_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_sponsors_callback'
));

// clients & partners section subtitle
$wp_customize->add_setting('home_client_section_subtitle', array(
    'default' => esc_html__('You Grow We Grow', 'wphester-plus'),
    'sanitize_callback' => 'wphester_home_page_sanitize_text',
));
$wp_customize->add_control('home_client_section_subtitle', array(
    'label' => esc_html__('Sub Title', 'wphester-plus'),
    'section' => 'home_client_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_sponsors_callback'
));

//clients & partners section discription
$wp_customize->add_setting('home_client_section_discription', array(
    'default' => __('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore <br> magna aliqua quis ips gravida Risus commodo.', 'wphester-plus'),
));
$wp_customize->add_control('home_client_section_discription', array(
    'label' => esc_html__('Description', 'wphester-plus'),
    'section' => 'home_client_section',
    'type' => 'textarea',
    'active_callback' => 'wphester_plus_sponsors_callback'
));

// Enable Background Color
$wp_customize->add_setting( 'enable_clt_bg_color', array(
    'default' => false,
    'sanitize_callback' => 'sanitize_text_field',
) );

$wp_customize->add_control('enable_clt_bg_color', array(
    'label'    => __('Enable Client Section Background Color', 'wphester-plus' ),
    'section'  => 'home_client_section',
    'type' => 'checkbox',
    'active_callback' => 'wphester_plus_sponsors_callback'
) );    

//Client Background Overlay Color
$wp_customize->add_setting('clt_bg_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => '#111111',
));

$wp_customize->add_control(new WPHester_Plus_Customize_Alpha_Color_Control($wp_customize, 'clt_bg_color', array(
            'label' => esc_html__('Client Section Background Color', 'wphester-plus'),
            'palette' => true,
            'active_callback' => 'wphester_plus_sponsors_callback',
            'section' => 'home_client_section')
));

if (class_exists('WPHester_Plus_Repeater')) {
    $wp_customize->add_setting(
            'wphester_clients_content', array(
            )
    );

    $wp_customize->add_control(
            new WPHester_Plus_Repeater(
                    $wp_customize, 'wphester_clients_content', array(
                'label' => esc_html__('Client Content', 'wphester-plus'),
                'section' => 'home_client_section',
                'add_field_label' => esc_html__('Add new client', 'wphester-plus'),
                'item_name' => esc_html__('Client', 'wphester-plus'),
                'customizer_repeater_image_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'active_callback' => 'wphester_plus_sponsors_callback'
                    )
            )
    );
}

$wp_customize->add_setting('client_items',
        array(
            'default' => 4,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wphester_sanitize_number_range',
        )
);
$wp_customize->add_control('client_items',
        array(
            'label' => esc_html__('Number of Clients', 'wphester-plus'),
            'section' => 'home_client_section',
            'type' => 'number',
            'input_attrs' => array('min' => 1, 'max' => 5, 'step' => 1, 'style' => 'width: 100%;'),
            'active_callback' => 'wphester_plus_sponsors_callback'
        )
);
//Navigation Type
$wp_customize->add_setting('client_nav_style', array('default' => 'navigation'));
$wp_customize->add_control('client_nav_style', array(
    'label' => __('Navigation Style', 'wphester-plus'),
    'section' => 'home_client_section',
    'type' => 'radio',
    'choices' => array(
        'bullets' => __('Bullets', 'wphester-plus'),
        'navigation' => __('Navigation', 'wphester-plus'),
        'both' => __('Both', 'wphester-plus'),
    ),
    'active_callback' => 'wphester_plus_sponsors_callback'
));

// animation speed
$wp_customize->add_setting('client_animation_speed', array('default' => 3000));
$wp_customize->add_control('client_animation_speed',
        array(
            'label' => __('Animation Speed', 'wphester-plus'),
            'section' => 'home_client_section',
            'type' => 'select',
            'choices' => array(
                2000 => '2.0',
                3000 => '3.0',
                4000 => '4.0',
                5000 => '5.0',
                6000 => '6.0',
            ),
            'active_callback' => 'wphester_plus_sponsors_callback'
));

// smooth speed
$wp_customize->add_setting('client_smooth_speed', array('default' => 1000));
$wp_customize->add_control('client_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wphester-plus'),
            'section' => 'home_client_section',
            'type' => 'select',
            'active_callback' => 'wphester_plus_sponsors_callback',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0')
));


$wp_customize->selective_refresh->add_partial('clt_bg_color', array(
    'selector' => '.sponsors.bg-default-color-3',
    'settings' => 'clt_bg_color',
    'render_callback' => 'clt_bg_color_render_callback'
));

$wp_customize->selective_refresh->add_partial('wphester_clients_content', array(
    'selector' => '.sponsors #clients-carousel',
    'settings' => 'wphester_clients_content',
    'render_callback' => 'wphester_clients_content_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_client_section_title', array(
    'selector' => '.sponsors .section-header h2.section-title',
    'settings' => 'home_client_section_title',
    'render_callback' => 'home_client_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_client_section_subtitle', array(
    'selector' => '.sponsors .section-header p.section-subtitle',
    'settings' => 'home_client_section_subtitle',
    'render_callback' => 'home_client_section_subtitle_render_callback'
));
$wp_customize->selective_refresh->add_partial('home_client_section_discription', array(
    'selector' => '.sponsors .section-header p.description',
    'settings' => 'home_client_section_discription',
    'render_callback' => 'home_client_section_discription_render_callback'
));
function home_client_section_discription_render_callback() {
    return get_theme_mod('home_client_section_discription');
}
function home_client_section_subtitle_render_callback() {
    return get_theme_mod('home_client_section_subtitle');
}
function home_client_section_title_render_callback() {
    return get_theme_mod('home_client_section_title');
}
function clt_bg_color_render_callback() {
    return get_theme_mod('clt_bg_color');
}
function wphester_clients_content_render_callback() {
    return get_theme_mod('wphester_clients_content');
}

?>