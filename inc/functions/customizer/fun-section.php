<?php

/* funfact section */
$wp_customize->add_section('funfact_section', array(
    'title' => __('Funfact Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 12,
));
$wp_customize->add_setting('funfact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'wphester_sanitize_checkbox'
));



$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'funfact_section_enabled',
                array(
            'label' => __('Enable/Disable Funfact Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'funfact_section',
                )
));


// funfact section title
$wp_customize->add_setting('funfact_section_title', array(
    'default' => __('We Have Random And <br> Interesting Facts.', 'wphester-plus'),
    'sanitize_callback' => 'wphester_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('funfact_section_title', array(
    'label' => esc_html__('Title', 'wphester-plus'),
    'section' => 'funfact_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_funfact_callback'
));

// funfact section subtitle
$wp_customize->add_setting('funfact_section_subtitle', array(
    'default' => __('We are ready to provide you with any financial, legal and auditing help as <br> well as prepare a business plan.', 'wphester-plus'),
    'sanitize_callback' => 'wphester_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('funfact_section_subtitle', array(
    'label' => esc_html__('Sub Title', 'wphester-plus'),
    'section' => 'funfact_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_funfact_callback'
));


if (class_exists('WPHester_Plus_Repeater')) {
    $wp_customize->add_setting('wphester_funfact_content', array());

    $wp_customize->add_control(new WPHester_Plus_Repeater($wp_customize, 'wphester_funfact_content', array(
                'label' => esc_html__('Funfact Content', 'wphester-plus'),
                'section' => 'funfact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Funfact', 'wphester-plus'),
                'item_name' => esc_html__('Funfact', 'wphester-plus'),
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'active_callback' => 'wphester_plus_funfact_callback'
    )));
}


//FunFact Background Image
$wp_customize->add_setting('funfact_callout_background', array(
    'default' => WPHESTERP_PLUGIN_URL .'inc/images/bg/funfact-bg.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'funfact_callout_background', array(
            'label' => esc_html__('Background Image', 'wphester-plus'),
            'section' => 'funfact_section',
            'settings' => 'funfact_callout_background',
            'active_callback' => 'wphester_plus_funfact_callback'
        )));

// Image overlay
$wp_customize->add_setting('funfact_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('funfact_image_overlay', array(
    'label' => esc_html__('Enable Funfact image overlay', 'wphester-plus'),
    'section' => 'funfact_section',
    'type' => 'checkbox',
    'active_callback' => 'wphester_plus_funfact_callback'
));

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('wphester_funfact_content', array(
    'selector' => '.funfact .row.fun-content',
    'settings' => 'wphester_funfact_content',
    'render_callback' => 'wphester_funfact_content_render_callback'
));

function wphester_funfact_content_render_callback() {
    return get_theme_mod('wphester_funfact_content');
}
$wp_customize->selective_refresh->add_partial('funfact_callout_background', array(
    'selector' => '.funfact.bg-default',
    'settings' => 'funfact_callout_background',    
    'render_callback' => 'funfact_callout_background_render_callback'
));

function funfact_callout_background_render_callback() {
    return get_theme_mod('funfact_callout_background');
}
$wp_customize->selective_refresh->add_partial('funfact_section_title', array(
    'selector' => '.funfact .funfact-block h2',
    'settings' => 'funfact_section_title',    
    'render_callback' => 'funfact_section_title_render_callback'
));

function funfact_section_title_render_callback() {
    return get_theme_mod('funfact_section_title');
}
$wp_customize->selective_refresh->add_partial('funfact_section_subtitle', array(
    'selector' => '.funfact .funfact-block p',
    'settings' => 'funfact_section_subtitle',    
    'render_callback' => 'funfact_section_subtitle_render_callback'
));

function funfact_section_subtitle_render_callback() {
    return get_theme_mod('funfact_section_subtitle');
}

?>