<?php

//Latest News Section
$wp_customize->add_section('wphester_latest_news_section', array(
    'title' => __('Latest News Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' =>14,
));


// Enable news section
$wp_customize->add_setting('latest_news_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_plus_sanitize_checkbox'
));

$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'latest_news_section_enable',
                array(
            'label' => __('Enable/Disable Latest News Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'wphester_latest_news_section',
                )
));

// News section title
$wp_customize->add_setting('home_news_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('All News & Articles', 'wphester-plus'),
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_title', array(
    'label' => __('Title', 'wphester-plus'),
    'section' => 'wphester_latest_news_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_news_callback'
));



//News section subtitle
$wp_customize->add_setting('home_news_section_discription', array(
    'default' => __('Our Blogs', 'wphester-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_discription', array(
    'label' => __('Sub Title', 'wphester-plus'),
    'section' => 'wphester_latest_news_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_news_callback'
));

$wp_customize->add_setting('home_news_design_layout', array('default' => 1));
$wp_customize->add_control('home_news_design_layout',
        array(
            'label' => __('Design Style', 'wphester-plus'),
            'section' => 'wphester_latest_news_section',
            'active_callback' => 'wphester_plus_news_callback',
            'type' => 'select',
            'choices' => array(                
                1 => __('Carousel Style', 'wphester-plus'),
                2 => __('List Style', 'wphester-plus'),
                3 => __('Masonry Style', 'wphester-plus'),
                4 => __('Grid Style', 'wphester-plus'),
                5 => __('Switcher Style', 'wphester-plus'),
            )
));

/* * ****************** Blog Content ****************************** */

$wp_customize->add_setting('wphester_homeblog_layout',
        array(
            'default' => 4,
            'sanitize_callback' => 'wphester_sanitize_select'
        )
);

$wp_customize->add_control('wphester_homeblog_layout',
        array(
            'label' => esc_html__('Column Layout', 'wphester-plus'),
            'section' => 'wphester_latest_news_section',
            'type' => 'select',
            'active_callback' => 'wphester_column_callback',
            'active_callback' => function($control) {
                return (
                        wphester_plus_news_callback($control) &&
                        wphester_column_callback($control)
                        );
            },
            'choices' => array(
                6 => esc_html__('2 Column', 'wphester-plus'),
                4 => esc_html__('3 Column', 'wphester-plus'),
            )
        )
);

$wp_customize->add_setting('wphester_homeblog_counts',
        array(
            'default' => 4,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'wphester_sanitize_number_range',
        )
);
$wp_customize->add_control('wphester_homeblog_counts',
        array(
            'label' => esc_html__('Number of Posts', 'wphester-plus'),
            'section' => 'wphester_latest_news_section',
            'type' => 'number',
            'input_attrs' => array('min' => 2, 'max' => 20, 'step' => 1, 'style' => 'width: 100%;'),
            'active_callback' => 'wphester_plus_news_callback'
        )
);


// Read More Button
$wp_customize->add_setting('home_news_button_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Read More', 'wphester-plus'),
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_button_title', array(
    'label' => __('Read More Text', 'wphester-plus'),
    'section' => 'wphester_latest_news_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_news_callback'
));

// enable/disable meta section 
$wp_customize->add_setting(
        'home_meta_section_settings',
        array('capability' => 'edit_theme_options',
            'default' => true,
));
$wp_customize->add_control(
        'home_meta_section_settings',
        array(
            'type' => 'checkbox',
            'label' => __('Enable post meta in blog section', 'wphester-plus'),
            'section' => 'wphester_latest_news_section',
            'active_callback' => 'wphester_plus_news_callback'
        )
);

//Navigation Type
$wp_customize->add_setting('news_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('news_nav_style', array(
    'label' => __('Navigation Style', 'wphester-plus'),
    'section' => 'wphester_latest_news_section',
    'type' => 'radio',
    'choices' => array(
        'bullets' => __('Bullets', 'wphester-plus'),
        'navigation' => __('Navigation', 'wphester-plus'),
        'both' => __('Both', 'wphester-plus'),
    ),
    'active_callback' => 'wphester_plus_news_callback'
));

// animation speed
$wp_customize->add_setting('newz_animation_speed', array('default' => 3000));
$wp_customize->add_control('newz_animation_speed',
        array(
            'label' => __('Animation Speed', 'wphester-plus'),
            'section' => 'wphester_latest_news_section',
            'type' => 'select',
            'choices' => array(
                2000 => '2.0',
                3000 => '3.0',
                4000 => '4.0',
                5000 => '5.0',
                6000 => '6.0',
            ),
            'active_callback' => 'wphester_plus_news_callback'
));

// smooth speed
$wp_customize->add_setting('newz_smooth_speed', array('default' => 1000));
$wp_customize->add_control('newz_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wphester-plus'),
            'section' => 'wphester_latest_news_section',
            'type' => 'select',
            'active_callback' => 'wphester_plus_news_callback',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0')
));

/**
 * Add selective refresh for Front page news section controls.
 */
$wp_customize->selective_refresh->add_partial('home_news_section_title', array(
    'selector' => '.home-blog .section-header h2',
    'settings' => 'home_news_section_title',
    'render_callback' => 'wphester_plus_home_news_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_section_discription', array(
    'selector' => '.home-blog .section-header h5',
    'settings' => 'home_news_section_discription',
    'render_callback' => 'wphester_plus_home_news_section_discription_render_callback',
));



function wphester_plus_home_news_section_title_render_callback() {
    return get_theme_mod('home_news_section_title');
}

function wphester_plus_home_news_section_discription_render_callback() {
    return get_theme_mod('home_news_section_discription');
}

function wphester_plus_home_blog_more_btn_render_callback() {
    return get_theme_mod('home_blog_more_btn');
}

function wphester_plus_column_callback($control) {
    if ($control->manager->get_setting('home_news_design_layout')->value() == '2') {
        return false;
    }
    return true;
}