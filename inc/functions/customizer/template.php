<?php

/**
 * Template Options Customizer
 *
 * @package wphester
 */
function wphester_plus_template_customizer($wp_customize) {
    
    $wp_customize->add_panel('wphester_template_settings',
            array(
                'priority' => 920,
                'capability' => 'edit_theme_options',
                'title' => __('Page Settings', 'wphester-plus')
            )
    );

    // Add section to manage Portfolio page settings
    $wp_customize->add_section(
            'porfolio_page_section',
            array(
                'title' => __('Portfolio Page Settings', 'wphester-plus'),
                'panel' => 'wphester_template_settings',
                'priority' => 600,
            )
    );

    // Portfolio page title
    $wp_customize->add_setting(
            'porfolio_page_title', array(
        'default' => __('Our Portfolio', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'wphester-plus'),
                'section' => 'porfolio_page_section',
            )
    );

    // Portfolio page subtitle
    $wp_customize->add_setting(
            'porfolio_page_subtitle', array(
        'default' => __('Our Recent Works', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_subtitle',
            array(
                'type' => 'text',
                'label' => __('Sub Title', 'wphester-plus'),
                'section' => 'porfolio_page_section',
            )
    );
    
    // Add section to manage Portfolio category page settings
    $wp_customize->add_section(
            'porfolio_category_page_section',
            array(
                'title' => __('Portfolio Category Page Settings', 'wphester-plus'),
                'panel' => 'wphester_template_settings',
                'priority' => 700,
            )
    );

     // Portfolio category page title
    $wp_customize->add_setting(
            'porfolio_category_page_title', array(
        'default' => __('Portfolio Category Title', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'wphester-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    
    // Portfolio category page subtitle
    $wp_customize->add_setting(
            'porfolio_category_page_desc', array(
        'default' => __('Morbi facilisis, ipsum ut ullamcorper venenatis, nisi diam placerat turpis, at auctor nisi magna cursus arcu.', 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_desc',
            array(
                'type' => 'text',
                'label' => __('Description', 'wphester-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    
   // Number of portfolio column layout
    $wp_customize->add_setting('portfolio_cat_column_layout', array(
        'default' => 4,
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('portfolio_cat_column_layout', array(
        'type' => 'select',
        'label' => __('Column Layout', 'wphester-plus'),
        'section' => 'porfolio_category_page_section',
        'choices' => array(
                6 => __('2 Column', 'wphester-plus'),
                4 => __('3 Column', 'wphester-plus'),
                3 => __('4 Column', 'wphester-plus'),
            )
    ));

    // Add section to manage Contact page settings
    $wp_customize->add_section(
            'contact_page_section',
            array(
                'title' => __('Contact Page Settings', 'wphester-plus'),
                'panel' => 'wphester_template_settings',
                'priority' => 800,
            )
    );

    // Contact form title
    $wp_customize->add_setting(
            'contact_cf7_title', array(
        'default' => __("Send Us A Message", 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_cf7_title',
            array(
                'type' => 'text',
                'label' => __('Contact Form Title', 'wphester-plus'),
                'section' => 'contact_page_section',
            )
    );

    // Contact Detail Title
    $wp_customize->add_setting(
            'contact_dt_title', array(
        'default' => __("Contact Details", 'wphester-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_dt_title',
            array(
                'type' => 'text',
                'label' => __('Contact Detail Title', 'wphester-plus'),
                'section' => 'contact_page_section',
            )
    );

    if (class_exists('WPHester_Plus_Repeater')) {
        $wp_customize->add_setting('wphester_plus_contact_content', array());

        $wp_customize->add_control(new WPHester_Plus_Repeater($wp_customize, 'wphester_plus_contact_content', array(
                    'label' => esc_html__('Contact Details', 'wphester-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Contact Info', 'wphester-plus'),
                    'item_name' => esc_html__('Contact', 'wphester-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_title_control' => true,
                    'customizer_repeater_text_control' => true,
        )));
    }
    if (class_exists('wphester_plus_Repeater')) {
        $wp_customize->add_setting('wphester_social_links', array());

        $wp_customize->add_control(new wphester_plus_Repeater($wp_customize, 'wphester_social_links', array(
                    'label' => esc_html__('Social Links', 'busicare-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Social Link', 'busicare-plus'),
                    'item_name' => esc_html__('Social Link', 'busicare-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_link_control' => true,
        )));
    }
    // google map shortcode
    $wp_customize->add_setting('contact_google_map_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_google_map_shortcode', array(
        'label' => __('Google Map Shortcode', 'wphester-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    // Contact form 7 shortcode
    $wp_customize->add_setting('contact_form_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_form_shortcode', array(
        'label' => esc_html__('Contact Form Shortcode', 'wphester-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    $wp_customize->selective_refresh->add_partial('contact_cf7_title', array(
        'selector' => '.contact-form h2',
        'settings' => 'contact_cf7_title',
        'render_callback' => 'contact_cf7_title_render_callback'
    ));
    
    $wp_customize->selective_refresh->add_partial('contact_dt_title', array(
        'selector' => '.contact-section .contact-info h2,.contact-info.conatct-page4 h2.section-title',
        'settings' => 'contact_dt_title',
        'render_callback' => 'contact_dt_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_title', array(
        'selector' => '.portfolio-cat-page h2.section-title',
        'settings' => 'porfolio_category_page_title',
        'render_callback' => 'porfolio_category_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_desc', array(
        'selector' => '.portfolio-cat-page h5.section-subtitle ',
        'settings' => 'porfolio_category_page_desc',
        'render_callback' => 'porfolio_category_page_desc_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_title', array(
        'selector' => '.portfolio-page h2.section-title',
        'settings' => 'porfolio_page_title',
        'render_callback' => 'porfolio_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_subtitle', array(
        'selector' => '.portfolio-page h5.section-subtitle',
        'settings' => 'porfolio_page_subtitle',
        'render_callback' => 'porfolio_page_subtitle_render_callback'
    ));

    function contact_cf7_title_render_callback() {
        return get_theme_mod('contact_cf7_title');
    }
    function contact_dt_title_render_callback() {
        return get_theme_mod('contact_dt_title');
    }
    
    function contact_info_title_render_callback() {
        return get_theme_mod('contact_info_title');
    }

    function porfolio_category_page_title_render_callback() {
        return get_theme_mod('porfolio_category_page_title');
    }

    function porfolio_category_page_desc_render_callback() {
        return get_theme_mod('porfolio_category_page_desc');
    }

    function porfolio_page_title_render_callback() {
        return get_theme_mod('porfolio_page_title');
    }

    function porfolio_page_subtitle_render_callback() {
        return get_theme_mod('porfolio_page_subtitle');
    }

}

add_action('customize_register', 'wphester_plus_template_customizer');
?>