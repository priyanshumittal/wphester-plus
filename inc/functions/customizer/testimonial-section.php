<?php

/* Testimonial Section */
$wp_customize->add_section('testimonial_section', array(
    'title' => __('Testimonials Settings', 'wphester-plus'),
    'panel' => 'section_settings',
    'priority' => 17,
));

// Enable testimonial section
$wp_customize->add_setting('testimonial_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'wphester_sanitize_checkbox'
));

$wp_customize->add_control(new WPHester_Toggle_Control($wp_customize, 'testimonial_section_enable',
                array(
            'label' => __('Enable/Disable Testimonial Section', 'wphester-plus'),
            'type' => 'toggle',
            'section' => 'testimonial_section',
                )
));


// testimonial section title
$wp_customize->add_setting('home_testimonial_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Our Happy Customers', 'wphester-plus'),
    'sanitize_callback' => 'wphester_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_testimonial_section_title', array(
    'label' => __('Title', 'wphester-plus'),
    'section' => 'testimonial_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_testimonial_callback'
));

//testimonials & partners section discription
$wp_customize->add_setting('home_testimonial_section_discription', array(
    'default' => __('What Our Client Says', 'wphester-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_testimonial_section_discription', array(
    'label' => __('Sub Title', 'wphester-plus'),
    'section' => 'testimonial_section',
    'type' => 'text',
    'active_callback' => 'wphester_plus_testimonial_callback'
));


//Style Design
$wp_customize->add_setting('home_testimonial_design_layout', array('default' => 1));
$wp_customize->add_control('home_testimonial_design_layout',
        array(
            'label' => __('Design Style', 'wphester-plus'),
            'active_callback' => 'wphester_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design 1', 'wphester-plus'),
                2 => __('Design 2', 'wphester-plus'),
                3 => __('Design 3', 'wphester-plus'),
                4 => __('Design 4', 'wphester-plus')
            )
));

//Slide Item
$wp_customize->add_setting('home_testimonial_slide_item', array('default' => 1));
$wp_customize->add_control('home_testimonial_slide_item',
        array(
            'label' => __('Slide Item', 'wphester-plus'),
            'active_callback' => 'wphester_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => __('One', 'wphester-plus'),
                2 => __('Two', 'wphester-plus'),
                3 => __('Three', 'wphester-plus'),
            )
));

if (class_exists('WPHester_Plus_Repeater')) {
    $wp_customize->add_setting('wphester_testimonial_content', array(
    ));

    $wp_customize->add_control(new WPHester_Plus_Repeater($wp_customize, 'wphester_testimonial_content', array(
                'label' => esc_html__('Testimonial Content', 'wphester-plus'),
                'section' => 'testimonial_section',
                'add_field_label' => esc_html__('Add new Testimonial', 'wphester-plus'),
                'item_name' => esc_html__('Testimonial', 'wphester-plus'),
//                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'customizer_repeater_user_name_control' => true,
                'customizer_repeater_designation_control' => true,
                'active_callback' => 'wphester_plus_testimonial_callback'
    )));
}

//Testimonial Background Image
$wp_customize->add_setting('testimonial_callout_background', array(
    'default' => WPHESTERP_PLUGIN_URL.'/inc/images/bg/img.png',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'testimonial_callout_background', array(
            'label' => __('Background Image', 'wphester-plus'),
            'section' => 'testimonial_section',
            'settings' => 'testimonial_callout_background',
            'active_callback' => 'wphester_plus_testimonial_callback'
        )));

// Image overlay
$wp_customize->add_setting('testimonial_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('testimonial_image_overlay', array(
    'label' => __('Enable testimonial image overlay', 'wphester-plus'),
    'section' => 'testimonial_section',
    'type' => 'checkbox',
    'active_callback' => 'wphester_plus_testimonial_callback'
));


//Testimonial Background Overlay Color
$wp_customize->add_setting('testimonial_overlay_section_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'rgba(0,0,0,0.3)',
));

$wp_customize->add_control(new WPHester_Plus_Customize_Alpha_Color_Control($wp_customize, 'testimonial_overlay_section_color', array(
            'label' => __('Testimonial Image Overlay Color', 'wphester-plus'),
            'palette' => true,
            'active_callback' => 'wphester_plus_testimonial_callback',
            'section' => 'testimonial_section')
));

//Navigation Type
$wp_customize->add_setting('testimonial_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('testimonial_nav_style', array(
    'label' => __('Navigation Style', 'wphester-plus'),
    'section' => 'testimonial_section',
    'type' => 'radio',
    'priority' => 17,
    'choices' => array(
        'bullets' => __('Bullets', 'wphester-plus'),
        'navigation' => __('Navigation', 'wphester-plus'),
        'both' => __('Both', 'wphester-plus'),
    ),
    'active_callback' => 'wphester_plus_testimonial_callback'
));

// animation speed
$wp_customize->add_setting('testimonial_animation_speed', array('default' => 3000));
$wp_customize->add_control('testimonial_animation_speed',
        array(
            'label' => __('Animation Speed', 'wphester-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'wphester_plus_testimonial_callback'
));

// smooth speed
$wp_customize->add_setting('testimonial_smooth_speed', array('default' => 1000));
$wp_customize->add_control('testimonial_smooth_speed',
        array(
            'label' => __('Smooth Speed', 'wphester-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0'),
            'active_callback' => 'wphester_plus_testimonial_callback'
));


/**
 * Add selective refresh for Front page testimonial section controls.
 */
$wp_customize->selective_refresh->add_partial('home_testimonial_section_title', array(
    'selector' => '.section-space.testimonial .section-title',
    'settings' => 'home_testimonial_section_title',
    'render_callback' => 'wphester_plus_home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_section_discription', array(
    'selector' => '.testimonial .section-header p',
    'settings' => 'home_testimonial_section_discription',
    'render_callback' => 'wphester_plus_home_testimonial_section_discription_render_callback',
));

function wphester_plus_home_testimonial_section_title_render_callback() {
    return get_theme_mod('home_testimonial_section_title');
}

function wphester_plus_home_testimonial_section_discription_render_callback() {
    return get_theme_mod('home_testimonial_section_discription');
}